SET folder=%~dp0\..\EternalKingdom\bin\console

php %folder% doctrine:migrations:migrate --no-interaction
php %folder% doctrine:fixtures:load --no-interaction --append --group=prod