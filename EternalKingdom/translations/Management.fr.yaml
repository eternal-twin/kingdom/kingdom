tabHeader: "Gestion"
header:
  infos:
    name: "Mes infos"
  logs:
    name: "Historique de votre Royaume"
  overlord:
    name: "Suzerain"
    desc: "Si votre ville est conquise, vous aurez un suzerain qui taxera votre commerce"

messages:
  openCity: "Gérer votre Capitale"
  noSuzerain: "Votre capitale est actuellement libre, vous êtes votre propre Suzerain."

actions:
  buyTurns: "Acheter des tours"
  suicide: "Mettre fin à mon règne"

tabs:
  generals:
    title: "Généraux"
    tooltip: "Vos généraux sont les seuls qui peuvent transporter vos soldats et agrandir votre Royaume"
    noGeneral: "Vous n'avez pas encore recruté de général."
    noHeadQuarters: "Il vous faudra construire un Quartier Général dans votre capitale auparavant"
    state:
      waitingAt: "En attente à "
      movingTo: "Se déplace entre <a href='%originLink%'>%originName%</a> et <a href='%destinationLink%'>%destinationName%</a>"
      attacking: "Participe à la <a href='%battleLink%'>Bataille de %locationName%</a>"
      fortifying: "Fortifie à "
    reputation: "Réputation"
    reputationTooltip: "La réputation de votre général grandit quand il remporte des batailles. Chaque point de réputation permet à votre général de réduire le coût en or des unités qui l\\'accompagnent."
    maintenance: "Entretien"
    maintenanceTooltip: "Le coût d\\'entretien des soldats en défense ou en garnison à chaque tour."

  vassals:
    title: "Vassaux"
    tooltip: "Les seigneurs dont vous avez conquis la capitale deviendront vos vassaux, ce qui vous permettra de les taxer"
    noVassal: "Vous n'avez actuellement aucun vassal. Il vous faudra d'abord conquérir de nouvelles capitales à l'aide de vos généraux"
    taxes: "Taxes"
    taxesToCollect: "Taxes à collecter"
    collect: "Collecter"

  kingdom:
    title: "Royaume"
    tooltip: "Tous les lieux que vous avez conquis, ainsi que les garnisons qui y sont stationnées."
    noTown: "Vous ne contrôlez actuellement aucun lieu en plus de votre capitale. Il vous faudra d'abord en prendre le contrôle à l'aide de vos généraux."
    increasingTrade: "Grâce à la protection de vos soldats, le commerce est en plein développement dans cette partie de votre Royaume !"
    stableTrade: "Le commerce se porte bien dans cette partie de votre Royaume, mais votre garnison a du mal à faire face au nombre croissant de bandits."
    decreasingTrade: "Votre garnison lutte activement contre les bandits, mais sans renforts il leur faudra encore un certain temps avant de redresser la situation."
    noCityTrade: "Une capitale ne génère pas de commerce."
    noDefense: "Aucune unité en défense"
    defense: "Défense"
    town: "Ville"
    status: "Statut"
    units: "Unités"
    unitsTooltip: "Le nombre de vos unités en défense ou en garnison sur ce lieu"

  diplomacy:
    title: "Diplomatie"
    tooltip: "Résume vos accords diplomatiques que vous pouvez changer à partir de la fiche de chaque joueur"
    relation:
      player: "Joueur"
      type:
        title: "État"
        tooltip: "Votre neutralité vis-à-vis de ce joueur."
        friend: "Ami"
        neutral: "Neutre"
        enemy: "Ennemi"
      canCross:
        title: "Passage"
        tooltip: "Vous avez autorisé ce joueur à commercer et à se déplacer à travers votre royaume."
      canHarvest:
        title: "Récolte"
        tooltip: "Vous avez autorisé ce joueur à collecter les ressources sur votre royaume."
    noDiplomacy:
      yourAgreements: "Vous n'avez pas encore d'accord diplomatique avec d'autres joueurs"
      inverted: "Aucun joueur n'a effectué d'accord diplomatique vous concernant."
    searchPlayer: "Chercher un joueur"
    search: "Chercher"
    invert:
      button: "Inverser"
      tooltip: "Voir les accords diplomatiques des autres joueurs vous concernant"
    yourAgreements:
      button: "Vos accords"
      tootlip: "Voir les accords diplomatiques que vous avez passé"

healths:
  excellent: "Excellente"
  good: "Bonne"
  bad: "Mauvaise"
  dead: "Mort"

stats:
  title: 
    name: "Titre"
    desc: "Votre titre dépend de la Gloire que vous avez atteint à votre apogée"
  age: 
    name: "Age"
    desc: "Votre âge augmente d\\'environ une semaine à chaque action de production dans votre capitale"
  health: 
    name: "Santé"
    desc: "Votre santé se détériorera en fonction de votre âge. Faîtes attention car même les rois les plus puissants meurent un jour..."
  glory: 
    name: "Gloire"
    desc: "Votre Gloire correspond à la taille de votre Royaume à laquelle s\\'ajoute la taille des Royaumes de vos vassaux."
  maxGlory: 
    name: "Apogée"
    desc: "Il s\\'agit de la Gloire maximum que vous avez atteint au cours de votre vie."
  trade: 
    name: "Commerce"
    desc: "Le commerce est généré par les lieux que vous contrôlez dans votre royaume. Il s\\'agit de l\\'or que vous gagnez ou perdez à chaque tour de production "

city:
  capitalCity:
    title: "Capitale"
    desc: "Votre capitale est le centre de votre Royaume, vous pouvez y construire des bâtiments, déveloper votre population, et former vos armées"
  food:
    title: "Quantité actuelle de nourriture"
    desc: "Il s\\'agit de la <em>nourriture</em> disponible dans les réserves de votre Capitale."
  consumption: "Il s\\'agit de la <em>nourriture</em> utilisée pour nourrir la population de la capitale après chaque tour de production."
  gold:
    title: "Quantité actuelle d\\'or"
    desc: "Il s\\'agit de l\\'<em>or</em> disponible dans les réserves de votre capitale."
  armyMaintenance:
    title: "Entretien Total"
    desc: "Il s\\'agit de l\\'<em>or</em> utilisé pour payer les soldats après chaque tour de production."
  trade:
    title: "Commerce Total"
    desc: "Il s\\'agit de l\\'<em>or</em> que vous gagnez ou perdez après chaque tour de production grâce au commerce généré dans votre Royaume."
    desc2: "<b>Votre suzerain prélève <strong>0</strong> <img src=\\'/img/icons/small/res_gold.gif\\'/> de taxes sur votre commerce.</b>"
  turn: "tour"
  total: "Total"
