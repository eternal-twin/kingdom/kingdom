tabHeader: "Capitale"

header:
  population: "Population"
  units: "Unités"
  actions: "Actions"
  buildings: "Bâtiments"
  logs: "Historique de votre capitale"
  build: "Construisez un bâtiment"
  soldierOptions: "Options des soldats"
  convertResources: "Convertir des ressources"
  recruitGeneral: "Recruter un général"

buildings:
  palace:
    name: "Palais"
    desc: "Le Palais est le bâtiment central de la capitale mais aussi le centre de votre pouvoir."
    upgradeDesc: "Améliorer votre Palais vous permettra d\\'améliorer vos bâtiments au niveau suivant."
  attic:
    name: "Grenier"
    desc: "Le Grenier vous permet de stocker votre nourriture, ce qui est particulièrement utile en cas de famine prolongée."
    upgradeDesc: "Plus le grenier de la ville est grand et plus votre de stock de <em>nourriture</em> augmentera."
  farm:
    name: "Ferme"
    desc: "La Ferme est habitée par vos <em>Paysans</em> et contribue à nourrir la population de votre ville, à condition que le blé veuille bien pousser !"
    upgradeDesc: "Plus la Ferme de la ville est développée et plus la production de nourriture par les <em>Paysans</em> augmente."
  hut:
    name: "Cabane"
    desc: "La Cabane est le lieu de travail de vos <em>Bûcherons</em>. C\\'est un lieu bruyant où sont entassés de grandes quantités de bois et parait-il d\\'alcool."
    upgradeDesc: "Une cabane plus grande vous permettra d\\'augmenter à la fois la production de bois par les <em>Bûcherons</em> mais aussi le stock maximum de bois que votre ville peut conserver."
  market:
    name: "Marché"
    desc: "Le Marché augmente l\\'activité des <em>Marchands</em> de votre ville, ce qui vous permettra de financer vos coûteuses campagnes militaires."
    upgradeDesc: "Améliorer votre marché rendra vos <em>Marchands</em> plus efficaces et ils produiront donc plus d\\'or."
  workshop:
    name: "Atelier"
    desc: "L\\'Atelier est un lieu où on assemble les machines de guerre <em>Balistes</em> et <em>Catapultes</em>, particulièrement meurtrières pour peu qu\\'on les manie correctement."
    upgradeDesc: "Un Atelier plus grand diminuera les coûts des <em>Balistes</em> et des <em>Catapultes</em> et augmentera aussi la productivité de vos <em>Ouvriers</em>."
  constructionSite:
    name: "Chantier"
    desc: "Quand les <em>Ouvriers</em> n\\'ont rien à construire, ils produiront quand-même un peu d\\'or et de nourriture s\\'ils ont un Chantier où s\\'occuper. Mais attention à toujours porter un casque... Surtout quand ils s\\'amusent au lancer de marteau !"
    upgradeDesc: "L\\'amélioration du Chantier permettra à vos <em>Ouvriers</em> de produire un peu de nourriture et d\\'or quand vous effectuez l\\'action construire et qu\\'aucun bâtiment n\\'est en cours de construction."
  barracks:
    name: "Caserne"
    desc: "Engagez-vous qu\\'ils disaient ! La Caserne vous permet de recruter des <em>Soldats</em> qui viendront défendre la ville contre les attaques de vos ennemis."
    upgradeDesc: "Chaque agrandissement de la caserne vous donnera la possibilité d\\'avoir quelques soldats qui ne demanderont aucun salaire tant qu\\'ils restent dans votre capitale."
  headquarters:
    name: "Quartier Général"
    desc: "On reconnait le Quartier Général aux bonnes odeurs qui s\\'en échappent vers l\\'heure du déjeuner. Mais il ne s\\'agit pas d\\'une simple cantine trois-étoiles car il abrite les <em>Généraux</em> de passage."
    upgradeDesc: "Chaque amélioration du Quartier Général vous permettra de contrôler un <em>Général</em> supplémentaire pour mener vos armées."
  wall:
    name: "Mur"
    desc: "Le Mur permet d\\'empêcher les <em>Généraux</em> des autres joueurs de traverser les lieux de votre royaume qui sont défendus."
    upgradeDesc: "Améliorer le Mur vous permettra de ralentir les attaques ennemies sur votre capitale, en diminuant le nombre de duels lors d\\'une bataille."
  militaryAcademy:
    name: "Académie Militaire"
    desc: "L\\'Académie est un lieu de formation pour les unités d\\'élite, mais aussi pour les fils de généraux qui ont eu du piston et rêvent de devenir <em>Chevalier</em> ou <em>Archer Monté</em>."
    upgradeDesc: "Améliorer l\\'Académie vous permettra de réduire le coût d\\'amélioration d\\'un Soldat en <em>Chevalier</em> ou en <em>Archer Monté</em>."
  guardTower:
    name: "Tour de garde"
    desc: "La Tour de Garde permet d\\'améliorer les simples <em>Soldats</em> en <em>Paladins</em>. On voit les ennemis de très loin depuis là-haut, ce qui en fait un endroit privilégié pour les futurs déserteurs."
    upgradeDesc: "Améliorer la Tour de Garde vous permettra de réduire le coût d\\'amélioration d\\'un Soldat en Paladin."
  stable:
    name: "Etables"
    desc: "On repère facilement la direction des Etables grâce à l\\'odeur. Mais pas question de critiquer, les <em>Cavaliers</em> sont très susceptibles quand il s\\'agit de leurs montures."
    upgradeDesc: "Agrandir les étables vous permettra de réduire le cout d\\'amélioration d\\'un soldat en <em>Cavalier</em> et d\\'augmenter votre stock maximum de chevaux."
  butcher:
    name: "Boucherie"
    desc: "La faim justifie parfois les moyens. En cas de famine, un cheval pourra toujours nourrir votre population."
    upgradeDesc: "Améliorer l\\'efficacité de la Boucherie vous permettra d\\'obtenir plus de nourriture pour chaque cheval (miam !)"
  archery:
    name: "Archerie"
    desc: "Il vaut mieux ne pas se promener du côté de l\\'Archerie à l\\'heure de l\\'apéritif, car il arrive parfois de se retrouver en face d\\'une flèche égarée par un <em>Archer</em> ayant forcé sur la boisson."
    upgradeDesc: "Agrandir l\\'archerie vous permettra de réduire le cout d\\'amélioration d\\'un soldat en <em>Archer</em>"
  factory:
    name: "Manufacture"
    desc: "La Manufacture vous permettra de tisser des vêtements à l\\'aide de <em>lin</em> qui seront ensuite vendus contre de l\\'or à vos voisins pour que vous puissiez enfin les envahir. On y stocke aussi du <em>lin</em>."
    upgradeDesc: "Agrandir la manufacture vous permettra d\\'augmenter votre stock maximum de lin ainsi que l\\'or tiré de chaque lin tissé"
  forge:
    name: "Forge"
    desc: "La Forge est un lieu surchauffé rempli de vapeurs nauséabondes. On y fabrique les lances qui équipent les <em>Piquiers</em> et on y stocke aussi du métal."
    upgradeDesc: "Agrandir la Forge vous permettra de réduire le coût d\\'amélioration d\\'un Soldat en Piquier et d\\'augmenter votre stock maximum de métal."
  cauldron:
    name: "Chaudron"
    desc: "Le Chaudron vous permettra de faire fondre de vastes quantités de terre ocre pour en extraire un peu de métal. Pas très écologique, mais ça marche."
    upgradeDesc: "Améliorer l\\'efficacité du Chaudron vous permettra d\\'avoir un meilleur bilan énergétique, et donc de consommer moins de bois par unité de métal produite."

resources:
  wheat:
    name: "Nourriture"
    desc: "La nourriture est produite par les <em>paysans</em> et permet de nourrir la population de la ville."
  gold:
    name: "Or"
    desc: "L\\'or est produit par les <em>marchands</em> et permet de payer les soldats."
  wood:
    name: "Bois"
    desc: "Le bois et produit par les <em>bûcherons</em> et utilisé pour construire les bâtiments."
  iron:
    name: "Métal"
    desc: "Le métal est extrait des mines et permet entre autres d\\'améliorer les soldats en <em>paladins</em>."
  lin:
    name: "Lin"
    desc: "Le lin se trouve dans certains lieux et permet entre autres d\\'améliorer les soldats en <em>archers</em>."
  horse: 
    name: "Chevaux"
    desc: "Les chevaux se capturent dans les prairies et permettent entre autres d\\'améliorer les soldats en <em>cavaliers</em>"
  

jobs:
  farmer:
    name: "Paysan"
    desc: "Les paysans produisent la nourriture permettant de nourrir la population."
  lumberjack:
    name: "Bûcheron"
    desc: "Les bûcherons approvisionnent la ville en bois permettant la construction de bâtiments."
  worker:
    name: "Ouvrier"
    desc: "Les ouvriers travaillent sur les chantiers à la construction des bâtiments de la ville."
  merchant:
    name: "Marchand"
    desc: "Les marchands produisent de l\\'or grâce à leur commerce, ce qui permet de payer les soldats."
  recruiter:
    name: "Recruteur"
    desc: "Les recruteurs permettent d\\'accélérer la construction des armées."
  citizen:
    name: "Citoyen"
    desc: "Les citoyen ne servent à rien, assignez leur un métier pour les rendre utiles !"

messages:
  turn:
    remaining: "Tours en attente"
    wasted: "Tours gaspillés "
    nextTurn: "Prochain tour"
    warning: "Vous n'avez plus de<br> %icon% <b>tour</b> disponible"
    infinite: "Tours Infini"
  noAction: "Aucune action n'est disponible pour l'instant"
  noArmy: "Aucun soldat en défense actuellement"
  noWorker: "Recrutez un ouvrier pour pouvoir construire votre palais"
  soldierOptions:
    noRequiredBuilding: "Vous n'avez actuellement aucun bâtiment vous permettant d'améliorer vos soldats."
    unitPrice: "Prix par unité :"
    recruitment: 
      enable: "Activer le recrutement"
      disable: "Désactiver le recrutement" 
      desc: "Cette action va modifier le recrutement des soldat lors de l\\'alignement d\\'épées."
  build:  
    start: "Construire"
    list: "Liste des bâtiments"
    cancel: "Annuler la construction"
    requireBuilding: "Nécessite "
    requiresTitle: "Nécessite le titre "
    hammerTooltip:
      title: "Marteaux"
      description: "Nombre de marteaux nécessaires pour aboutir cette construction."
  pendingConstruction:
    message: "Construction en cours de"
    remaining: "restants"
  convert:
    spend: "Dépenser"
    use: "Utiliser le"
    for: "pour"
    quantity: "Quantité"
  recruitGeneral:
    tooltip: "Un général vous permettra de déplacer vos unités et conquérir de nouveaux territoires pour agrandir votre royaume. Recruter ce général vous coûtera <strong>%price%</strong> %icon%"
    title: "Nommez votre général"
    name: "Nom"
  forbiddenSwap: "Vous ne pouvez inverser deux symboles que si ceux-ci vous permettent d'en aligner trois. Une action est toujours possible et est indiquée par un léger contour blanc."
  lvl: "Niv"
  level: "Niveau"   
