<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220628124504 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added Resource spot';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE resource_spot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE resource_spot (id INT NOT NULL, owning_world_id INT NOT NULL, node_id INT NOT NULL, resource_id INT NOT NULL, amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B612DAC97F5CDE8E ON resource_spot (owning_world_id)');
        $this->addSql('CREATE INDEX IDX_B612DAC9460D9FD7 ON resource_spot (node_id)');
        $this->addSql('CREATE INDEX IDX_B612DAC989329D25 ON resource_spot (resource_id)');
        $this->addSql('ALTER TABLE resource_spot ADD CONSTRAINT FK_B612DAC97F5CDE8E FOREIGN KEY (owning_world_id) REFERENCES world_map (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resource_spot ADD CONSTRAINT FK_B612DAC9460D9FD7 FOREIGN KEY (node_id) REFERENCES world_node_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resource_spot ADD CONSTRAINT FK_B612DAC989329D25 FOREIGN KEY (resource_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE resource_spot_id_seq CASCADE');
        $this->addSql('DROP TABLE resource_spot');
    }
}
