<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221105150456 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added User inversed mapping to the Lord entity';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lord ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE lord ADD CONSTRAINT FK_44E9A58FA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_44E9A58FA76ED395 ON lord (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE lord DROP CONSTRAINT FK_44E9A58FA76ED395');
        $this->addSql('DROP INDEX UNIQ_44E9A58FA76ED395');
        $this->addSql('ALTER TABLE lord DROP user_id');
    }
}
