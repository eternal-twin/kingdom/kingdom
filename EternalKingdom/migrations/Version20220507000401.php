<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220507000401 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Linked WorldData to World - Added map directory to WorldData';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE world_data ADD dir_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE world_map ADD data_id INT NOT NULL');
        $this->addSql('ALTER TABLE world_map ADD CONSTRAINT FK_ECF1BB5637F5A13C FOREIGN KEY (data_id) REFERENCES world_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_ECF1BB5637F5A13C ON world_map (data_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE world_map DROP CONSTRAINT FK_ECF1BB5637F5A13C');
        $this->addSql('DROP INDEX IDX_ECF1BB5637F5A13C');
        $this->addSql('ALTER TABLE world_map DROP data_id');
        $this->addSql('ALTER TABLE world_data DROP dir_name');
    }
}
