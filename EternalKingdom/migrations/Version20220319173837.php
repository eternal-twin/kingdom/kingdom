<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220319173837 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Refactor Log System';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE game_log_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE city_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE log_event_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE city_log (id INT NOT NULL, event_id INT NOT NULL, lord_id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, dynamic_data JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A209124D71F7E88B ON city_log (event_id)');
        $this->addSql('CREATE INDEX IDX_A209124D868E8BB9 ON city_log (lord_id)');
        $this->addSql('CREATE TABLE log_event (id INT NOT NULL, name VARCHAR(255) NOT NULL, expected_data JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE city_log ADD CONSTRAINT FK_A209124D71F7E88B FOREIGN KEY (event_id) REFERENCES log_event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city_log ADD CONSTRAINT FK_A209124D868E8BB9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE game_log');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE city_log DROP CONSTRAINT FK_A209124D71F7E88B');
        $this->addSql('DROP SEQUENCE city_log_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE log_event_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE game_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE game_log (id INT NOT NULL, lord_id INT NOT NULL, "timestamp" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, log VARCHAR(255) NOT NULL, params TEXT DEFAULT NULL, category VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_94657b00868e8bb9 ON game_log (lord_id)');
        $this->addSql('COMMENT ON COLUMN game_log.params IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE game_log ADD CONSTRAINT fk_94657b00868e8bb9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE city_log');
        $this->addSql('DROP TABLE log_event');
    }
}
