<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220424214108 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'World Map';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE world_map_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE world_map (id INT NOT NULL, name VARCHAR(255) NOT NULL, language VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE lord ADD world_map_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lord ADD CONSTRAINT FK_44E9A58FC3FB2227 FOREIGN KEY (world_map_id) REFERENCES world_map (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_44E9A58FC3FB2227 ON lord (world_map_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE lord DROP CONSTRAINT FK_44E9A58FC3FB2227');
        $this->addSql('DROP SEQUENCE world_map_id_seq CASCADE');
        $this->addSql('DROP TABLE world_map');
        $this->addSql('DROP INDEX IDX_44E9A58FC3FB2227');
        $this->addSql('ALTER TABLE lord DROP world_map_id');
    }
}
