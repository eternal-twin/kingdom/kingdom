<?php

namespace App\Kingdom\Ruleset\CoreRuleset;

use App\Kingdom\Lord\Enum\TitleEnum;

/**
 * @phpstan-type Title array{id: TitleEnum, name: string, glory: int}
 */
abstract class CoreRuleset
{
    public const string healthKey = 'health';
    public const string sideBonusKey = 'sideBonus';
    public const string attackSideKey = 'attack';
    public const string defenseSideKey = 'defense';
    public const string dmgBonusKey = 'bonusValue';
    public const string strongAgainstKey = 'strongAgainst';
    public const string abilityKey = 'ability';
    public const string cancelAnyBonus = 'cancelAnyBonus';

    /**
     * @return array<Title>
     */
    abstract public function getTitles(): array;
    abstract public function getMaxSimultaneousFightsInBattle(): int;
    abstract public function getUnitsInfo(): array;
}
