<?php

namespace App\Kingdom\Grid\Enum;

enum PuzzlePieceEnum: string
{
    case FOOD = 'PFood';
    case GOLD = 'PGold';
    case WOOD = 'PWood';
    case BUILD = 'PBuild';
    case SOLDIER = 'PSoldier';
    case PEOPLE = 'PPeople';
    case AGAIN = 'PAgain';
}
