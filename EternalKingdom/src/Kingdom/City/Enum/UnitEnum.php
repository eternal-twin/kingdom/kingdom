<?php

namespace App\Kingdom\City\Enum;

enum UnitEnum: string
{
    case SOLDIER = 'soldier';
    case ARCHER = 'archer';
    case HORSEMAN = 'horseman';
    case PALADIN = 'paladin';
    case PIKEMAN = 'pikeman';
    case KNIGHT = 'knight';
    case MOUNTED_ARCHER = 'mountedArcher';
    case CATAPULT = 'catapult';
    case BALLISTA = 'ballista';
}
