<?php

namespace App\Kingdom\Battle\Service;

use App\Entity\Battle\Battle;
use App\Entity\Battle\EngagedArmy;
use App\Entity\Battle\Fight;
use App\Entity\Battle\FightingUnit;
use App\Entity\Battle\FightSurvivor;
use App\Kingdom\City\Enum\BuildingEnum;
use App\Kingdom\City\Enum\UnitEnum;
use App\Kingdom\Ruleset\CoreRuleset\CoreRuleset;
use App\Kingdom\Ruleset\Ruleset;
use App\Kingdom\Ruleset\Service\GetRuleset;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Generates as many fights as possible on the specified Battle
 * It will rely on the current amount of pending fights, wall level, remaining unit on each side...
 */
class RefillFights
{
    public const string K_UnitName = 'unitName';
    public const string K_UnitArmy = 'unitArmy';
    public const string K_FightSurvivor = 'fightSurvivor';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly GetRuleset $getRuleset,
    ) {
    }

    public function exec(Battle $battle): void
    {
        $currentFightAmount = $battle->getFights()->count();
        $maxFightAmount = $this->calculateMaxFightAmount($battle);

        $missingFightAmount = $maxFightAmount - $currentFightAmount;
        if ($missingFightAmount <= 0) {
            return;
        }

        $attackingArmies = $battle->getAttackingArmies();
        $attackingUnitsPool = $this->generateUnitsPool($attackingArmies);

        $defendingArmies = $battle->getDefensingArmies();
        $defendingUnitsPool = $this->generateUnitsPool($defendingArmies);

        $amountOfFightsToGenerate = min(count($attackingUnitsPool), count($defendingUnitsPool), $missingFightAmount);
        if ($amountOfFightsToGenerate > 0) {
            $previousFightAmount = $battle->getFights()->count();

            $damageBonusDataAsAttacker = $this->computeAllDamageBonus(true);
            $damageBonusDataAsDefender = $this->computeAllDamageBonus(false);

            for ($i = 0; $i < $amountOfFightsToGenerate; ++$i) {
                $ruleSet = $this->getRuleset->exec();
                $attackingUnit = $this->selectUnitForNextFight($attackingUnitsPool, $ruleSet);
                $defendingUnit = $this->selectUnitForNextFight($defendingUnitsPool, $ruleSet);

                $attackingUnitDamageBonus = $this->selectDamageBonusFromData($damageBonusDataAsAttacker, $attackingUnit, $defendingUnit);
                $attackingUnit->setDamageBonus($attackingUnitDamageBonus);

                $defendingUnitDamageBonus = $this->selectDamageBonusFromData($damageBonusDataAsDefender, $defendingUnit, $attackingUnit);
                $defendingUnit->setDamageBonus($defendingUnitDamageBonus);

                $battle->addFight(new Fight($battle, $attackingUnit, $defendingUnit));
            }

            assert($battle->getFights()->count() - $previousFightAmount === $amountOfFightsToGenerate);

            $this->entityManager->persist($battle);
        }
    }

    private function calculateMaxFightAmount(Battle $battle): int
    {
        $ruleset = $this->getRuleset->exec();

        $wallLevel = $battle->getLocation()->getSovereign()->getCity()->getBuildingLevel(BuildingEnum::WALL->value);

        return $ruleset->getMaxSimultaneousFights($wallLevel);
    }

    /**
     * Generates all the Units available for fight from the specified armies.
     *
     * @param array<EngagedArmy> $engagedArmies
     */
    private function generateUnitsPool(array $engagedArmies): array
    {
        $unitsPool = [];
        foreach ($engagedArmies as $engagedArmy) {
            foreach ($engagedArmy->getArmyContainer()->getUnits() as $unitArmy) {
                if ($unitArmy->getAmount() > 0) {
                    for ($u = 0; $u < $unitArmy->getAmount(); ++$u) {
                        $unitsPool[] = [
                            self::K_UnitArmy => $engagedArmy,
                            self::K_UnitName => $unitArmy->getResource()->getName(),
                        ];
                    }
                }
            }

            foreach ($engagedArmy->getFightSurvivors() as $fightSurvivor) {
                $unitsPool[] = [
                    self::K_FightSurvivor => $fightSurvivor,
                ];
            }
        }

        shuffle($unitsPool);

        return $unitsPool;
    }

    private function computeAllDamageBonus(bool $isAttacking): array
    {
        $unitFightOutcomes = [];
        $ruleSet = $this->getRuleset->exec();
        $allUnitsInfo = $ruleSet->getAllUnitsInfo();

        foreach (UnitEnum::cases() as $unit) {
            $unitName = $unit->value;
            $unitInfo = $allUnitsInfo[$unitName];

            if (array_key_exists(CoreRuleset::strongAgainstKey, $unitInfo)) {
                foreach ($unitInfo[CoreRuleset::strongAgainstKey] as $otherUnitName) {
                    $unitFightOutcomes[$unitName][$otherUnitName] = $unitInfo[CoreRuleset::dmgBonusKey];
                }
            }

            if (array_key_exists(CoreRuleset::sideBonusKey, $unitInfo)) {
                $hasAttackBonusFromSide = CoreRuleset::attackSideKey == $unitInfo[CoreRuleset::sideBonusKey] && $isAttacking
                    || CoreRuleset::defenseSideKey == $unitInfo[CoreRuleset::sideBonusKey] && false == $isAttacking;
                if ($hasAttackBonusFromSide) {
                    foreach (UnitEnum::cases() as $otherUnit) {
                        $otherUnitInfo = $allUnitsInfo[$otherUnit->value];
                        if (array_key_exists(CoreRuleset::abilityKey, $otherUnitInfo) && CoreRuleset::cancelAnyBonus == $otherUnitInfo[CoreRuleset::abilityKey]) {
                            continue;
                        }
                        $unitFightOutcomes[$unitName][$otherUnit->value] = $unitInfo[CoreRuleset::dmgBonusKey];
                    }
                }
            }
        }

        return $unitFightOutcomes;
    }

    private function selectDamageBonusFromData(array $damageBonusData, FightingUnit $attackingUnit, FightingUnit $defendingUnit)
    {
        $attackingUnitName = $attackingUnit->getUnitType()->getName();
        $defendingUnitName = $defendingUnit->getUnitType()->getName();
        if (array_key_exists($attackingUnitName, $damageBonusData)
            && array_key_exists($defendingUnitName, $damageBonusData[$attackingUnitName])) {
            return $damageBonusData[$attackingUnitName][$defendingUnitName];
        }

        return 0;
    }

    /**
     * Get a random Unit from the specified pool
     * It will take a full health or injured Unit.
     */
    private function selectUnitForNextFight(array &$unitsPool, Ruleset $ruleSet): FightingUnit
    {
        $randomSelectedUnit = array_pop($unitsPool);

        if (array_key_exists(self::K_FightSurvivor, $randomSelectedUnit)) {
            /** @var FightSurvivor $fightSurvivor */
            $fightSurvivor = $randomSelectedUnit[self::K_FightSurvivor];
            $this->entityManager->remove($fightSurvivor);

            $newFightingUnit = new FightingUnit($fightSurvivor->getUnitType(), $fightSurvivor->getOwningEngagedArmy(), $fightSurvivor->getRemainingHealth());
        } else {
            /** @var EngagedArmy $owningEngagedArmy */
            $owningEngagedArmy = $randomSelectedUnit[self::K_UnitArmy];
            $unitArmy = $owningEngagedArmy->getArmyContainer()->getUnit($randomSelectedUnit[self::K_UnitName]);
            $unitArmy->setAmount($unitArmy->getAmount() - 1);

            $unitInfo = $ruleSet->getUnitsInfo($unitArmy->getResource()->getName());
            $newFightingUnit = new FightingUnit($unitArmy->getResource(), $owningEngagedArmy, $unitInfo[CoreRuleset::healthKey]);
        }

        return $newFightingUnit;
    }
}
