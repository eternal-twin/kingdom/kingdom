<?php

namespace App\DataFixtures;

use App\Service\GameData\EKMaxStoragesLoader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MaxStorageFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function __construct(public EKMaxStoragesLoader $dataLoader)
    {
    }

    #[\Override]
    public function load(ObjectManager $manager): void
    {
        $this->dataLoader->loadGameData($manager);
    }

    #[\Override]
    public function getDependencies(): array
    {
        return [ResourceFixtures::class, ConstructionFixtures::class];
    }

    #[\Override]
    public static function getGroups(): array
    {
        return ['prod'];
    }
}
