<?php

namespace App\DataFixtures;

use App\Service\GameData\EKResourcesLoader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class ResourceFixtures extends Fixture implements FixtureGroupInterface
{
    public function __construct(private readonly EKResourcesLoader $dataLoader)
    {
    }

    #[\Override]
    public function load(ObjectManager $manager): void
    {
        $this->dataLoader->loadGameData($manager);
    }

    #[\Override]
    public static function getGroups(): array
    {
        return ['prod', 'world'];
    }
}
