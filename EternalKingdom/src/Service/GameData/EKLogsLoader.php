<?php

namespace App\Service\GameData;

use App\Entity\LogSystem\LogEvent;
use App\Enum\LogEventType;
use App\Enum\LogParamType;
use App\Repository\LogSystem\LogEventRepository;
use Doctrine\Persistence\ObjectManager;

/**
 * Describle all possible Log events for the game (Management, City, World, Battle).
 */
class EKLogsLoader extends GameDataLoader
{
    private array $logEvents = [
        // City Logs
        [
            'name' => LogEventType::CITY_RESOURCE_PRODUCTION,
            'data' => [
                ['type' => LogParamType::RAW,      'name' => '%production%'],
                ['type' => LogParamType::RESOURCE, 'name' => '%icon%'],
            ],
        ],
        [
            'name' => LogEventType::CITY_RESOURCE_WASTE,
            'data' => [
                ['type' => LogParamType::RAW,      'name' => '%waste%'],
                ['type' => LogParamType::RESOURCE, 'name' => '%icon%'],
            ],
        ],
        [
            'name' => LogEventType::CITY_RESOURCE_CONVERSION,
            'data' => [
                ['type' => LogParamType::RAW,       'name' => '%inAmount%'],
                ['type' => LogParamType::RESOURCE,  'name' => '%inIcon%'],
                ['type' => LogParamType::RAW,       'name' => '%outAmount%'],
                ['type' => LogParamType::RESOURCE,  'name' => '%outIcon%'],
            ],
        ],
        [
            'name' => LogEventType::CITY_RESOURCE_CONSUMPTION_FOOD,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%amount%'],
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'small/res_wheat.gif'],
            ],
        ],
        [
            'name' => LogEventType::CITY_RESOURCE_CONSUMPTION_FOOD_GOLD,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%foodAmount%'],
                ['type' => LogParamType::ICON, 'name' => '%foodIcon%', 'path' => 'small/res_wheat.gif'],
                ['type' => LogParamType::RAW,  'name' => '%goldAmount%'],
                ['type' => LogParamType::ICON, 'name' => '%goldIcon%', 'path' => 'small/res_gold.gif'],
            ],
        ],
        [
            'name' => LogEventType::CITY_HAMMER_PROGRESS,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%amount%'],
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'small/icon_hammer.gif'],
            ],
        ],
        [
            'name' => LogEventType::CITY_HAMMER_NO_CONSTRUCTION,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%amount%'],
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_cantbuild.png'],
            ],
        ],
        [
            'name' => LogEventType::CITY_HAMMER_PRODUCTION,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%amount%'],
                ['type' => LogParamType::ICON, 'name' => '%iconGold%', 'path' => 'small/res_gold.gif'],
                ['type' => LogParamType::ICON, 'name' => '%iconFood%', 'path' => 'small/res_wheat.gif'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CITIZEN_BIRTH,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'small/people_pend.gif'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CITIZEN_CONVERSION,
            'data' => [
                ['type' => LogParamType::TRANSLATION, 'name' => '%jobName%', 'prefix' => 'jobs.', 'suffix' => '.name', 'domain' => 'City'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CITIZEN_STARVATION,
            'data' => [
                ['type' => LogParamType::TRANSLATION, 'name' => '%jobName%', 'prefix' => 'jobs.', 'suffix' => '.name', 'domain' => 'City'],
            ],
        ],
        [
            'name' => LogEventType::CITY_ARMY_NO_BARRACKS,
        ],
        [
            'name' => LogEventType::CITY_ARMY_NO_RECRUITMENT,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_cantrecruit.png'],
            ],
        ],
        [
            'name' => LogEventType::CITY_ARMY_PRODUCTION,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%amount%'],
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'small/u_soldier.gif'],
            ],
        ],
        [
            'name' => LogEventType::CITY_ARMY_CONVERSION,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%soldierAmount%'],
                ['type' => LogParamType::RAW,  'name' => '%outAmount%'],
                ['type' => LogParamType::UNIT, 'name' => '%outIcon%'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CONSTRUCTION_BUILDING_STARTED,
            'data' => [
                ['type' => LogParamType::TRANSLATION,  'name' => '%name%', 'prefix' => 'buildings.', 'suffix' => '.name', 'domain' => 'City'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CONSTRUCTION_BUILDING_FINISHED,
            'data' => [
                ['type' => LogParamType::TRANSLATION,  'name' => '%name%', 'prefix' => 'buildings.', 'suffix' => '.name', 'domain' => 'City'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CONSTRUCTION_BUILDING_CANCELLED,
            'data' => [
                ['type' => LogParamType::TRANSLATION,  'name' => '%name%', 'prefix' => 'buildings.', 'suffix' => '.name', 'domain' => 'City'],
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_cantbuild.png'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CONSTRUCTION_UNIT_STARTED,
            'data' => [
                ['type' => LogParamType::TRANSLATION,  'name' => '%name%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CONSTRUCTION_UNIT_FINISHED,
            'data' => [
                ['type' => LogParamType::TRANSLATION,  'name' => '%name%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
            ],
        ],
        [
            'name' => LogEventType::CITY_CONSTRUCTION_UNIT_CANCELLED,
            'data' => [
                ['type' => LogParamType::TRANSLATION,  'name' => '%name%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_cantbuild.png'],
            ],
        ],
        [
            'name' => LogEventType::CITY_REPLAY,
            'data' => [
                ['type' => LogParamType::RAW,  'name' => '%count%'],
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'small/res_rec.gif'],
            ],
        ],
        [
            'name' => LogEventType::CITY_GRID_RESET,
        ],
        [
            'name' => LogEventType::CITY_GENERAL_RECRUITMENT,
        ],
        [
            'name' => LogEventType::CITY_GENERAL_HARVEST,
            'data' => [
                ['type' => LogParamType::RAW,       'name' => '%generalName%'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
                ['type' => LogParamType::RAW,       'name' => '%amount%'],
                ['type' => LogParamType::RESOURCE,  'name' => '%icon%'],
            ],
        ],

        // Kingdom Logs
        [
            'name' => LogEventType::MANAGEMENT_SPAWN,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_new.png'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_DEATH_INACTIVITY,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_destroy.png'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_DEATH_NATURAL,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_destroy.png'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_CONQUEST_NO_RESISTANCE,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_conquest.png'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_CONQUEST_WITH_BATTLE,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_conquest.png'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_TERRITORY_GOT_STOLEN,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_conquest.png'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_PROMOTION,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_up.png'],
                ['type' => LogParamType::TRANSLATION, 'name' => '%title%', 'prefix' => 'lord.titles.', 'suffix' => '', 'domain' => 'Common'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_HEALTH_DEGRADATION_1,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_badhp.png'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_HEALTH_DEGRADATION_2,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_verybadhp.png'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_DIPLOMACY_FRIEND,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_friend.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_DIPLOMACY_ENEMY,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_enemy.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_DIPLOMACY_NEUTRAL,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_neutral.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_CROSS_RIGHT_ADDED,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_pass.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_CROSS_RIGHT_REMOVED,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_dontpass.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_HARVEST_RIGHT_ADDED,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_resources.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_HARVEST_RIGHT_REMOVED,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_refusres.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_BATTLE_INSTIGATED_AGAINST_BARBARIAN,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_battle.png'],
                ['type' => LogParamType::RAW, 'name' => '%generalName%'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_BATTLE_INSTIGATED_AGAINST_PLAYER,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_battle.png'],
                ['type' => LogParamType::RAW, 'name' => '%generalName%'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_BATTLE_DEFENSE_ON_TERRITORY,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_battle.png'],
                ['type' => LogParamType::RAW, 'name' => '%generalName%'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_BATTLE_DEFENSE_ON_CAPITAL_CITY,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_attackcap.png'],
                ['type' => LogParamType::RAW, 'name' => '%generalName%'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_BATTLE_WON,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_battlewon.png'],
                ['type' => LogParamType::BATTLE_LINK, 'name' => '%battleLink%'],
            ],
        ],
        [
            'name' => LogEventType::MANAGEMENT_BATTLE_LOST,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_battlelost.png'],
                ['type' => LogParamType::BATTLE_LINK, 'name' => '%battleLink%'],
            ],
        ],

        // World Logs
        [
            'name' => LogEventType::WORLD_SPAWN,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_new.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::WORLD_DEATH_INACTIVITY,
            'data' => [
                ['type' => LogParamType::ICON, 'name' => '%icon%', 'path' => 'l_destroy.png'],
                ['type' => LogParamType::USER_LINK, 'name' => '%lordLink%'],
                ['type' => LogParamType::NODE_LINK, 'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::WORLD_CONQUEST_WITH_BATTLE,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_conquest.png'],
                ['type' => LogParamType::RAW,           'name' => '%generalName%'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
                ['type' => LogParamType::NODE_LINK,     'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::WORLD_CONQUEST_NO_RESISTANCE,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_conquest.png'],
                ['type' => LogParamType::RAW,           'name' => '%generalName%'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
                ['type' => LogParamType::NODE_LINK,     'name' => '%nodeLink%'],
            ],
        ],
        [
            'name' => LogEventType::WORLD_PROMOTION,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_up.png'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%newTitle%', 'prefix' => 'lord.titles.', 'suffix' => '', 'domain' => 'Common'],
            ],
        ],
        [
            'name' => LogEventType::WORLD_RESOURCE_SPAWN,
            'data' => [
                ['type' => LogParamType::RESOURCE,      'name' => '%icon%'],
                ['type' => LogParamType::NODE_LINK,     'name' => '%nodeLink%'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%resourceSpot%', 'prefix' => 'map.node.resourceSpot.', 'suffix' => '', 'domain' => 'Map'],
            ],
        ],

        // TODO Rename all %lordLink% -> %userLink%

        // Battle Logs
        [
            'name' => LogEventType::BATTLE_BARBARIAN_JOINED,
            'data' => [
                ['type' => LogParamType::RAW,           'name' => '%amount%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_GARRISON_JOINED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_battleplus.png'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
                ['type' => LogParamType::RAW,           'name' => '%amount%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_GENERAL_JOINED_DEFENSE,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_battleplus.png'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
                ['type' => LogParamType::RAW,           'name' => '%generalName%'],
                ['type' => LogParamType::RAW,           'name' => '%amount%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_GENERAL_JOINED_ATTACK,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_battleplus.png'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
                ['type' => LogParamType::RAW,           'name' => '%generalName%'],
                ['type' => LogParamType::RAW,           'name' => '%amount%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_NODE_OWNER_UNIT_DIED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_death.png'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%unit%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_NODE_OWNER_UNIT_DESTROYED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_death.png'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%unit%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_GENERAL_UNIT_DIED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_death.png'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%unit%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
                ['type' => LogParamType::RAW,           'name' => '%generalName%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_GENERAL_UNIT_DESTROYED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_death.png'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%unit%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
                ['type' => LogParamType::RAW,           'name' => '%generalName%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_BARBARIAN_UNIT_DIED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_death.png'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%unit%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_BARBARIAN_UNIT_DESTROYED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_death.png'],
                ['type' => LogParamType::TRANSLATION,   'name' => '%unit%', 'prefix' => 'units.', 'suffix' => '.name', 'domain' => 'Common'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_GENERAL_DEFEATED,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_battlelost.png'],
                ['type' => LogParamType::RAW,           'name' => '%generalName%'],
                ['type' => LogParamType::USER_LINK,     'name' => '%lordLink%'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_DEFENSE_WON,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_battlewon.png'],
            ],
        ],
        [
            'name' => LogEventType::BATTLE_ATTACK_WON,
            'data' => [
                ['type' => LogParamType::ICON,          'name' => '%icon%', 'path' => 'l_battlewon.png'],
            ],
        ],
    ];

    #[\Override]
    public function loadGameData(ObjectManager $manager): void
    {
        /** @var LogEventRepository $repo */
        $repo = $manager->getRepository(LogEvent::class);
        foreach ($this->logEvents as $logEventData) {
            $entity = $repo->findOneBy(['name' => $logEventData['name']]);
            if (null == $entity) {
                $entity = new LogEvent();
                $entity->setName($logEventData['name']);
            }

            if (array_key_exists('data', $logEventData)) {
                $entity->setExpectedData($logEventData['data']);
            } else {
                $entity->setExpectedData([]);
            }

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
