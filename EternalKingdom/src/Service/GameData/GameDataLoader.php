<?php

namespace App\Service\GameData;

/*
 * Define a set of game data and allow to load them inside the DB
 */
use Doctrine\Persistence\ObjectManager;

abstract class GameDataLoader
{
    abstract public function loadGameData(ObjectManager $manager);

    // TODO Do a similar logic than the fixtures (getDependencies ?)
}
