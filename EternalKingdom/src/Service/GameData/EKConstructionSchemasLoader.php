<?php

namespace App\Service\GameData;

use App\Entity\Constructions;
use App\Entity\ConstructionSchemas;
use App\Entity\Resources;
use Doctrine\Persistence\ObjectManager;

class EKConstructionSchemasLoader extends GameDataLoader
{
    private array $constructionsSchemas = [
        ['construction' => 'palace',            'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'palace',            'level' => 1,     'resource' => 'gold',             'amount' => 100],
        ['construction' => 'palace',            'level' => 1,     'resource' => 'wood',             'amount' => 100],
        ['construction' => 'palace',            'level' => 1,     'resource' => 'remainingHammers', 'amount' => 12],

        ['construction' => 'farm',              'level' => 1,     'resource' => 'wood',             'amount' => 20],
        ['construction' => 'farm',              'level' => 1,     'resource' => 'remainingHammers', 'amount' => 8],

        ['construction' => 'attic',             'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'attic',             'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],

        ['construction' => 'market',            'level' => 1,     'resource' => 'gold',             'amount' => 20],
        ['construction' => 'market',            'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'market',            'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],

        ['construction' => 'hut',               'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'hut',               'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],

        ['construction' => 'constructionSite',  'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'constructionSite',  'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'constructionSite',  'level' => 1,     'resource' => 'remainingHammers', 'amount' => 8],

        ['construction' => 'barracks',          'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'barracks',          'level' => 1,     'resource' => 'gold',             'amount' => 100],
        ['construction' => 'barracks',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 2],

        ['construction' => 'wall',              'level' => 1,     'resource' => 'wood',             'amount' => 200],
        ['construction' => 'wall',              'level' => 1,     'resource' => 'remainingHammers', 'amount' => 10],

        ['construction' => 'headquarters',      'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'headquarters',      'level' => 1,     'resource' => 'gold',             'amount' => 100],
        ['construction' => 'headquarters',      'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],

        ['construction' => 'workshop',          'level' => 1,     'resource' => 'gold',             'amount' => 40],
        ['construction' => 'workshop',          'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'workshop',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 16],

        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'lin',              'amount' => 5],
        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'iron',             'amount' => 5],
        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'remainingHammers', 'amount' => 22],

        ['construction' => 'militaryAcademy',   'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'militaryAcademy',   'level' => 1,     'resource' => 'gold',             'amount' => 160],
        ['construction' => 'militaryAcademy',   'level' => 1,     'resource' => 'remainingHammers', 'amount' => 6],

        ['construction' => 'archery',           'level' => 1,     'resource' => 'gold',             'amount' => 60],
        ['construction' => 'archery',           'level' => 1,     'resource' => 'wood',             'amount' => 160],
        ['construction' => 'archery',           'level' => 1,     'resource' => 'lin',              'amount' => 5],
        ['construction' => 'archery',           'level' => 1,     'resource' => 'remainingHammers', 'amount' => 2],

        ['construction' => 'factory',           'level' => 1,     'resource' => 'gold',             'amount' => 60],
        ['construction' => 'factory',           'level' => 1,     'resource' => 'wood',             'amount' => 100],
        ['construction' => 'factory',           'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],

        ['construction' => 'stable',            'level' => 1,     'resource' => 'gold',             'amount' => 140],
        ['construction' => 'stable',            'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'stable',            'level' => 1,     'resource' => 'horse',            'amount' => 5],
        ['construction' => 'stable',            'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],

        ['construction' => 'butcher',           'level' => 1,     'resource' => 'wheat',            'amount' => 30],
        ['construction' => 'butcher',           'level' => 1,     'resource' => 'gold',             'amount' => 120],
        ['construction' => 'butcher',           'level' => 1,     'resource' => 'remainingHammers', 'amount' => 2],

        ['construction' => 'forge',             'level' => 1,     'resource' => 'wheat',            'amount' => 20],
        ['construction' => 'forge',             'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'forge',             'level' => 1,     'resource' => 'iron',             'amount' => 5],
        ['construction' => 'forge',             'level' => 1,     'resource' => 'remainingHammers', 'amount' => 14],

        ['construction' => 'cauldron',          'level' => 1,     'resource' => 'wood',             'amount' => 80],
        ['construction' => 'cauldron',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 12],

        ['construction' => 'catapult',          'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'catapult',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 24],

        ['construction' => 'ballista',          'level' => 1,     'resource' => 'wood',             'amount' => 240],
        ['construction' => 'ballista',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 6],
    ];

    private array $resources = [];
    private array $constructions = [];

    #[\Override]
    public function loadGameData(ObjectManager $manager): void
    {
        $this->fillData();

        $constructionsRepo = $manager->getRepository(Constructions::class);
        $resourcesRepo = $manager->getRepository(Resources::class);
        $constructionSchemasRepo = $manager->getRepository(ConstructionSchemas::class);

        foreach ($this->constructionsSchemas as $schemaData) {
            $construction = EKDataHelpers::findEntity($constructionsRepo, $this->constructions, $schemaData['construction']);
            $resource = EKDataHelpers::findEntity($resourcesRepo, $this->resources, $schemaData['resource']);

            $entity = $constructionSchemasRepo->findOneBy(['construction' => $construction, 'level' => $schemaData['level'], 'resource' => $resource]);
            if (null == $entity) {
                $entity = new ConstructionSchemas();
                $entity->setConstruction($construction)
                    ->setResource($resource)
                    ->setLevel($schemaData['level']);
            }

            $entity->setAmount($schemaData['amount']);

            $manager->persist($entity);
        }
        $manager->flush();
    }

    // Determines superior level costs from level1 cost
    private function fillData(): void
    {
        foreach ($this->constructionsSchemas as $schema) {
            if ('catapult' == $schema['construction'] || 'ballista' == $schema['construction']) {
                switch ($schema['resource']) {
                    case 'wood':
                        $schemaLevel2 = $this->generateScaledSchema($schema, 2, 0.8);
                        $schemaLevel3 = $this->generateScaledSchema($schema, 3, 0.7);
                        $schemaLevel4 = $this->generateScaledSchema($schema, 4, 0.6);
                        $schemaLevel5 = $this->generateScaledSchema($schema, 5, 0.5);
                        $this->constructionsSchemas[] = $schemaLevel2;
                        $this->constructionsSchemas[] = $schemaLevel3;
                        $this->constructionsSchemas[] = $schemaLevel4;
                        $this->constructionsSchemas[] = $schemaLevel5;
                        break;
                    case 'remainingHammers':
                        $schemaLevel2 = $this->generateScaledSchema($schema, 2, 1);
                        $schemaLevel3 = $this->generateScaledSchema($schema, 3, 1);
                        $schemaLevel4 = $this->generateScaledSchema($schema, 4, 1);
                        $schemaLevel5 = $this->generateScaledSchema($schema, 5, 1);
                        $this->constructionsSchemas[] = $schemaLevel2;
                        $this->constructionsSchemas[] = $schemaLevel3;
                        $this->constructionsSchemas[] = $schemaLevel4;
                        $this->constructionsSchemas[] = $schemaLevel5;
                }
            } else {
                switch ($schema['resource']) {
                    case 'wheat':
                        $schemaLevel2 = $this->generateScaledSchema($schema, 2, 2.5);
                        $schemaLevel3 = $this->generateScaledSchema($schema, 3, 5);
                        $schemaLevel4 = $this->generateScaledSchema($schema, 4, 20);
                        $schemaLevel5 = $this->generateScaledSchema($schema, 5, 50);
                        break;
                    case 'wood':
                        $schemaLevel2 = $this->generateScaledSchema($schema, 2, 2.5);
                        $schemaLevel3 = $this->generateScaledSchema($schema, 3, 5);
                        $schemaLevel4 = $this->generateScaledSchema($schema, 4, 20);
                        $schemaLevel5 = $this->generateScaledSchema($schema, 5, 47.5);
                        break;
                    case 'gold':
                        $schemaLevel2 = $this->generateScaledSchema($schema, 2, 2);
                        $schemaLevel3 = $this->generateScaledSchema($schema, 3, 4);
                        $schemaLevel4 = $this->generateScaledSchema($schema, 4, 15);
                        $schemaLevel5 = $this->generateScaledSchema($schema, 5, 32.5);
                        break;
                    case 'remainingHammers':
                        $schemaLevel2 = $this->generateScaledSchema($schema, 2, 2);
                        $schemaLevel3 = $this->generateScaledSchema($schema, 3, 4.5);
                        $schemaLevel4 = $this->generateScaledSchema($schema, 4, 10);
                        $schemaLevel5 = $this->generateScaledSchema($schema, 5, 20);
                        break;
                    default: // lin, iron, horse
                        $schemaLevel2 = $this->generateScaledSchema($schema, 2, 4);
                        $schemaLevel3 = $this->generateScaledSchema($schema, 3, 8);
                        $schemaLevel4 = $this->generateScaledSchema($schema, 4, 14);
                        $schemaLevel5 = $this->generateScaledSchema($schema, 5, 20);
                        break;
                }
                $this->constructionsSchemas[] = $schemaLevel2;
                $this->constructionsSchemas[] = $schemaLevel3;
                $this->constructionsSchemas[] = $schemaLevel4;
                $this->constructionsSchemas[] = $schemaLevel5;
            }
        }
        array_splice($this->constructionsSchemas, 0, 4);
        $palaceWoodSchema = ['construction' => 'palace', 'level' => 1, 'resource' => 'wood', 'amount' => 15];
        $palaceHammerSchema = ['construction' => 'palace', 'level' => 1, 'resource' => 'remainingHammers', 'amount' => 2];
        array_unshift($this->constructionsSchemas, $palaceHammerSchema, $palaceWoodSchema);
    }

    private function generateScaledSchema($schema, $level, $amountMultiplier): array
    {
        return ['construction' => $schema['construction'], 'level' => $level, 'resource' => $schema['resource'], 'amount' => $schema['amount'] * $amountMultiplier];
    }
}
