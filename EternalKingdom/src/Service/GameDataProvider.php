<?php

namespace App\Service;

use App\Entity\BuiltBuildings;
use App\Entity\Constructions;
use App\Entity\ConstructionSchemas;
use App\Entity\LogSystem\LogEvent;
use App\Entity\MaxStorage;
use App\Entity\ResourceConversionSchemas;
use App\Entity\Resources;
use App\Entity\Storage;
use App\Entity\Titles;
use App\Entity\UnitArmy;
use App\Kingdom\City\Enum\BuildingEnum;
use App\Kingdom\City\Enum\UnitEnum;
use App\Kingdom\Lord\Enum\TitleEnum;
use Doctrine\ORM\EntityManagerInterface;

class GameDataProvider
{
    private array $resourceDict;
    private array $constructionDict;
    private array $titleDict;
    private array $constructionSchemaDict;
    private array $maxStorageDict;
    private array $conversionSchemaDict;
    private array $logEventDict;

    private bool $isLoaded = false;

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function loadDicts(): void
    {
        if (true == $this->isLoaded) {
            return;
        }

        $this->loadResourceDict();
        $this->loadConstructionDict();
        $this->loadTitleDict();
        $this->loadConstructionSchemaDict();
        $this->loadMaxStorageDict();
        $this->loadConversionSchemaDict();
        $this->loadLogEventDict();

        $this->isLoaded = true;
    }

    // Loads an array (resourceName => resourceObject)
    public function loadResourceDict(): void
    {
        $resourceDict = [];
        $resources = $this->entityManager->getRepository(Resources::class)->findAll();
        foreach ($resources as $resource) {
            $resourceDict[$resource->getName()] = $resource;
        }
        $this->resourceDict = $resourceDict;
    }

    public function getResourceDict(): array
    {
        return $this->resourceDict;
    }

    // Loads an array (constructionName => constructionObject)
    private function loadConstructionDict(): void
    {
        $constructionDict = [];
        $constructions = $this->entityManager->getRepository(Constructions::class)->findAll();
        foreach ($constructions as $construction) {
            $constructionDict[$construction->getName()] = $construction;
        }
        $this->constructionDict = $constructionDict;
    }

    /** @return array<Constructions> */
    public function getConstructionDict(): array
    {
        return $this->constructionDict;
    }

    // Loads an array (titleName => titleObject)
    private function loadTitleDict(): void
    {
        $titleDict = [];
        $titles = $this->entityManager->getRepository(Titles::class)->findAll();
        foreach ($titles as $title) {
            $titleDict[$title->getName()] = $title;
        }
        $this->titleDict = $titleDict;
    }

    public function getTitleDict(): array
    {
        return $this->titleDict;
    }

    // Loads an array (constructionName => array(level => [constructionsShemasObjects]))
    private function loadConstructionSchemaDict(): void
    {
        $schemaDict = [];
        $schemas = $this->entityManager->getRepository(ConstructionSchemas::class)->findAll();
        foreach ($schemas as $schema) {
            if (!array_key_exists($schema->getConstruction()->getName(), $schemaDict)) {
                $schemaDict[$schema->getConstruction()->getName()] = [1 => [], 2 => [], 3 => [], 4 => [], 5 => []];
            }
            $schemaDict[$schema->getConstruction()->getName()][$schema->getLevel()][$schema->getResource()->getName()] = $schema;
        }
        $this->constructionSchemaDict = $schemaDict;
    }

    public function getConstructionSchemaDict(): array
    {
        return $this->constructionSchemaDict;
    }

    /** @return array<string,array<int,array{construction: BuildingEnum, level: int, requiredConstruction: BuildingEnum|null, requiredLevel: int|null, requiredTitle: TitleEnum|null}>> */
    public function getConstructionRequirementDict(): array
    {
        // TODO DTO Class
        $constructionRequirementDict = [
            ['construction' => BuildingEnum::PALACE, 'level' => 1, 'requiredConstruction' => null, 'requiredLevel' => null, 'requiredTitle' => null],
            ['construction' => BuildingEnum::FARM, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::STORE, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::MARKET, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::HUT, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::YARD, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::CASERN, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::WALL, 'level' => 1, 'requiredConstruction' => BuildingEnum::CASERN, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::HEADQUARTERS, 'level' => 1, 'requiredConstruction' => BuildingEnum::CASERN, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::WORKSHOP, 'level' => 1, 'requiredConstruction' => BuildingEnum::CASERN, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::GUARD_TOWER, 'level' => 1, 'requiredConstruction' => BuildingEnum::CASERN, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::MILITARY_ACADEMY, 'level' => 1, 'requiredConstruction' => BuildingEnum::CASERN, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::ARCHERY, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::FACTORY, 'level' => 1, 'requiredConstruction' => BuildingEnum::ARCHERY, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::STABLE, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::BUTCHER, 'level' => 1, 'requiredConstruction' => BuildingEnum::STABLE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::FORGE, 'level' => 1, 'requiredConstruction' => BuildingEnum::PALACE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => BuildingEnum::CAULDRON, 'level' => 1, 'requiredConstruction' => BuildingEnum::FORGE, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => UnitEnum::CATAPULT, 'level' => 1, 'requiredConstruction' => BuildingEnum::WORKSHOP, 'requiredLevel' => 1, 'requiredTitle' => null],
            ['construction' => UnitEnum::BALLISTA, 'level' => 1, 'requiredConstruction' => BuildingEnum::WORKSHOP, 'requiredLevel' => 1, 'requiredTitle' => null],
        ];

        // NOTE: I strongly dislike this part. These are simple rules easy to compute on demand
        /**
         * Compute all the construction upgrade requirements from their level 1 requirement.
         * Rule 1 : Palace requires a higher title to be upgraded.
         * Rule 2 : Buildings construction can be upgraded up to the current palace level.
         */
        $titles = TitleEnum::cases();

        foreach ($constructionRequirementDict as $requirement) {
            $construction = $requirement['construction'];

            if (BuildingEnum::PALACE === $construction) {
                for ($level = 2; $level <= 5; ++$level) {
                    $requirement['level'] = $level;
                    $requirement['requiredTitle'] = $titles[2 * $level - 3];
                    $constructionRequirementDict[] = $requirement;
                }
            } else {
                if ($construction instanceof BuildingEnum) {
                    $requirement['requiredConstruction'] = BuildingEnum::PALACE;
                }

                for ($level = 2; $level <= 5; ++$level) {
                    $requirement['level'] = $level;
                    $requirement['requiredLevel'] = $level;
                    $constructionRequirementDict[] = $requirement;
                }
            }
        }

        return array_reduce(
            $constructionRequirementDict,
            function (array $carry, mixed $item) {
                $carry[$item['construction']->value][$item['level']] = $item;

                return $carry;
            },
            []
        );
    }

    // Loads an array (resourceName => array(level => MaxStorageObject))
    private function loadMaxStorageDict(): void
    {
        $maxStorageDict = ['wheat' => [], 'wood' => [], 'iron' => [], 'horse' => [], 'lin' => []];
        $maxStorages = $this->entityManager->getRepository(MaxStorage::class)->findAll();
        foreach ($maxStorages as $maxStorage) {
            $maxStorageDict[$maxStorage->getResource()->getName()][$maxStorage->getLevel()] = $maxStorage;
        }
        $this->maxStorageDict = $maxStorageDict;
    }

    public function getMaxStorageDict(): array
    {
        return $this->maxStorageDict;
    }

    // Loads an array (producedResourceName => array(level => [constructionsShemasObjects]))
    private function loadConversionSchemaDict(): void
    {
        $schemaDict = [];
        $schemas = $this->entityManager->getRepository(ResourceConversionSchemas::class)->findAll();
        foreach ($schemas as $schema) {
            if (!array_key_exists($schema->getProducedResource()->getName(), $schemaDict)) {
                $schemaDict[$schema->getProducedResource()->getName()] = ['building' => $schema->getRequiredBuilding()->getName(), 1 => [], 2 => [], 3 => [], 4 => [], 5 => []];
            }
            $schemaDict[$schema->getProducedResource()->getName()][$schema->getRequiredLevel()][$schema->getRequiredResource()->getName()] = $schema;
        }
        $this->conversionSchemaDict = $schemaDict;
    }

    public function getConversionSchemaDict(): array
    {
        return $this->conversionSchemaDict;
    }

    private function loadLogEventDict(): void
    {
        $dict = [];
        $logEvents = $this->entityManager->getRepository(LogEvent::class)->findAll();
        foreach ($logEvents as $logEvent) {
            // if(array_key_exists($logEvent->getEventName(), $dict))
            //    throw new Exception("Duplicated log event name found: " . $logEvent->getEventName() . " while loading Log Event...");

            $dict[$logEvent->getEventName()] = $logEvent;
        }

        $this->logEventDict = $dict;
    }

    public function getLogEventDict(): array
    {
        return $this->logEventDict;
    }

    public function createBuilding(string $buildingName, int $level): BuiltBuildings
    {
        return new BuiltBuildings($this->constructionDict[$buildingName], $level);
    }

    public function createStorage(string $resourceName, int $amount): Storage
    {
        return new Storage($this->resourceDict[$resourceName], $amount);
    }

    public function createArmyUnit(string $unitName, int $amount): UnitArmy
    {
        return new UnitArmy($this->resourceDict[$unitName], $amount);
    }
}
