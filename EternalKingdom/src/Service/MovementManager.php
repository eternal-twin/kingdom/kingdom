<?php

namespace App\Service;

use App\Entity\General;
use App\Entity\Lord;
use App\Entity\Relation;
use App\Entity\World\MovementOrder;
use App\Entity\World\MovementSection;
use App\Entity\World\WorldMapNode;
use App\Enum\DiplomacyType;
use App\Kingdom\CheatSystem\CheatConfiguration;
use App\Repository\RelationRepository;
use App\Repository\World\MovementOrderRepository;
use App\Repository\World\WorldMapNodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class MovementManager
{
    public const int K_BASE_TRAVEL_DURATION = 60;
    public const int K_NO_ACCESS_TRAVEL_MULTIPLIER = 3;

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function validateGeneralBeforeMoving(General $general): bool
    {
        if (null != $general->getMovementOrder() || $general->isFortifying()) {
            return false;
        }

        return $general->hasAnArmy() || CheatConfiguration::CHEAT_ALLOW_MOVE_WITHOUT_ARMY;
    }

    /**
     * @param WorldMapNode[] $path
     */
    public function validatePathBeforeMoving(array $path): bool
    {
        if (count($path) > 1) {
            $isAdjacent = true;
            for ($i = 0; $i < count($path) - 1 && $isAdjacent; ++$i) {
                $isAdjacent &= $path[$i]->doesAdjacencyExist($path[$i + 1]);
            }

            return $isAdjacent;
        }

        return false;
    }

    /**
     * @param int[] $path (list of WorldNodeData graph indexes)
     */
    public function startMovement(General $general, array $path): bool
    {
        if (!$this->validateGeneralBeforeMoving($general)) {
            dump('General is not ready to move! See rules in validateGeneral');

            return false;
        }

        /** @var WorldMapNodeRepository $worldMapNodeRepo */
        $worldMapNodeRepo = $this->entityManager->getRepository(WorldMapNode::class);
        $nodesFromPath = $worldMapNodeRepo->findNodesRelatedToPath($general->getOwner()->getWorldMap(), $path);

        if (!$this->validatePathBeforeMoving($nodesFromPath)) {
            dump('Path is not valid! See rules in validatePathBeforeMoving');

            return false;
        }

        $movementOrder = $this->createMovementOrder($general, $nodesFromPath);
        if (null == $movementOrder) {
            dump('Failed to create movement order');

            return false;
        }

        $general->setMovementOrder($movementOrder);
        $this->entityManager->flush();

        return true;
    }

    public function revertMovement(General $general): void
    {
        $currentSectionInfo = $general->getMovementOrder()->getCurrentSectionInfo();
        /** @var MovementSection $currentSection */
        $currentSection = $general->getMovementOrder()->getPath()[$currentSectionInfo['index']];

        // TODO Try catch with rollback if something got wrong?
        $this->entityManager->beginTransaction();

        $order = $general->getMovementOrder();
        foreach ($order->getPath() as $section) {
            $this->entityManager->remove($section);
        }
        $this->entityManager->remove($order);
        $general->setMovementOrder(null);
        $this->entityManager->persist($general);
        $this->entityManager->flush();

        $newOrder = $this->createMovementOrder($general, [$currentSection->getEnd(), $currentSection->getStart()]);
        $newOrder->setInitialProgression(1.0 - $currentSectionInfo['percentage']);
        $general->setMovementOrder($newOrder);
        $this->entityManager->flush();

        $this->entityManager->commit();
    }

    /**
     * Check if we can cross the specified node. (aka DDP).
     */
    private function canCross(Lord $lord, WorldMapNode $node): bool
    {
        // TODO Test all of these cases!
        $owningLord = $node->getSovereign() ?: $node->getOwner();
        $canCross = true;

        $isPlaceDefended = $node->getArmyContainer()->hasAnyUnits();

        if ($owningLord instanceof Lord) {
            // if the owner have a wall, don't allow passage
            if ($owningLord->getCity()->getBuildingLevel('wall') > 0 && $lord !== $owningLord) {
                // if we have the cross right for this territory it's fine
                /** @var RelationRepository $relationRepo */
                $relationRepo = $this->entityManager->getRepository(Relation::class);
                $relation = $relationRepo->findOneBy(['target' => $owningLord]);
                if (null !== $relation) {
                    $canCross = $relation->canCross();
                } else {
                    if ($node->getOwner() instanceof Lord) {
                        $isPlaceDefended = $node->getOwner()->getCity()->getDefense()->hasAnyUnits();
                    }

                    // if this territory is defended, don't allow passage
                    if ($isPlaceDefended || $node->isAnyFortifyingGeneralReady()) {
                        $canCross = false;
                    }
                }
            }
        } else {
            // if there are barbarians units, don't allow passage
            $canCross = !$isPlaceDefended;
        }

        return $canCross;
    }

    /**
     * Evaluate the travel duration of the specified MovementSection.
     *
     * @return bool Can we continue after this node?
     */
    private function evaluateMovementSection(Lord $instigator, MovementSection $section): bool
    {
        $destination = $section->getEnd();

        $isTerritoryEmpty = !$destination->getOwner() instanceof Lord && !$destination->getSovereign() instanceof Lord;
        $canCrossDestination = $this->canCross($instigator, $destination);

        if ($isTerritoryEmpty) {
            $isLongTravel = true;
        } else {
            $canCrossOrigin = $this->canCross($instigator, $section->getStart());
            $isLongTravel = !($canCrossOrigin && $canCrossDestination);
        }

        $sectionTravelDuration = $this::K_BASE_TRAVEL_DURATION * $instigator->getWorldMap()->getMapSpeedMultiplier();
        if ($isLongTravel) {
            $sectionTravelDuration *= $this::K_NO_ACCESS_TRAVEL_MULTIPLIER;
        }

        $section->setTravelDuration($sectionTravelDuration);

        return $canCrossDestination;
    }

    public function verifyAllPendingMovements(): void
    {
        /** @var MovementOrderRepository $movementOrderRepo */
        $movementOrderRepo = $this->entityManager->getRepository(MovementOrder::class);
        // TODO Should we do that per world to light the cost of resolving everything?
        $pendingMovementOrders = $movementOrderRepo->findAll();

        $hasAtLeastOneComplete = false;
        foreach ($pendingMovementOrders as $movementOrder) {
            $hasAtLeastOneComplete |= $this->verifyMovementStatus($movementOrder);
        }

        // TODO Understand why we update ALL the node data for no reason (450 queries)!
        if ($hasAtLeastOneComplete) {
            $this->entityManager->flush();
        }
    }

    private function verifyMovementStatus(MovementOrder $movementOrder): bool
    {
        // TODO Recursive call until the movement is complete OR still pending (See the original source code).

        $isMovementFinished = $this->shouldEndMovement($movementOrder);
        if ($this->shouldEndMovement($movementOrder)) {
            $this->endMovement($movementOrder);
        }

        return $isMovementFinished;
    }

    private function shouldEndMovement(MovementOrder $movementOrder): bool
    {
        if (CheatConfiguration::CHEAT_INSTANT_MOVE) {
            return true;
        }

        // TODO Interrupt the movement and move the general at the relevant location depending rights

        return $movementOrder->getRemainingTravelDuration() <= 0;
    }

    /**
     * @param WorldMapNode[] $path
     */
    private function createMovementOrder(General $general, array $path): ?MovementOrder
    {
        $movementOrder = null;
        $movementSections = new ArrayCollection();

        $canContinue = true;
        $lastIndex = count($path) - 1;
        for ($i = 0; $i < $lastIndex && $canContinue; ++$i) {
            $movementSection = new MovementSection($path[$i], $path[$i + 1]);
            $canContinue = $this->evaluateMovementSection($general->getOwner(), $movementSection);
            $movementSections->add($movementSection);
        }

        if (count($movementSections) > 0) {
            $movementOrder = new MovementOrder($general, $movementSections);
            $this->entityManager->persist($movementOrder);
        }

        return $movementOrder;
    }

    private function endMovement(MovementOrder $movementOrder): void
    {
        $instigatorGeneral = $movementOrder->getInstigator();

        $instigatorGeneral->setLocation($movementOrder->getDestination());
        $instigatorGeneral->setMovementOrder(null);

        $this->entityManager->remove($movementOrder);
        $this->entityManager->persist($instigatorGeneral);

        /** @var RelationRepository $relationRepository */
        $relationRepository = $this->entityManager->getRepository(Relation::class);
        $relation = $relationRepository->findOneBy(
            ['lord' => $instigatorGeneral->getOwner(), 'target' => $movementOrder->getDestination()->getSovereign()]
        );

        if (null != $relation && DiplomacyType::TYPE_ENEMY === $relation->getDiplomacyType()) {
            // TODO Auto-Attack!
        }
    }
}
