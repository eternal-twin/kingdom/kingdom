<?php

namespace App\Service;

use App\Entity\Battle\Battle;
use App\Entity\Battle\EngagedArmy;
use App\Entity\City;
use App\Entity\General;
use App\Entity\Lord;
use App\Entity\Relation;
use App\Entity\User;
use App\Entity\World\WorldMap;
use App\Enum\HealthType;
use App\Enum\LogEventType;
use App\Kingdom\CheatSystem\CheatConfiguration;
use App\Kingdom\City\Enum\BuildingEnum;
use App\Kingdom\Lord\Enum\TitleEnum;
use App\Kingdom\Ruleset\Service\GetRuleset;
use App\Repository\Battle\BattleRepository;
use App\Repository\RelationRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Lord Manager aims to :
 * - Generate a new Lord when the player joins a game and assign it to the user
 * - Generate the main city to play with
 * - Update Lord states
 */
class LordManager
{
    /**
     * LordProvider constructor.
     */
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly GameDataProvider $gameDataProvider,
        private readonly WorldManager $worldManager,
        private readonly GameLogManager $gameLogManager,
        private readonly GetRuleset $getRuleset,
        UserService $userService
    ) {
    }

    /**
     * Creates a new Lord and assigns it to the user that requested it!
     */
    public function createLord(User $user): Lord
    {
        $this->gameDataProvider->loadDicts();
        $ruleset = $this->getRuleset->exec();

        $lord = new Lord($user, $ruleset);
        $lord->setName($user->getDisplayName());

        if (CheatConfiguration::CHEAT_CUSTOM_LORD) {
            $this->cheatOverrideLord($lord);
        }

        return $lord;
    }

    /**
     * Tries to create a city for the specified Lord.
     */
    public function createCity(Lord $owningLord, WorldMap $world): ?City
    {
        $spawnNode = $this->worldManager->getRandomAvailableCity($world->getId());

        if (!$spawnNode instanceof \App\Entity\World\WorldMapNode) {
            return null;
        }

        $city = new City();
        $city->setNode($spawnNode);
        $city->addStorage($this->gameDataProvider->createStorage('wheat', 30));
        $city->addStorage($this->gameDataProvider->createStorage('farmer', 1));
        $city->addStorage($this->gameDataProvider->createStorage('remainingHammers', 1));

        // We now own this place since we spawned on it and we also are our own sovereign.
        $spawnNode->setOwner($owningLord);
        // Maybe this is not needed and we should find another way to display our capital city in our kingdom...
        $owningLord->addTerritory($spawnNode);
        $owningLord->setCity($city);

        if (CheatConfiguration::CHEAT_CUSTOM_CITY) {
            $this->cheatOverrideCity($city);
        }

        if (CheatConfiguration::CHEAT_CUSTOM_ARMY) {
            $this->cheatOverrideArmy($owningLord, $city);
        }

        return $city;
    }

    /**
     * Returns true if the lord should die right now.
     */
    public function shouldLordDie(Lord $lord): bool
    {
        assert($lord);

        return $lord->isDying();
    }

    /**
     * TODO Displays the option in management page if this is enabled!
     */
    public function isForceDeathEnabled(): bool
    {
        return CheatConfiguration::CHEAT_FORCE_DEATH_ENABLED;
    }

    /**
     * Kill a Lord and clean stuff on its world map.
     */
    public function triggerLordDeath(Lord $lord): void
    {
        if ($lord->isDead()) {
            return;
        }

        $objectManager = $this->managerRegistry->getManager();

        $lord->setHealth(HealthType::TYPE_DEAD);

        // TODO Service for Territory cleanup, Cleanup battle, units, owner

        // Remove Linked Army on owned Territories.
        /** @var BattleRepository $battleRepo */
        $battleRepo = $this->managerRegistry->getRepository(Battle::class);
        $pendingBattles = $battleRepo->findPendingBattleOnTerritoriesOwnedBy($lord);
        foreach ($pendingBattles as $pendingBattle) {
            /** @var EngagedArmy $engagedArmy */
            $engagedArmy = array_filter($pendingBattle->getDefensingArmies(),
                fn ($engagedArmy) => $engagedArmy->getLinkedArmy()->getId() === $pendingBattle->getLocation()->getArmyContainer()->getId()
            );
            if (1 === count($engagedArmy)) {
                $engagedArmy[0]->clearLinkedArmy();
                $objectManager->persist($engagedArmy[0]);
            }
        }

        // Cleanup owned territories.
        foreach ($lord->getKingdom() as $territory) {
            // TODO Army is now Barbare but is reduced!
            $territory->getArmyContainer()->clearAllUnits();
            $territory->setSovereign(null);

            $objectManager->persist($territory);
        }

        $lord->getSpawnNode()->setOwner(null);
        $objectManager->persist($lord->getSpawnNode());

        // TODO move this to a service : Kill General!
        // Ensure every Movements are cleaned!
        // Ensure every Battles are cleaned!
        foreach ($lord->getGenerals() as $general) {
            $pendingBattle = $general->getBattle();
            if (null !== $pendingBattle) {
                /** @var EngagedArmy $engagedArmy */
                $engagedArmy = $pendingBattle->getAllEngagedArmies()->findFirst(
                    fn (int $i, EngagedArmy $army) => $army->getLinkedArmy()->getId() === $general->getArmyContainer()->getId()
                );
                if (null !== $engagedArmy) {
                    $engagedArmy->clearLinkedArmy();
                    $objectManager->persist($engagedArmy);
                }
            }

            $objectManager->remove($general);
        }

        $lord->clearGenerals();
        $objectManager->persist($lord);

        /** @var RelationRepository $relationRepo */
        $relationRepo = $this->managerRegistry->getRepository(Relation::class);
        $relationRepo->deleteAllRelations($lord);

        $objectManager->flush();

        $this->logDeath($lord);
    }

    /**
     * Called after lord is flush into DB else it creates errors!
     */
    public function logSpawn(Lord $lord): void
    {
        $this->gameLogManager->beginLogTransaction();
        $this->gameLogManager->addKingdomLog(
            $lord,
            LogEventType::MANAGEMENT_SPAWN,
            false,
            [
                '%worldId%' => $lord->getWorldMap()->getId(),
                '%nodeId%' => $lord->getSpawnNode()->getId(),
                '%nodeName%' => $lord->getSpawnNode()->getName(),
            ]
        );
        $this->gameLogManager->addWorldLog(
            $lord->getWorldMap(),
            LogEventType::WORLD_SPAWN,
            false,
            [
                '%userId%' => $lord->getUser()->getId(),
                '%lord%' => $lord->getName(),
                '%worldId%' => $lord->getWorldMap()->getId(),
                '%nodeId%' => $lord->getSpawnNode()->getId(),
                '%nodeName%' => $lord->getSpawnNode()->getName(),
            ]
        );
        $this->gameLogManager->finishLogTransaction();
    }

    public function logDeath(Lord $lord): void
    {
        // TODO add reason as function arg to distinct death
        $this->gameLogManager->beginLogTransaction();
        $this->gameLogManager->addKingdomLog($lord, LogEventType::MANAGEMENT_DEATH_INACTIVITY, false);
        $this->gameLogManager->addWorldLog(
            $lord->getWorldMap(),
            LogEventType::WORLD_DEATH_INACTIVITY,
            false,
            [
                '%title%' => $lord->getTitle(),
                '%userId%' => $lord->getUser()->getId(),
                '%lord%' => $lord->getName(),
                '%worldId%' => $lord->getWorldMap()->getId(),
                '%nodeId%' => $lord->getSpawnNode()->getId(),
                '%nodeName%' => $lord->getSpawnNode()->getName(),
            ]
        );
        $this->gameLogManager->finishLogTransaction();
    }

    /**
     * Refresh all Lords States (Turn System, Inactive Lords, ...)
     * TODO Don't do that too often!
     * Could be a cron or maybe DB could store timestamp?
     */
    public function refreshAllLords(): void
    {
        // Only do that every 5 minutes ?! Adapt depending world speed ?!
        $this->refreshTurnsOnAllAliveLords();

        // Only do that every hour ?!
        $this->killAllInactiveLords();
    }

    private function refreshTurnsOnAllAliveLords(): void
    {
        // TODO Turn System Update !
        // 1. increment turn count according the last update time
        // 2. Any overflowing turn should increment waste turn
        // 3. If waste turn reaches max amount, raise the flag inactive !
    }

    private function killAllInactiveLords(): void
    {
        if (CheatConfiguration::CHEAT_IGNORE_LORD_INACTIVITY) {
            return;
        }

        /** @var UserRepository $userRepo */
        $userRepo = $this->managerRegistry->getRepository(User::class);
        $inactiveUsers = $userRepo->findAllInactivePlayers();

        /** @var User $user */
        foreach ($inactiveUsers as $user) {
            $this->triggerLordDeath($user->getLord());
        }
    }

    // Cheats helpers

    /**
     * Override a Lord with a prebuilt list of buildings, some population and armies.
     */
    private function cheatOverrideLord(Lord $lord): void
    {
        $lordReflection = new \ReflectionClass(Lord::class);

        $lord->setAgeMonth(0);
        $lord->setAgeYear(30);
        $lordReflection->getProperty('title')->setValue($lord, TitleEnum::MARQUIS->value);
        $lord->setHealth('excellent');
        $lordReflection->getProperty('glory')->setValue($lord, 17);
        $lordReflection->getProperty('apogee')->setValue($lord, 20);
    }

    /**
     * Override the City with a prebuilt list of buildings, resources and populations.
     */
    private function cheatOverrideCity(City $city): void
    {
        $levelForAllBuildings = 2;
        foreach (BuildingEnum::cases() as $buildingName) {
            $city->addBuiltBuilding($this->gameDataProvider->createBuilding($buildingName->value, $levelForAllBuildings));
        }

        $storage = $city->findStorage('wheat');
        $storage->setAmount(250);
        $city->addStorage($this->gameDataProvider->createStorage('gold', 1000));
        $city->addStorage($this->gameDataProvider->createStorage('wood', 300));
        // $city->addStorage($this->gameDataProvider->createStorage('iron', 0));
        // $city->addStorage($this->gameDataProvider->createStorage('lin', 0));
        // $city->addStorage($this->gameDataProvider->createStorage('horse', 0));

        $storage = $city->findStorage('farmer');
        $storage->setAmount(5);
        // $city->getStorages($this->gameDataProvider->createStorage('citizen', 5));

        $city->addHouses(2 + random_int(0, 4));
    }

    /**
     * Override the City with a prebuilt list of armies in the owned city and Generals.
     */
    private function cheatOverrideArmy(Lord $lord, City $city): void
    {
        $city->getDefense()->addUnit($this->gameDataProvider->createArmyUnit('soldier', 7));
        $city->getDefense()->addUnit($this->gameDataProvider->createArmyUnit('archer', 3));
        $city->getDefense()->addUnit($this->gameDataProvider->createArmyUnit('paladin', 10));
        $city->getDefense()->addUnit($this->gameDataProvider->createArmyUnit('ballista', 101));
        $city->getDefense()->addUnit($this->gameDataProvider->createArmyUnit('knight', 101));
        $city->getDefense()->addUnit($this->gameDataProvider->createArmyUnit('mountedArcher', 17));

        $general1 = new General('ORM', $city->getNode());
        $general1->setReputation(12);
        $general1->getArmyContainer()->addUnit($this->gameDataProvider->createArmyUnit('soldier', 7));
        $general1->getArmyContainer()->addUnit($this->gameDataProvider->createArmyUnit('catapult', 12));
        $general1->getArmyContainer()->addUnit($this->gameDataProvider->createArmyUnit('paladin', 23));
        $lord->addGeneral($general1);

        $lord->addGeneral(new General('Symfony', $city->getNode()));
    }
}
