<?php

// src/Service/GridManager.php

namespace App\Service;

use App\Entity\City;
use App\Kingdom\CheatSystem\CheatConfiguration;
use App\Model\Alignment;
use App\Model\GridChainReactionStep;
use App\Model\SwapCandidate;
use App\Model\Tile;
use App\Model\TileRespawnInfo;

/**
 * Grid Manager aims to:
 * - initializes randomized grid
 * - Trigger swap and apply chain reaction (alignment, gravity, respawn)
 */
class GridManager
{
    public const int K_GRID_SIZE = 8;

    // Buffers for all calculations
    private City $city;
    private array $gridData;

    /**
     * @var array<Tile>
     */
    private array $tilesToCheck;

    /**
     * @var array<GridChainReactionStep> Chain reaction information that is sent to the client after each swap
     */
    private array $steps = [];
    private int $currentStep;

    /**
     * Initialize a grid (elements are generated thanks to the City Data).
     */
    public function initGrid(City $city, bool $useStaticGrid = false): array
    {
        $this->city = $city;

        if (CheatConfiguration::CHEAT_USE_STATIC_GRID || $useStaticGrid) {
            $this->loadStaticGrid();
        } else {
            $this->randomizeGrid();
        }

        return $this->gridData;
    }

    /**
     * Tries to execute the specified swap
     * Returns if the swap is valid or not.
     * If the swap is valid, this triggers a chain reaction and stores every intermediary steps.
     */
    public function swap(City &$city, SwapCandidate $swapCandidate): bool
    {
        $this->city = &$city;
        $this->gridData = array_map(fn ($x) => (int) $x, str_split((string) $city->getGrid()));

        if (!$this->isValidSwap($swapCandidate) && !CheatConfiguration::CHEAT_ALLOW_INVALID_GRID_SWAP) {
            return false;
        }

        [$row1, $col1] = [$swapCandidate->tile1->row, $swapCandidate->tile1->col];
        [$row2, $col2] = [$swapCandidate->tile2->row, $swapCandidate->tile2->col];
        $this->swapTiles($row1, $col1, $row2, $col2);
        $this->steps = [];
        $this->currentStep = 0;
        $this->tilesToCheck = [$swapCandidate->tile1, $swapCandidate->tile2];
        $this->chainReaction();

        return true;
    }

    /**
     * Find ValidAlignments after the Swap swapCandidate.
     */
    private function findValidAlignments(SwapCandidate $swapCandidate): array
    {
        [$row1, $col1] = [$swapCandidate->tile1->row, $swapCandidate->tile1->col];
        [$row2, $col2] = [$swapCandidate->tile2->row, $swapCandidate->tile2->col];
        $this->swapTiles($row1, $col1, $row2, $col2);
        $alignment1 = $this->findAlignmentInBothDirections($row1, $col1);
        $alignment2 = $this->findAlignmentInBothDirections($row2, $col2);
        $validAlignments = [];
        foreach ([$alignment1, $alignment2] as $alignment) {
            if ($alignment->isValidAlignment()) {
                $validAlignments[] = $alignment;
            }
        }
        $this->swapTiles($row1, $col1, $row2, $col2); // We put the tiles back in their original place

        return $validAlignments;
    }

    /**
     * Swap the two specified tiles.
     */
    private function swapTiles(int $r1, int $c1, int $r2, int $c2): void
    {
        $buffer = $this->gridData[$r1 * GridManager::K_GRID_SIZE + $c1];
        $this->gridData[$r1 * GridManager::K_GRID_SIZE + $c1] = $this->gridData[$r2 * GridManager::K_GRID_SIZE + $c2];
        $this->gridData[$r2 * GridManager::K_GRID_SIZE + $c2] = $buffer;
    }

    /**
     * RECURSIVE CALL
     * Find if we still have alignments to clean in the grid
     * For each alignment found: remove all tiles, apply gravity & respawn new elements.
     */
    private function chainReaction(): void
    {
        $allAlignments = $this->findAllValidAlignments();
        if (0 == count($allAlignments)) {
            $this->endChainReaction();

            return;
        }

        $this->tilesToCheck = [];
        $this->steps[$this->currentStep] = new GridChainReactionStep();
        $this->clearAlignments($allAlignments);
        $this->applyGravity();
        $this->respawnElements();
        ++$this->currentStep;
        $this->chainReaction();
    }

    /**
     * Find all the Alignments in the grid from bottom to top.
     */
    private function findAllValidAlignments(): array
    {
        $gridCopy = $this->gridData;   // Buffer
        $alignments = [];
        $sortedTiles = $this->sortedTilesByNeighbors();
        foreach ($sortedTiles as $tileList) {
            foreach ($tileList as $tileToCheck) {
                $alignment = $this->findAlignmentInBothDirections($tileToCheck->row, $tileToCheck->col);
                if ($alignment->isValidAlignment()) {
                    $alignments[] = $alignment;
                    foreach ($alignment->tiles as $tile) {
                        $this->gridData[GridManager::K_GRID_SIZE * $tile->row + $tile->col] = -1;
                    }
                }
            }
        }
        $this->gridData = $gridCopy; // Restore Grid, TODO : Put Grid in argument to low-level functions

        return $alignments;
    }

    /**
     * Returns a list of tiles, sorted by number of same element neihbors.
     */
    private function sortedTilesByNeighbors(): array
    {
        $sortedTiles = [4 => [], 3 => [], 2 => [], 1 => []];
        foreach ($this->tilesToCheck as $tileToCheck) {
            $neighbors = 0;
            $elem = $this->getElementAt($tileToCheck->row, $tileToCheck->col);
            if ($tileToCheck->row > 0 && $this->getElementAt($tileToCheck->row - 1, $tileToCheck->col) === $elem) {
                ++$neighbors;
            }
            if ($tileToCheck->row < GridManager::K_GRID_SIZE - 1 && $this->getElementAt($tileToCheck->row + 1, $tileToCheck->col) === $elem) {
                ++$neighbors;
            }
            if ($tileToCheck->col > 0 && $this->getElementAt($tileToCheck->row, $tileToCheck->col - 1) === $elem) {
                ++$neighbors;
            }
            if ($tileToCheck->col < GridManager::K_GRID_SIZE - 1 && $this->getElementAt($tileToCheck->row, $tileToCheck->col + 1) === $elem) {
                ++$neighbors;
            }
            if ($neighbors > 0) {
                $sortedTiles[$neighbors][] = $tileToCheck;
            }
        }

        return $sortedTiles;
    }

    /**
     * Empty all specified alignments' tiles.
     */
    private function clearAlignments(array $alignments): void
    {
        foreach ($alignments as $alignment) {
            $this->steps[$this->currentStep]->addAlignment($alignment);
            foreach ($alignment->tiles as $tile) {
                $this->gridData[$tile->row * GridManager::K_GRID_SIZE + $tile->col] = -1;
            }
        }
    }

    /**
     * Apply gravity in the whole grid.
     * For each column, move all occupied tiles at the lowest possible empty position.
     */
    private function applyGravity(): void
    {
        for ($col = 0; $col < GridManager::K_GRID_SIZE; ++$col) {
            $currentEmptyRow = -1;
            for ($row = GridManager::K_GRID_SIZE - 1; $row >= 0; --$row) {
                $elem = $this->getElementAt($row, $col);
                if (-1 == $currentEmptyRow && -1 == $elem) {
                    $currentEmptyRow = $row;
                    $this->tilesToCheck[] = new Tile($currentEmptyRow, $col);
                } elseif (-1 != $currentEmptyRow && -1 != $elem) {
                    $this->swapTiles($row, $col, $currentEmptyRow, $col);
                    --$currentEmptyRow;
                    $this->tilesToCheck[] = new Tile($currentEmptyRow, $col);
                }
            }
        }
    }

    /**
     * Respawns a new element in all empty tiles of the grid and stores all the respawn information.
     */
    private function respawnElements(): void
    {
        $probabilities = $this->getGridProbabilities();

        for ($row = 0; $row < GridManager::K_GRID_SIZE; ++$row) {
            for ($col = 0; $col < GridManager::K_GRID_SIZE; ++$col) {
                if (-1 == $this->getElementAt($row, $col)) {
                    $elem = $this->getRandomElement($probabilities);
                    $this->tilesToCheck[] = new Tile($row, $col);
                    $this->gridData[$row * GridManager::K_GRID_SIZE + $col] = $elem;
                    $this->steps[$this->currentStep]->addRespawn(new TileRespawnInfo($row, $col, $elem));
                }
            }
        }
    }

    /**
     * Triggered once a grid chain reaction ended.
     */
    private function endChainReaction(): void
    {
        if ([] === $this->findAllValidSwaps() || CheatConfiguration::CHEAT_ALWAYS_TRIGGER_GRID_RESET) {
            $this->initGrid($this->city);
            $string = static::convertGridToString($this->gridData);
            $this->city->setGrid($string);
            $this->steps[] = $string;
        } else {
            $string = static::convertGridToString($this->gridData);
            $this->city->setGrid($string);
        }
    }

    /**
     * Returns valid swaps in the whole grid.
     */
    private function findAllValidSwaps(): array
    {
        $allValidSwaps = [];
        for ($row = 0; $row < GridManager::K_GRID_SIZE; $row += 2) {
            for ($col = 0; $col < GridManager::K_GRID_SIZE; ++$col) {
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row + 1, $col));
                if ($this->IsValidSwap($swapCandidate)) {
                    $allValidSwaps[] = $swapCandidate;
                }
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row - 1, $col));
                if ($this->IsValidSwap($swapCandidate)) {
                    $allValidSwaps[] = $swapCandidate;
                }
            }
        }
        for ($col = 0; $col < GridManager::K_GRID_SIZE; $col += 2) {
            for ($row = 0; $row < GridManager::K_GRID_SIZE; ++$row) {
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row, $col + 1));
                if ($this->IsValidSwap($swapCandidate)) {
                    $allValidSwaps[] = $swapCandidate;
                }
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row, $col - 1));
                if ($this->IsValidSwap($swapCandidate)) {
                    $allValidSwaps[] = $swapCandidate;
                }
            }
        }

        return $allValidSwaps;
    }

    /**
     * Returns if the specified swap is valid.
     */
    private function isValidSwap(SwapCandidate $swapCandidate): bool
    {
        if ($this->isTileInGrid($swapCandidate->tile1) && $this->isTileinGrid($swapCandidate->tile2)) {
            $validAlignments = $this->findValidAlignments($swapCandidate);
            if ([] !== $validAlignments) {
                return true;
            }
        }

        return false;
    }

    private function isTileInGrid(Tile $tile): bool
    {
        return 0 <= $tile->row && $tile->row < GridManager::K_GRID_SIZE
            && 0 <= $tile->col && $tile->col < GridManager::K_GRID_SIZE;
    }

    /**
     * Load a grid with static values.
     */
    private function loadStaticGrid(): void
    {
        // Please make sure, this grid has no alignment!
        // Note: This setup helps to reproduce easily some bugs.

        // $this->gridData =
        // [
        //     3, 1, 2, 3, 6, 3, 3, 6,
        //     5, 5, 0, 6, 0, 2, 3, 3,
        //     2, 2, 3, 3, 2, 6, 1, 0,
        //     2, 2, 1, 1, 2, 6, 3, 2,
        //     6, 5, 4, 3, 0, 1, 3, 5,
        //     0, 4, 5, 6, 5, 0, 1, 0,
        //     3, 6, 1, 3, 1, 1, 0, 5,
        //     0, 1, 3, 3, 0, 4, 3, 6,
        // ];

        $this->gridData =
            [
                6, 6, 0, 1, 2, 3, 4, 5,
                0, 1, 2, 3, 4, 5, 5, 6,
                6, 6, 2, 1, 5, 3, 4, 5,
                0, 2, 1, 2, 2, 6, 5, 5,
                6, 6, 0, 4, 6, 4, 6, 6,
                0, 1, 2, 3, 4, 6, 1, 4,
                6, 6, 0, 1, 2, 3, 4, 5,
                0, 1, 2, 3, 4, 5, 6, 6,
            ];
    }

    /**
     * Randomizes the grid values while avoiding creating any alignments.
     */
    private function randomizeGrid(): void
    {
        $this->gridData = array_fill(0, 64, -1);

        $probabilities = $this->getGridProbabilities();
        for ($row = 0; $row < GridManager::K_GRID_SIZE; ++$row) {
            for ($col = 0; $col < GridManager::K_GRID_SIZE; ++$col) {
                do {
                    $this->gridData[$row * GridManager::K_GRID_SIZE + $col] = $this->getRandomElement($probabilities);
                    $isCreatingAlignment = $this->findAlignmentInBothDirections($row, $col)->isValidAlignment();
                } while ($isCreatingAlignment);
            }
        }
    }

    /**
     * Evaluates current probabilities of the grid.
     *
     * @return float[]
     */
    private function getGridProbabilities(): array
    {
        $probabilities = [
            30,             // Wheat
            30,             // Gold
            30,             // Wood
            30,             // Build
            1,              // Army
            1,              // Citizen
            20,             // Replay
        ];
        $population = $this->city->getPopulationCount();
        $squaredPopulation = ceil($population ** 2);

        if ($this->city->getStorageCount('gold') >= ($squaredPopulation >> 1)) {
            $probabilities[4] = 15;
        }

        if ($this->city->getStorageCount('wheat') >= $squaredPopulation) {
            $probabilities[5] = 25 - ($population >> 2);
        }

        if ($population >= 100) {
            $probabilities[5] = 0;
        }

        return $probabilities;
    }

    /**
     * Returns the next element to generate in the grid.
     *
     * @return int 0 to 7
     */
    private function getRandomElement(array $probabilities): int
    {
        // We could calculate this only once?!
        $total = 0;
        foreach ($probabilities as $p) {
            $total += $p;
        }

        $rand = random_int(0, $total - 1);
        foreach ($probabilities as $key => $p) {
            $rand -= $p;
            if ($rand < 0) {
                return $key;
            }
        }

        throw new \LogicException('Failed to randomize next element');
    }

    /**
     * Get the element at the specified coordinates.
     */
    private function getElementAt($row, $col): int
    {
        return $this->gridData[$row * GridManager::K_GRID_SIZE + $col];
    }

    /**
     * Try to find a list of tiles with a valid alignment.
     * Prioritize the longer alignment or the horizontal if length is the same.
     */
    private function findAlignmentInBothDirections($row, $col): Alignment
    {
        $elem = $this->getElementAt($row, $col);

        // Probably clean by another alignment // BE CARE!
        if (-1 === $elem) {
            return Alignment::InvalidAlignment();
        }

        $alignmentH = $this->findHorizontalAlignment($row, $col);
        $alignmentV = $this->findVerticalAlignment($row, $col);

        return $this->getBestAlignment($alignmentH, $alignmentV);
    }

    /**
     * Returns a horizontal alignment of the same tile element from the specified coordinates.
     */
    private function findHorizontalAlignment(int $row, int $col): Alignment
    {
        $elementToSearch = $this->getElementAt($row, $col);

        if (-1 === $elementToSearch) {
            return Alignment::InvalidAlignment();
        }

        $tiles = [];
        $tiles[] = new Tile($row, $col);

        for ($j = $col + 1; $j < GridManager::K_GRID_SIZE; ++$j) {
            if ($this->getElementAt($row, $j) !== $elementToSearch) {
                break;
            }

            $tiles[] = new Tile($row, $j);
        }

        for ($j = $col - 1; $j >= 0; --$j) {
            if ($this->getElementAt($row, $j) !== $elementToSearch) {
                break;
            }

            $tiles[] = new Tile($row, $j);
        }

        return new Alignment($tiles, $elementToSearch);
    }

    /**
     * Returns a vertical alignment of the same tile element from the specified coordinates.
     */
    private function findVerticalAlignment($row, $col): Alignment
    {
        $elementToSearch = $this->getElementAt($row, $col);

        if (-1 === $elementToSearch) {
            return Alignment::InvalidAlignment();
        }

        $tiles = [];
        $tiles[] = new Tile($row, $col);

        for ($i = $row + 1; $i < GridManager::K_GRID_SIZE; ++$i) {
            if ($this->getElementAt($i, $col) !== $elementToSearch) {
                break;
            }

            $tiles[] = new Tile($i, $col);
        }

        for ($i = $row - 1; $i >= 0; --$i) {
            if ($this->getElementAt($i, $col) !== $elementToSearch) {
                break;
            }

            $tiles[] = new Tile($i, $col);
        }

        return new Alignment($tiles, $elementToSearch);
    }

    /**
     * Returns the best valid alignment between the two specified.
     */
    private function getBestAlignment($alignmentH, $alignmentV): Alignment
    {
        $bestAlignment = $alignmentH->getLength() >= $alignmentV->getLength() ? $alignmentH : $alignmentV;

        if ($bestAlignment->getLength() >= 3) {
            return $bestAlignment;
        }

        return Alignment::InvalidAlignment();
    }

    /**
     * Convert a grid data to a single string of 64 characters.
     */
    public static function convertGridToString(array $grid): string
    {
        return implode('', $grid);
    }

    /**
     * Returns all info about the last chain reaction triggered following a swap.
     */
    public function getChainReactionSteps(): array
    {
        return $this->steps;
    }
}
