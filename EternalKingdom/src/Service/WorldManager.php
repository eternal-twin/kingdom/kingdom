<?php

namespace App\Service;

use App\Entity\Resources;
use App\Entity\World\ResourceSpot;
use App\Entity\World\WorldData;
use App\Entity\World\WorldMap;
use App\Entity\World\WorldMapNode;
use App\Entity\World\WorldNodeData;
use App\Enum\LogEventType;
use App\Enum\WorldSpeedType;
use App\Kingdom\CheatSystem\CheatConfiguration;
use App\Repository\World\ResourceSpotRepository;
use App\Repository\World\WorldDataRepository;
use App\Repository\World\WorldMapNodeRepository;
use App\Repository\World\WorldMapRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class WorldManager
{
    public const int K_DELAY_BETWEEN_WORLD_UPDATE = 2 * 60;
    public const int K_MAX_WORLD_SIMULATION_TICK_PER_UPDATE = 60;
    public const int K_MAX_WORLD_ID = 15;
    public const int RESOURCE_BASE_RESPAWN_DELAY = 1800;
    public const array RESOURCE_SPAWNABLE_TYPES = ['lin', 'iron', 'horse'];
    public const int RESOURCE_RESPAWN_MIN_DOZEN = 20;
    public const int RESOURCE_RESPAWN_MAX_DOZEN = 39;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly GameDataProvider $gameDataProvider,
        private readonly GameLogManager $gameLogManager,
        private readonly BattleManager $battleManager,
        private readonly MovementManager $movementManager,
        private readonly KernelInterface $kernel)
    {
    }

    /**
     * Generate a world from scratch.
     */
    public function generateNewWorld(string $worldDataName, string $worldName, string $desiredLanguage, int $speed, int $slotCount): bool
    {
        /** @var WorldDataRepository $worldDataRepository */
        $worldDataRepository = $this->entityManager->getRepository(WorldData::class);

        /** @var WorldData $worldData */
        $worldData = $worldDataRepository->findOneBy(['mapFileName' => $worldDataName]);

        if (null === $worldData) {
            // TODO FIX THAT !! THIS IS WRONG ! We'll only be able to get world named french in any director folder!
            $worldData = $this->createWorldData($worldDataName);
            if ($worldData instanceof WorldData) {
                $this->entityManager->persist($worldData);
            }
        }

        if (null !== $worldData) {
            $this->createWorld($worldData, $worldName, $desiredLanguage, $speed, $slotCount);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    /**
     * Create a WorldData Entity (including nodes and adjacencies) from the json file.
     */
    public function createWorldData(string $fileName): ?WorldData
    {
        // https://stackoverflow.com/questions/6654157/how-to-get-a-platform-independent-directory-separator-in-php
        $worldJsonPath = $this->kernel->getProjectDir().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'maps'.DIRECTORY_SEPARATOR.$fileName.'.json';
        if (!is_file($worldJsonPath)) {
            dump('File '.$worldJsonPath.' not found');

            return null;
        }

        $jsonResult = file_get_contents($worldJsonPath);
        if (false === $jsonResult) {
            dump('Bad JSON');

            return null;
        }

        $jsonArray = json_decode($jsonResult, true);
        if (is_null($jsonArray)) {
            dump('failed to load maps/'.$worldJsonPath);

            return null;
        }

        $cities = $jsonArray['cities'];

        $worldData = new WorldData($fileName);
        $allNodes = [];
        foreach ($cities as $i => $item) {
            $nodeData = new WorldNodeData($item['name'], $i, $item['x'], $item['y'], true === $item['cap']);

            $worldData->addNode($nodeData);
            $allNodes[$i] = $nodeData;
        }

        foreach ($cities as $i => $item) {
            $nodeData = $allNodes[$i];
            foreach ($item['adj'] as $adjacency) {
                $nodeData->addAdjacency($allNodes[$adjacency]);
            }
        }

        return $worldData;
    }

    /**
     * Initialize a world and add it to the list of World to persist.
     */
    public function createWorld(WorldData $worldData, string $name, string $language, int $speed = WorldSpeedType::REGULAR, int $slotCount = 50): WorldMap
    {
        $world = new WorldMap($name, $language, $worldData, $speed, $slotCount);

        foreach ($worldData->getNodes() as $nodeData) {
            $world->addNode(new WorldMapNode($nodeData));
        }

        $this->spawnResourceSpots($world, $worldData);

        $this->entityManager->persist($world);

        return $world;
    }

    public function tickAllWorlds(): void
    {
        /** @var WorldMapRepository $worldMapRepo */
        $worldMapRepo = $this->entityManager->getRepository(WorldMap::class);
        $worlds = $worldMapRepo->findAll();

        foreach ($worlds as $world) {
            $this->tickWorld($world);
        }
    }

    // TODO Get rid of this and only tick Movement of one world!
    public function tickAllMovements(): void
    {
        $this->movementManager->verifyAllPendingMovements();
    }

    public function tickWorld(WorldMap $world)
    {
        $now = new \DateTime();
        $delta = $now->getTimestamp() - ($world->getLastUpdateDate()->getTimestamp() + self::K_DELAY_BETWEEN_WORLD_UPDATE);
        if ($delta < 0 && false === CheatConfiguration::CHEAT_IGNORE_WORLD_TICK_DELAY) {
            return;
        }

        try {
            // TODO 1. Tick all pending movements

            $tickCount = min(self::K_MAX_WORLD_SIMULATION_TICK_PER_UPDATE, max(1, floor($delta / self::K_DELAY_BETWEEN_WORLD_UPDATE)));

            // 2. Tick all Battles
            $this->battleManager->refreshAllBattles($world, $tickCount);

            // TODO 3. Tick all Cities (Turns, bankruptcy, trade, ...)

            $this->verifyResourceSpotRespawn($world);

            // TODO Store date : now + current tickCount * K_WORLD_UPDATE_DELAY ?
            $world->refreshLastUpdateDate();
            $this->entityManager->persist($world);

            $this->entityManager->flush();
        } catch (\Exception $e) {
            dump('Something went wrong during World update');
            throw $e;
        }
    }

    /**
     * Verify if we should spawn new resource spots in a World.
     */
    public function verifyResourceSpotRespawn(WorldMap $worldMap): void
    {
        if ($worldMap->shouldRespawnResourceSpot()) {
            $worldMap->clearResourceRespawnTimeSchedule();
            $spawnedResourceSpots = $this->spawnResourceSpots($worldMap, $worldMap->getData());
            $this->entityManager->flush();

            $this->gameLogManager->beginLogTransaction();
            foreach ($spawnedResourceSpots as $resourceSpot) {
                /** @var WorldMapNodeRepository $worldMapNodeRepo */
                $worldMapNodeRepo = $this->entityManager->getRepository(WorldMapNode::class);
                $node = $worldMapNodeRepo->findOneBy(['owningWorld' => $worldMap, 'data' => $resourceSpot->getNode()]);

                $resourceName = $resourceSpot->getResource()->getName();
                $this->gameLogManager->addWorldLog(
                    $worldMap,
                    LogEventType::WORLD_RESOURCE_SPAWN,
                    false,
                    [
                        '%icon%' => $resourceName,
                        '%resourceSpot%' => $resourceName,
                        '%worldId%' => $worldMap->getId(),
                        '%nodeId%' => $node->getId(),
                        '%nodeName%' => $node->getName(),
                    ]
                );
            }
            $this->gameLogManager->finishLogTransaction();

            // dump(sprintf("Resource respawned : %d", count($spawnedResourceSpots)));
        }
    }

    /**
     * Spawns resource spots on the specified World (thanks to the WorldMapData nodes buffers).
     * A Resource spot can't have adjacent node that own Resource Spot !
     *
     * @return ResourceSpot[]
     */
    private function spawnResourceSpots(WorldMap $world, WorldData $worldData): array
    {
        $cityNodeBuffer = [];
        $emptyNodesBuffer = [];
        foreach ($worldData->getNodes() as $nodeData) {
            if ($nodeData->isSpawnPoint()) {
                $cityNodeBuffer[] = $nodeData;
            } else {
                $emptyNodesBuffer[] = $nodeData;
            }
        }

        $this->gameDataProvider->loadResourceDict();
        $resourceDict = $this->gameDataProvider->getResourceDict();

        $spawnedResourceSpots = [];
        $availableNodes = $emptyNodesBuffer;
        $spawnLimit = min(count($cityNodeBuffer), $world->getMaxPlayersCount());

        /** @var ResourceSpotRepository $resourceSpotRepository */
        $resourceSpotRepository = $this->entityManager->getRepository(ResourceSpot::class);
        $existingResourceSpots = $resourceSpotRepository->findBy(['owningWorld' => $world]);
        foreach ($existingResourceSpots as $resourceSpot) {
            $this->excludeNodeFromBuffer($resourceSpot->getNode(), $availableNodes);
        }

        //        dump(sprintf("Remaining available nodes : %d", count($availableNodes)));
        //        dump(sprintf("%d / %d", count($existingResourceSpots), $spawnLimit));

        while (count($availableNodes) > 0 && (count($spawnedResourceSpots) + count($existingResourceSpots)) < $spawnLimit) {
            $randomIndex = array_rand($availableNodes);
            $currentNode = $availableNodes[$randomIndex];

            $resourceType = random_int(0, count(self::RESOURCE_SPAWNABLE_TYPES) - 1);
            /** @var Resources $resource */
            $resource = $resourceDict[self::RESOURCE_SPAWNABLE_TYPES[$resourceType]];
            $amount = random_int(self::RESOURCE_RESPAWN_MIN_DOZEN, self::RESOURCE_RESPAWN_MAX_DOZEN) * 10;
            $spawnedResourceSpots[] = new ResourceSpot($world, $currentNode, $resource, $amount);

            $this->excludeNodeFromBuffer($currentNode, $availableNodes);
        }

        foreach ($spawnedResourceSpots as $resourceSpot) {
            $this->entityManager->persist($resourceSpot);
        }

        return $spawnedResourceSpots;
    }

    public function excludeNodeFromBuffer(WorldNodeData $node, array &$buffer): void
    {
        $key = array_search($node, $buffer);
        if (false !== $key) {
            array_splice($buffer, $key, 1);
        }

        foreach ($node->getAdjacencies() as $adjacentNode) {
            $key = array_search($adjacentNode, $buffer);
            if (false !== $key) {
                array_splice($buffer, $key, 1);
            }
        }
    }

    /**
     * Returns a random available node in the specified world.
     *
     * @return WorldNodeData|null
     */
    public function getRandomAvailableCity(int $worldId): ?WorldMapNode
    {
        /** @var WorldMapNodeRepository $nodesRepository */
        $nodesRepository = $this->entityManager->GetRepository(WorldMapNode::class);
        $availableNodes = $nodesRepository->findAvailableCities($worldId);

        // dump($availableNodes);

        $node = null;
        if (count($availableNodes) > 0) {
            $randomNodeIndex = array_rand($availableNodes);
            $node = $availableNodes[$randomNodeIndex];
            // dump('Randomized node is ' . $node->getName());
        }

        return $node;
    }

    public function getNode(int $nodeId): ?WorldMapNode
    {
        /** @var WorldMapNodeRepository $nodesRepository */
        $nodesRepository = $this->entityManager->GetRepository(WorldMapNode::class);

        return $nodesRepository->findOneBy(['id' => $nodeId]);
    }

    public function loadWorldData(string $worldDataName, array &$worldData): bool
    {
        // https://stackoverflow.com/questions/6654157/how-to-get-a-platform-independent-directory-separator-in-php
        $mapNamesJsonPath = $this->kernel->getProjectDir().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'maps'.DIRECTORY_SEPARATOR.$worldDataName.'.json';
        if (!is_file($mapNamesJsonPath)) {
            dump('File '.$mapNamesJsonPath.' not found');

            return false;
        }

        $jsonResult = file_get_contents($mapNamesJsonPath);
        if (false === $jsonResult) {
            dump('Bad JSON');

            return false;
        }

        $jsonResult = json_decode($jsonResult, true);
        if (is_null($jsonResult)) {
            dump('failed to load maps/'.$mapNamesJsonPath);

            return false;
        }

        $worldData = $jsonResult;
        return true;
    }

    public function loadAllMapNames(array &$mapNames): bool
    {
        // https://stackoverflow.com/questions/6654157/how-to-get-a-platform-independent-directory-separator-in-php
        $mapNamesJsonPath = $this->kernel->getProjectDir().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'maps'.DIRECTORY_SEPARATOR.'map_names.json';
        if (!is_file($mapNamesJsonPath)) {
            dump('File '.$mapNamesJsonPath.' not found');

            return false;
        }

        $jsonResult = file_get_contents($mapNamesJsonPath);
        if (false === $jsonResult) {
            dump('Bad JSON');

            return false;
        }

        $jsonResult = json_decode($jsonResult, true);
        if (is_null($jsonResult)) {
            dump('failed to load maps/'.$mapNamesJsonPath);

            return false;
        }

        $mapNames = $jsonResult;
        return true;
    }
}
