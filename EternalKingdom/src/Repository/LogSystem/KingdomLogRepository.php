<?php

namespace App\Repository\LogSystem;

use App\Entity\LogSystem\KingdomLog;
use App\Entity\Lord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method KingdomLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method KingdomLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method KingdomLog[]    findAll()
 * @method KingdomLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KingdomLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KingdomLog::class);
    }

    public function deleteLogs(Lord $lord): void
    {
        $query = $this->createQueryBuilder('log')
            ->delete()
            ->where('log.lord = :id')
            ->setParameter('id', $lord->getId());

        $query->getQuery()->execute();
    }
}
