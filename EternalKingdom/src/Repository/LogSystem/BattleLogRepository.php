<?php

namespace App\Repository\LogSystem;

use App\Entity\LogSystem\BattleLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BattleLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method BattleLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method BattleLog[]    findAll()
 * @method BattleLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattleLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BattleLog::class);
    }
}
