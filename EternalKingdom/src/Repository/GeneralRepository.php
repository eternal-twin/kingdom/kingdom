<?php

namespace App\Repository;

use App\Entity\General;
use App\Entity\Lord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method General|null find($id, $lockMode = null, $lockVersion = null)
 * @method General|null findOneBy(array $criteria, array $orderBy = null)
 * @method General[]    findAll()
 * @method General[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeneralRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, General::class);
    }

    /**
     * @return General[] Returns the array of General in the specified world
     */
    public function findGenerals(int $worldId): array
    {
        return $this->createQueryBuilder('g')
            ->join(Lord::class, 'l', Join::WITH, 'g.owner = l.id')
            ->andWhere('l.worldMap = :val')
            ->setParameter('val', $worldId)
            ->getQuery()
            ->getResult()
        ;
    }
}
