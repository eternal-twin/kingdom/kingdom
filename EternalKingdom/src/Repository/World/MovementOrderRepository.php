<?php

namespace App\Repository\World;

use App\Entity\World\MovementOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MovementOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method MovementOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method MovementOrder[]    findAll()
 * @method MovementOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovementOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovementOrder::class);
    }
}
