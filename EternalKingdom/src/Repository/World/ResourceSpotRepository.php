<?php

namespace App\Repository\World;

use App\Entity\Lord;
use App\Entity\World\ResourceSpot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResourceSpot|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResourceSpot|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResourceSpot[]    findAll()
 * @method ResourceSpot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResourceSpotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResourceSpot::class);
    }

    public function findOwnedResourceSpot(Lord $lord): array
    {
        $nodes = [];
        foreach ($lord->getKingdom() as $node) {
            $nodes[] = $node->getData()->getId();
        }

        $qb = $this->createQueryBuilder('spot')
            ->where('spot.node IN (:ids)')
            ->setParameter('ids', $nodes);

        return $qb->getQuery()->getResult();
    }
}
