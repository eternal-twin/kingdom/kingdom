<?php

// src/Controller/HelpController.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelpController extends AbstractController
{
    /**
     * @Route("/help")
     */
    public function Help(): Response
    {
        return $this->render('Help/help.html.twig');
    }

    /**
     * @Route("/help/join")
     */
    public function Join(): Response
    {
        return $this->Help(); // alias Route("/help")
    }

    /**
     * @Route("/help/city")
     */
    public function City(): Response
    {
        return $this->render('Help/help_city.html.twig');
    }

    /**
     * @Route("/help/prod")
     */
    public function Prod(): Response
    {
        return $this->render('Help/help_prod.html.twig');
    }

    /**
     * @Route("/help/build")
     */
    public function Build(): Response
    {
        return $this->render('Help/help_build.html.twig');
    }

    /**
     * @Route("/help/build_list")
     */
    public function BuildList(): Response
    {
        return $this->render('Help/help_build_list.html.twig');
    }

    /**
     * @Route("/help/soldiers")
     */
    public function Soldiers(): Response
    {
        return $this->render('Help/help_soldiers.html.twig');
    }

    /**
     * @Route("/help/general")
     */
    public function General(): Response
    {
        return $this->render('Help/help_general.html.twig');
    }

    /**
     * @Route("/help/battle")
     */
    public function Battle(): Response
    {
        return $this->render('Help/help_battle.html.twig');
    }

    /**
     * @Route("/help/vassal")
     */
    public function Vassal(): Response
    {
        return $this->render('Help/help_vassal.html.twig');
    }

    /**
     * @Route("/help/glory")
     */
    public function Glory(): Response
    {
        return $this->render('Help/help_glory.html.twig');
    }

    /**
     * @Route("/help/trade")
     */
    public function Trade(): Response
    {
        return $this->render('Help/help_trade.html.twig');
    }

    /**
     * @Route("/help/rareres")
     */
    public function Rareres(): Response
    {
        return $this->render('Help/help_rareres.html.twig');
    }

    /**
     * @Route("/help/gameover")
     */
    public function Gameover(): Response
    {
        return $this->render('Help/help_gameover.html.twig');
    }

    /**
     * @Route("/help/diplo")
     */
    public function Diplo(): Response
    {
        return $this->render('Help/help_diplo.html.twig');
    }

    /**
     * @Route("/help/advanced")
     */
    public function Advanced(): Response
    {
        return $this->render('Help/help_advanced.html.twig');
    }
}
