<?php

// src/Controller/DefaultController.php

namespace App\Controller;

use App\Entity\Lord;
use App\Entity\Relation;
use App\Entity\User;
use App\Enum\DiplomacyType;
use App\Repository\RelationRepository;
use App\Repository\UserRepository;
use App\Service\LordManager;
use App\Service\WorldManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    public const string K_FLASH_REDIRECTION = 'redirection';
    public const string K_FLASH_SHOWNEWS = 'showNews';

    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly WorldManager $worldManager,
        private readonly LordManager $lordManager,
        private readonly FlashBagInterface $flashBag
    ) {
    }

    /**
     * Homepage
     * Redirects to the appropriate page depending player state
     * (Ex: Index, ServerSelection, Management, Death, ...).
     *
     * @Route("/", name="homepage")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (is_null($user)) {
            return $this->redirectToRoute('welcome');
        }

        if ($this->flashBag->has(self::K_FLASH_REDIRECTION)) {
            $message = $this->flashBag->peek(self::K_FLASH_REDIRECTION);
            if (count($message) > 0) {
                switch ($message[0]) {
                    case self::K_FLASH_SHOWNEWS:
                        return $this->redirectToRoute('app_default_news');
                    default:
                        break;
                }
            }
        }

        /** @var Lord $lord */
        $lord = $user->getLord();

        $hasGameInProgress = null !== $lord;
        if ($hasGameInProgress) {
            $parameters = [
                'id' => $lord->getId(),
            ];

            $this->lordManager->refreshAllLords();
            $this->worldManager->tickAllWorlds();

            if ($lord->isAlive()) {
                return $this->redirectToRoute('app_management_management', $parameters);
            }

            return $this->redirectToRoute('death');
        } else {
            return $this->redirectToRoute('serverSelection');
        }
    }

    /**
     * @Route("/welcome", name="welcome")
     */
    public function welcomePage(): Response
    {
        return $this->render('index.html.twig');
    }

    /**
     * User page.
     *
     * @Route("/user/{id}", name="user")
     */
    public function user(int $id): Response
    {
        /** @var UserRepository $userRepo */
        $userRepo = $this->managerRegistry->GetRepository(User::class);
        /** @var User|null $shownUser */
        $shownUser = $userRepo->findOneBy(['id' => $id]);

        if (null === $shownUser) {
            throw $this->createNotFoundException(sprintf('The user with id %d does not exist', $id));
        }

        return $this->generateUserResponse($shownUser);
    }

    /**
     * Same as user page but using the Eternaltwin user id.
     *
     * @Route("/userPage/{eternaltwinId}", name="userUsingEternalTwin")
     */
    public function userUsingEternalTwin(string $eternaltwinId): Response
    {
        /** @var UserRepository $userRepo */
        $userRepo = $this->managerRegistry->GetRepository(User::class);
        /** @var User|null $shownUser */
        $shownUser = $userRepo->findOneBy(['userId' => $eternaltwinId]);

        if (null === $shownUser) {
            throw $this->createNotFoundException(sprintf('The user with the EternalTwin ID %s does not exist', $eternaltwinId));
        }

        return $this->generateUserResponse($shownUser);
    }

    private function generateUserResponse(User $shownUser): Response
    {
        $userRelation = null;
        $friends = [];
        $enemies = [];

        if ($shownUser->getLord() instanceof Lord) {
            /** @var RelationRepository $relationRepo */
            $relationRepo = $this->managerRegistry->GetRepository(Relation::class);
            $allRelations = $relationRepo->findBy(['lord' => $shownUser->getLord()]);

            foreach ($allRelations as $relation) {
                if (DiplomacyType::TYPE_FRIEND == $relation->getDiplomacyType()) {
                    $friends[] = $relation->getTarget();
                } elseif (DiplomacyType::TYPE_ENEMY == $relation->getDiplomacyType()) {
                    $enemies[] = $relation->getTarget();
                }
            }

            /** @var User $currentUser */
            $currentUser = $this->getUser();
            if (null !== $currentUser->getLord()) {
                $userRelation = $relationRepo->findOneBy(['target' => $shownUser->getLord()]);
                if (null === $userRelation) {
                    $userRelation = new Relation($currentUser->getLord(), $shownUser->getLord());
                }
            }
        }

        return $this->render('Misc/user.html.twig', ['user' => $shownUser, 'friends' => $friends, 'enemies' => $enemies, 'relation' => $userRelation]);
    }

    /**
     * Route allowing to change the default locale!
     *
     * @Route("/lang/{_locale}/", name="homepage_locale")
     */
    public function changeLanguage(Request $request): Response
    {
        $isConnected = null !== $this->getUser();
        if ($isConnected) {
            return $this->redirect($request->headers->get('referer'));
        } else {
            // When not connected, the webpage is not translated since we rely on a session variable.
            return $this->forward("App\Controller\DefaultController::Index");
        }
    }

    /**
     * @Route("/credits", name="credits")
     */
    public function credits(): Response
    {
        return $this->render('Misc/credits.html.twig');
    }

    /**
     * @Route("/news")
     */
    public function news(): Response
    {
        return $this->render('Misc/news.html.twig');
    }
}
