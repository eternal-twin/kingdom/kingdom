<?php

// src/Controller/ManagementController.php

namespace App\Controller;

use App\Entity\LogSystem\KingdomLog;
use App\Entity\Lord;
use App\Entity\Relation;
use App\Entity\User;
use App\Entity\World\ResourceSpot;
use App\Repository\LogSystem\KingdomLogRepository;
use App\Repository\RelationRepository;
use App\Repository\World\ResourceSpotRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ManagementController will display the Management page
 * Inspect how you are, your cities, your relations with other realms, your productions, ...
 */
class ManagementController extends AbstractController
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry
    ) {
    }

    /**
     * Lord Management page
     * Forwarded from DefaultController when accessing index webpage.
     *
     * @Route("/management/{tab}")
     */
    public function Management($tab = 'generals'): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->redirectToRoute('homepage');
        }

        if (null !== $user->getLord() && $user->getLord()->isAlive()) {
            /** @var RelationRepository $relationsRepository */
            $relationsRepository = $this->managerRegistry->GetRepository(Relation::class);
            $relations = $relationsRepository->findBy(['lord' => $user->getLord()]);

            return $this->generateManagementResponse($tab, ['relations' => $relations]);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Lord Management page
     * Forwarded from DefaultController when accessing index webpage.
     *
     * @Route("/management/diplomacy/invert")
     */
    public function invertDiplomacy(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->redirectToRoute('homepage');
        }

        if (null !== $user->getLord() && $user->getLord()->isAlive()) {
            /** @var RelationRepository $relationsRepository */
            $relationsRepository = $this->managerRegistry->GetRepository(Relation::class);
            $relations = $relationsRepository->findBy(['target' => $user->getLord()]);

            $params = [
                'relations' => $relations,
                'isDiplomacyInverted' => true,
            ];

            return $this->generateManagementResponse('diplomacy', $params);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @param string $tab Generals, Vassals, Kingdom or Diplomacy
     */
    private function generateManagementResponse(string $tab, array $extraParameters = []): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->redirectToRoute('homepage');
        }

        if (null !== $user->getLord() && $user->getLord()->isAlive()) {
            $lord = $user->getLord();

            /** @var KingdomLogRepository $kingdomLogsRepository */
            $kingdomLogsRepository = $this->managerRegistry->GetRepository(KingdomLog::class);
            $kingdomLogs = $kingdomLogsRepository->findBy(['lord' => $lord], ['timestamp' => 'DESC', 'id' => 'ASC']);

            /** @var ResourceSpotRepository $resourceSpotRepository */
            $resourceSpotRepository = $this->managerRegistry->GetRepository(ResourceSpot::class);
            $resourceSpots = $resourceSpotRepository->findOwnedResourceSpot($lord);

            $commonParameters = [
                'lord' => $lord,
                'resourceSpots' => $resourceSpots,
                'logs' => $kingdomLogs,
                'tab' => $tab,
            ];
            $allParameters = array_merge($commonParameters, $extraParameters);

            return $this->render('Management/management.html.twig', $allParameters);
        }

        return $this->redirectToRoute('homepage');
    }
}
