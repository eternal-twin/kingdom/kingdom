<?php

namespace App\Controller;

use App\Entity\LogSystem\CityLog;
use App\Entity\LogSystem\KingdomLog;
use App\Entity\User;
use App\Entity\World\WorldMap;
use App\Kingdom\World\Service\AutomaticWorldGenerator;
use App\Repository\LogSystem\CityLogRepository;
use App\Repository\LogSystem\KingdomLogRepository;
use App\Service\LordManager;
use App\Repository\World\WorldMapRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Every actions that doesn't concern in game options!
 */
class OutGameController extends AbstractController
{
    /**
     * DefaultController constructor.
     */
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly LordManager $lordManager,
        private readonly AutomaticWorldGenerator $automaticWorldGenerator,
    ) {
    }

    /**
     * Displays the World selection.
     *
     * @Route("/chooseWorld", name="serverSelection")
     */
    public function serverSelection(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null !== $user->getLord() && false === \CheatConfiguration::CHEAT_CAN_SEE_SERVER_SELECTION_WHILE_PLAYING) {
            return $this->redirectToRoute('homepage');
        }

        $this->automaticWorldGenerator->exec();

        // TODO Load available servers from DB relevant for the current user (difficulty / language preference?)

        /** @var WorldMapRepository $worldMapRepository */
        $worldMapRepository = $this->managerRegistry->getManager()->GetRepository(WorldMap::class);
        $worlds = $worldMapRepository->findAllWorldsWithAvailableSlots();

        return $this->render('Game/ServerSelection.html.twig', ['worlds' => $worlds]);
    }

    /**
     * Displays the World selection.
     *
     * @Route("/joinGame/{worldId}")
     */
    public function joinGame(int $worldId): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null !== $user && null === $user->getLord()) {
            $worldMapRepository = $this->managerRegistry->getManager()->GetRepository(WorldMap::class);

            /* @var WorldMap $world */
            $world = $worldMapRepository->find($worldId);

            if (null !== $world && $world->hasEmptySlot()) {
                $lord = $this->lordManager->createLord($user);
                $city = $this->lordManager->createCity($lord, $world);
                if ($city instanceof \App\Entity\City) {
                    $world->addPlayer($lord);
                    $user->setLord($lord);
                    $user->setIsInactiveInGame(false);
                    $user->setLastConnectionDate(new \DateTime());
                    $this->managerRegistry->getManager()->flush();

                    $this->lordManager->logSpawn($lord);
                } else {
                    dump('Error : there is no spawn available right now, please choose another world!');
                }
            } else {
                dump('No spawn available, world is full!');
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Try to display the Death Page.
     *
     * @Route("/death", name="death")
     */
    public function death(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->redirectToRoute('homepage');
        }

        if (null !== $user->getLord()) {
            $lord = $user->getLord();

            if ($this->lordManager->shouldLordDie($lord)) {
                $this->lordManager->triggerLordDeath($lord);
            }

            assert($lord->isDead());

            $kingdomLogsRepository = $this->managerRegistry->GetRepository(KingdomLog::class);
            $kingdomLogs = $kingdomLogsRepository->findBy(['lord' => $lord], ['timestamp' => 'DESC']);

            return $this->render('Game/Death.html.twig', ['logs' => $kingdomLogs]);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Debug : Force death logic.
     *
     * @Route("/forceDeath")
     */
    public function forceDeath(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null !== $user && null !== $user->getLord()) {
            $lord = $user->getLord();
            if ($this->lordManager->isForceDeathEnabled() && $lord->isAlive()) {
                $this->lordManager->triggerLordDeath($lord);
            }
        }

        return $this->death();
    }

    /**
     * @Route("/confirmDeath")
     */
    public function confirmDeath(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null !== $user) {
            $lord = $user->getLord();
            if (null !== $lord && !$lord->isAlive()) {
                $em = $this->managerRegistry->getManager();

                // Free the slot. TODO Should we do it sooner?
                $world = $lord->getWorldMap();
                $world->removePlayer($lord);
                $em->persist($world);

                // Delete all useless Logs that won't be shown anymore.
                /** @var CityLogRepository $logsRepo */
                $logsRepo = $em->getRepository(CityLog::class);
                $logsRepo->deleteLogs($lord);

                /** @var KingdomLogRepository $logsRepo */
                $logsRepo = $em->getRepository(KingdomLog::class);
                $logsRepo->deleteLogs($lord);

                $user->setLord(null); // Unlink lord

                // TODO Identify what is ok to be deleted
                // Delete the Lord should delete the City (including Storage, Built Buildings), Generals, ...
                $em->remove($lord);

                $em->flush();
            }
        }

        return $this->redirectToRoute('homepage');
    }
}
