<?php

// src/Controller/MapController.php

namespace App\Controller;

use App\Entity\Battle\Battle;
use App\Entity\City;
use App\Entity\General;
use App\Entity\LogSystem\WorldLog;
use App\Entity\Lord;
use App\Entity\Relation;
use App\Entity\User;
use App\Entity\World\ResourceSpot;
use App\Entity\World\WorldMap;
use App\Entity\World\WorldMapNode;
use App\Entity\World\WorldNodeData;
use App\Enum\DiplomacyType;
use App\Repository\Battle\BattleRepository;
use App\Repository\CityRepository;
use App\Repository\GeneralRepository;
use App\Repository\LordRepository;
use App\Repository\RelationRepository;
use App\Repository\World\ResourceSpotRepository;
use App\Repository\World\WorldMapNodeRepository;
use App\Repository\World\WorldMapRepository;
use App\Service\MovementManager;
use App\Service\WorldManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class MapController extends AbstractController
{
    public const int K_RANKS_PAGE_SIZE = 25;

    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly WorldManager $worldManager)
    {
    }

    /**
     * @Route("/map/{worldId}")
     */
    public function Map(int $worldId): Response
    {
        return $this->generateCommonMapResponse($worldId);
    }

    /**
     * @Route("/map/{worldId}/node/{nodeId}", name="app_map_focusNode")
     */
    public function mapFocusNode(int $worldId, int $nodeId): Response
    {
        $focusedNode = '';
        if ($nodeId > 0) {
            $nodeRepo = $this->managerRegistry->GetRepository(WorldMapNode::class);

            /** @var WorldMapNode $node */
            $node = $nodeRepo->findOneBy(['owningWorld' => $worldId, 'id' => $nodeId]);
            if ($node) {
                $focusedNode = $node->getData()->getName();
            }
        }

        return $this->generateCommonMapResponse($worldId, ['focusedNode' => $focusedNode]);
    }

    /**
     * @Route("/map/{worldId}/general/{generalId}", name="app_map_focusGeneral")
     */
    public function mapFocusGeneral(int $worldId, int $generalId): Response
    {
        return $this->generateCommonMapResponse($worldId, ['focusedGeneral' => $generalId]);
    }

    /**
     * @Route("/map/{worldId}/kingdom/{lordId}", name="app_map_focusKingdom")
     */
    public function mapFocusKingdom(int $worldId, int $lordId): Response
    {
        /** @var LordRepository $lordRepository */
        $lordRepository = $this->managerRegistry->GetRepository(Lord::class);
        $kingdomOwninglord = $lordRepository->find($lordId);

        if (null === $kingdomOwninglord || $kingdomOwninglord->isDead()) {
            return $this->forward('App\Controller\MapController::Map', ['worldId' => $worldId]);
        }

        return $this->generateCommonMapResponse($worldId, ['focusedKingdom' => $lordId]);
    }

    private function generateCommonMapResponse(int $worldId, array $extraParameters = []): Response
    {
        /** @var WorldMapRepository $worldRepository */
        $worldRepository = $this->managerRegistry->GetRepository(WorldMap::class);
        $world = $worldRepository->find($worldId);

        if (null == $world) {
            throw new NotFoundHttpException('This world doesn\'t exist!');
        }

        /** @var User $user */
        $user = $this->getUser();

        $currentLord = null !== $user ? $user->getLord() : null;

        $this->worldManager->tickAllMovements();
        $this->worldManager->tickWorld($world);

        /** @var LordRepository $lordRepository */
        $lordRepository = $this->managerRegistry->GetRepository(Lord::class);
        $lords = $lordRepository->findAllAliveLordsOrderByAge($worldId);

        // Load all nodes to cache all node data that could be fetched individually and slow the action!
        $nodeDataRepo = $this->managerRegistry->GetRepository(WorldNodeData::class);
        $nodeDataRepo->findBy(['world' => $world->getData()->getId()]);

        $relations = [];
        if ($currentLord) {
            /** @var RelationRepository $relationRepo */
            $relationRepo = $this->managerRegistry->getRepository(Relation::class);
            $relations = $relationRepo->findBy(['target' => $currentLord]);
        }

        /** @var WorldMapNodeRepository $nodeRepo */
        $nodeRepo = $this->managerRegistry->GetRepository(WorldMapNode::class);
        $nodes = $nodeRepo->findBy(['owningWorld' => $worldId]);

        /** @var ResourceSpotRepository $resourceSpotRepo */
        $resourceSpotRepo = $this->managerRegistry->GetRepository(ResourceSpot::class);
        $resourceSpots = $resourceSpotRepo->findBy(['owningWorld' => $worldId]);

        /** @var GeneralRepository $generalRepo */
        $generalRepo = $this->managerRegistry->GetRepository(General::class);
        $generals = $generalRepo->findGenerals($worldId);

        $logsRepo = $this->managerRegistry->GetRepository(WorldLog::class);
        $logs = $logsRepo->findBy(['world' => $worldId], ['timestamp' => 'DESC']);

        /** @var CityRepository $cityRepo */
        $cityRepo = $this->managerRegistry->getRepository(City::class);
        $cities = $cityRepo->getAllCitiesInWorld($worldId);

        /** @var BattleRepository $battleRepo */
        $battleRepo = $this->managerRegistry->getRepository(Battle::class);
        $battles = $battleRepo->findOngoingBattles($worldId);

        $mapData = [
            'world' => $world,
            'lords' => $lords,
            'nodes' => $nodes,
            'cities' => $cities,
            'resourceSpots' => $resourceSpots,
            'generals' => $generals,
            'battles' => $battles,
        ];

        if (count($relations) > 0) {
            $mapData['relations'] = $relations;
        }

        $params = [
            'world' => $world,
            'lord' => $currentLord,
            'mapData' => json_encode($mapData),
            'logs' => $logs,
        ];

        $allParameters = array_merge($params, $extraParameters);

        return $this->render('Map/map.html.twig', $allParameters);
    }

    /**
     * @Route("/fetchNodeInfo", methods={"GET"})
     */
    public function fetchNodeInfo(Request $request): Response
    {
        $response = new Response();
        $statusCode = Response::HTTP_FORBIDDEN;

        /** @var User $user */
        $user = $this->getUser();
        if (null !== $user) {
            $worldId = $request->query->get('world');
            $nodeId = $request->query->get('node');

            if ($worldId > 0 && $nodeId > 0) {
                /** @var WorldMapNodeRepository $nodeRepo */
                $nodeRepo = $this->managerRegistry->GetRepository(WorldMapNode::class);
                /** @var WorldMapNode $node */
                $node = $nodeRepo->findOneBy(['owningWorld' => $worldId, 'id' => $nodeId]);

                if (null !== $node) {
                    $statusCode = Response::HTTP_OK;

                    // TODO What if we got both an owner and a sovereign?
                    // We prioritize the owner here.
                    $owningLord = $node->getOwner() ?: $node->getSovereign();
                    $canSeeArmy = $this->canSeeArmy($user, $owningLord);
                    $garrisonUnits = $node->getArmyContainer()->toArray(!$canSeeArmy);

                    $defenseUnits = [];
                    if (null !== $node->getOwner()) {
                        $defenseUnits = $node->getOwner()->getCity()->getDefense()->toArray(!$canSeeArmy);
                    }

                    $clientPayload = [];
                    $clientPayload['node'] = $nodeId;
                    $clientPayload['units'] = $garrisonUnits;
                    $clientPayload['defenseUnits'] = $defenseUnits;

                    $response = new JsonResponse();
                    $response->setContent(json_encode($clientPayload));
                }
            }

            $response->setStatusCode($statusCode);
        }

        return $response;
    }

    /**
     * @Route("/fetchGeneralInfo", methods={"GET"})
     */
    public function fetchGeneralInfo(Request $request): Response
    {
        $response = new Response();
        $statusCode = Response::HTTP_FORBIDDEN;

        /** @var User $user */
        $user = $this->getUser();
        if (null !== $user) {
            $worldId = $request->query->get('world');
            $generalId = $request->query->get('general');

            if ($worldId > 0 && $generalId > 0) {
                /** @var GeneralRepository $generalRepo */
                $generalRepo = $this->managerRegistry->GetRepository(General::class);
                $general = $generalRepo->findOneBy(['id' => $generalId]);

                if (null !== $general) {
                    $statusCode = Response::HTTP_OK;

                    $canSeeArmy = $this->canSeeArmy($user, $general->getOwner());
                    $units = $general->getArmyContainer()->toArray(!$canSeeArmy);

                    $clientPayload = [];
                    $clientPayload['general'] = $generalId;
                    $clientPayload['units'] = $units;

                    $response = new JsonResponse();
                    $response->setContent(json_encode($clientPayload));
                }
            }

            $response->setStatusCode($statusCode);
        }

        return $response;
    }

    /**
     * @Route("/world/modify/{worldId}")
     */
    public function modify(int $worldId): Response
    {
        dump('World to modify :'.$worldId);
        throw $this->createAccessDeniedException('Modify World Not implemented');
    }

    /**
     * @Route("/world/ranks/{worldId}/{page}")
     */
    public function ranks(int $worldId, int $page = 1): Response
    {
        /** @var WorldMapRepository $worldRepository */
        $worldRepository = $this->managerRegistry->GetRepository(WorldMap::class);
        $world = $worldRepository->find($worldId);

        if (null === $world) {
            throw new NotFoundHttpException('This world doesn\'t exist!');
        }

        /** @var LordRepository $lordRepository */
        $lordRepository = $this->managerRegistry->GetRepository(Lord::class);
        $lords = $lordRepository->findAllAliveLordsOrderByGlory($worldId, $page - 1, $this::K_RANKS_PAGE_SIZE);
        $pagesCount = ceil($lordRepository->countAliveLordsInWorld($worldId) / $this::K_RANKS_PAGE_SIZE);

        return $this->render(
            'Map/ranks.html.twig',
            [
                'lords' => $lords,
                'worldName' => $world->getName(),
                'currentPage' => $page,
                'totalPage' => $pagesCount,
                'pageSize' => $this::K_RANKS_PAGE_SIZE,
            ]
        );
    }

    /**
     * Return true if the specified player is able to see the army units amounts.
     */
    private function canSeeArmy(User $user, Lord $entityOwner): bool
    {
        $canSeeArmy = true;

        $currentLord = $user->getLord();

        // We're not in game
        if (!$currentLord instanceof Lord) {
            $canSeeArmy = false;
        }
        // TODO Maybe We should probably check sovereign instead?!
        // We don't own the node but check if the node owner consider me as a friend.
        elseif ($currentLord !== $entityOwner) {
            /** @var RelationRepository $relationRepo */
            $relationRepo = $this->managerRegistry->getRepository(Relation::class);
            $relation = $relationRepo->findOneBy(['lord' => $entityOwner, 'target' => $currentLord]);
            $canSeeArmy = null !== $relation && DiplomacyType::TYPE_FRIEND == $relation->getDiplomacyType();
        }

        return $canSeeArmy;
    }
}
