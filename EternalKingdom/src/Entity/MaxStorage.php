<?php

namespace App\Entity;

use App\Repository\MaxStorageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MaxStorageRepository::class)
 */
class MaxStorage
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Constructions $building;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $level;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $resource;

    /**
     * @ORM\Column(type="integer")
     */
    private int $maxAmount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuilding(): ?Constructions
    {
        return $this->building;
    }

    public function setBuilding(?Constructions $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getResource(): ?Resources
    {
        return $this->resource;
    }

    public function setResource(?Resources $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getMaxAmount(): ?int
    {
        return $this->maxAmount;
    }

    public function setMaxAmount(int $maxAmount): self
    {
        $this->maxAmount = $maxAmount;

        return $this;
    }
}
