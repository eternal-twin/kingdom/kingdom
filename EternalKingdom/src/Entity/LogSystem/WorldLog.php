<?php

namespace App\Entity\LogSystem;

use App\Entity\World\WorldMap;
use Doctrine\ORM\Mapping as ORM;

/**
 * Log associated with a specific World (displayed in Map).
 *
 * @ORM\Table(name="log_world")
 *
 * @ORM\Entity(repositoryClass=App\Repository\LogSystem\WorldLogRepository::class)
 */
class WorldLog extends Log
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMap::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMap $world;

    public function __construct(
        WorldMap $world,
        LogEvent $event,
        array $params
    ) {
        $this->world = $world;
        parent::__construct($event, $params);
    }

    public function getWorld(): ?WorldMap
    {
        return $this->world;
    }
}
