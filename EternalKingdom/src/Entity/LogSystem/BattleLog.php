<?php

namespace App\Entity\LogSystem;

use App\Entity\Battle\Battle;
use Doctrine\ORM\Mapping as ORM;

/**
 * Log associated with a specific Battle.
 *
 * @ORM\Table(name="log_battle")
 *
 * @ORM\Entity(repositoryClass=App\Repository\LogSystem\BattleLogRepository::class)
 */
class BattleLog extends Log
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Battle::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Battle $battle;

    public function __construct(
        Battle $battle,
        LogEvent $event,
        array $params
    ) {
        $this->battle = $battle;
        parent::__construct($event, $params);
    }

    public function getBattle(): ?Battle
    {
        return $this->battle;
    }
}
