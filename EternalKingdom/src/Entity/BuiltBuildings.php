<?php

namespace App\Entity;

use App\Repository\BuiltBuildingsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BuiltBuildingsRepository::class)
 */
class BuiltBuildings
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="builtBuildings")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private City $city;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Constructions $building;
    /**
     * @ORM\Column(type="smallint")
     */
    private int $level;

    public function __construct(
        /*
         * @ORM\ManyToOne(targetEntity=Constructions::class)
         *
         * @ORM\JoinColumn(nullable=false)
         */
        Constructions $building,
        /*
         * @ORM\Column(type="smallint")
         */
        int $level
    ) {
        $this->building = $building;
        $this->level = $level;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getBuilding(): ?Constructions
    {
        return $this->building;
    }

    public function setBuilding(?Constructions $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }
}
