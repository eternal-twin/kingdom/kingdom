<?php

namespace App\Entity;

use App\Entity\World\WorldMap;
use App\Entity\World\WorldMapNode;
use App\Enum\HealthType;
use App\Kingdom\Lord\Enum\TitleEnum;
use App\Kingdom\Ruleset\Ruleset;
use App\Repository\LordRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Lord is a character that is playing a game.
 * If a player has no lord, he doesn't play any game.
 *
 * @ORM\Entity(repositoryClass=LordRepository::class)
 */
class Lord implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @phpsan-ignore-next-line
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $birthDate;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $name;

    /**
     * @ORM\Column(type="integer")
     */
    private int $ageYear = 20;

    /**
     * @ORM\Column(type="float")
     */
    private float $ageMonth = 0;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $health = HealthType::TYPE_EXCELLENT;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $lastWeekWithoutHealthIssue = 0;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $title;

    /**
     * @ORM\Column(type="integer")
     */
    private int $glory;

    /**
     * @ORM\Column(name="max_glory", type="integer")
     */
    private int $apogee;

    /**
     * @ORM\OneToOne(targetEntity=City::class, mappedBy="lord", cascade={"persist", "remove"})
     */
    private City $city;

    /**
     * @ORM\OneToMany(targetEntity=General::class, mappedBy="owner", cascade={"persist", "remove"})
     *
     * @OrderBy({"id" = "ASC"})
     *
     * @var Collection<General>
     */
    private Collection $generals;

    /**
     * @ORM\OneToMany(targetEntity=WorldMapNode::class, cascade={"persist"}, mappedBy="sovereign")
     *
     * @var Collection<WorldMapNode>
     */
    private Collection $kingdom;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMap::class, inversedBy="lords")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?WorldMap $worldMap = null;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="lord")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private User $user;

    public function __construct(
        User $user,
        Ruleset $ruleset,
    ) {
        $this->generals = new ArrayCollection();
        $this->kingdom = new ArrayCollection();
        $this->birthDate = new \DateTime();
        $this->user = $user;

        $this->apogee = 1;
        $this->title = TitleEnum::KNIGHT->value;

        $this->setGlory(1, $ruleset);
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'userId' => $this->user->getId(),
            'title' => $this->title,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getBirthDate(): \DateTime
    {
        return $this->birthDate;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAgeYear(): ?int
    {
        return $this->ageYear;
    }

    public function setAgeYear(int $ageYear): self
    {
        $this->ageYear = $ageYear;

        return $this;
    }

    public function getAgeMonth(): float
    {
        return $this->ageMonth;
    }

    public function setAgeMonth(float $ageMonth): self
    {
        $this->ageMonth = $ageMonth;

        return $this;
    }

    /**
     * Returns the current age of the Lord in week.
     */
    public function getCurrentAgeInWeeks(): int
    {
        return $this->getAgeInWeeks((int) $this->getAgeYear()) + $this->getAgeMonth() * 4;
    }

    /**
     * Returns the specified years in weeks.
     * Function from official Kingdom.
     */
    public function getAgeInWeeks(int $years): int
    {
        return ($years - 20) * 12 << 2;
    }

    public function getLastWeekWithoutHealthIssue(): ?int
    {
        return $this->lastWeekWithoutHealthIssue ?: 0;
    }

    public function setLastWeekWithoutHealthIssue(?int $lastWeekWithoutHealthIssue): void
    {
        $this->lastWeekWithoutHealthIssue = $lastWeekWithoutHealthIssue;
    }

    public function getHealth(): ?string
    {
        return $this->health;
    }

    public function setHealth(string $health): self
    {
        if (!in_array($health, HealthType::getAvailableTypes())) {
            throw new \InvalidArgumentException('Invalid type');
        }
        $this->health = $health;

        return $this;
    }

    public function getTitle(): TitleEnum
    {
        return TitleEnum::from($this->title);
    }

    public function getGlory(): ?int
    {
        return $this->glory;
    }

    public function setGlory(int $glory, Ruleset $ruleset): self
    {
        $this->glory = $glory;

        if ($this->glory > $this->apogee) {
            $this->apogee = $this->glory;
            $this->title = $ruleset->getTitleFromApogee($this->apogee)->value;
        }

        return $this;
    }

    /** @deprecated */
    public function getMaxGlory(): int
    {
        return $this->getApogee();
    }

    public function getApogee(): int
    {
        return $this->apogee;
    }

    /**
     * Just a wrapper to ease access to spawn node.
     */
    public function getSpawnNode(): WorldMapNode
    {
        return $this->city->getNode();
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function &getCityRef(): City
    {
        return $this->city;
    }

    public function setCity(City $city): self
    {
        // set the owning side of the relation if necessary
        $city->setLord($this);
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|General[]
     */
    public function getGenerals(): Collection
    {
        return $this->generals;
    }

    public function getGeneralCount(): int
    {
        return count($this->generals);
    }

    public function addGeneral(General $general): self
    {
        if (!$this->generals->contains($general)) {
            $this->generals[] = $general;
            $general->setOwner($this);
        }

        return $this;
    }

    public function removeGeneral(General $general): self
    {
        // set the owning side to null (unless already changed)
        if ($this->generals->removeElement($general) && $general->getOwner() === $this) {
            $general->setOwner(null);
        }

        return $this;
    }

    public function clearGenerals(): void
    {
        $this->generals->clear();
    }

    /**
     * @return Collection<WorldMapNode>
     */
    public function getKingdom(): Collection
    {
        return $this->kingdom;
    }

    public function addTerritory(WorldMapNode $kingdom): self
    {
        if (!$this->kingdom->contains($kingdom)) {
            $this->kingdom[] = $kingdom;
            $kingdom->setSovereign($this);
        }

        return $this;
    }

    public function removeTerritory(WorldMapNode $kingdom): self
    {
        // set the owning side to null (unless already changed)
        if ($this->kingdom->removeElement($kingdom) && $kingdom->getSovereign() === $this) {
            $kingdom->setSovereign(null);
        }

        return $this;
    }

    public function getTrade(): int
    {
        $total = 0;
        foreach ($this->kingdom as $node) {
            $total += $node->getTrade();
        }

        return $total;
    }

    public function getReputation(): int
    {
        $total = 0;
        foreach ($this->generals as $general) {
            $total += $general->getReputation();
        }

        return $total;
    }

    public function isAlive(): bool
    {
        return false == (HealthType::TYPE_DEAD == $this->health || HealthType::TYPE_DYING == $this->health);
    }

    public function isDying(): bool
    {
        return HealthType::TYPE_DYING === $this->health;
    }

    public function isDead(): bool
    {
        return HealthType::TYPE_DEAD === $this->health;
    }

    public function getWorldMap(): ?WorldMap
    {
        return $this->worldMap;
    }

    public function setWorldMap(?WorldMap $worldMap): self
    {
        $this->worldMap = $worldMap;

        return $this;
    }

    /**
     * Count total army cost all over the world.
     */
    public function getTotalArmyCost(): int
    {
        $generalsCost = 0;
        /** @var General $general */
        foreach ($this->generals as $general) {
            if ($general->getArmyContainer()->hasAnyUnits()) {
                $generalsCost += $general->getArmyCost();
            }
        }

        $territoriesCost = 0;
        /** @var WorldMapNode $kingdom */
        foreach ($this->kingdom as $kingdom) {
            if ($kingdom === $this->city->getNode()) {
                $territoryCost = max(0, $this->city->getDefense()->getTotalUnitCount() - $this->city->getFreeUnitCount());
            } else {
                $territoryCost = $kingdom->getArmyContainer()->getTotalUnitCount();
            }
            $territoriesCost += $territoryCost;
        }

        return $generalsCost + $territoriesCost;
    }
}
