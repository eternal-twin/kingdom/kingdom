<?php

namespace App\Entity;

use App\Repository\ConstructionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConstructionsRepository::class)
 */
class Constructions
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $constructionType;

    /**
     * @ORM\OneToMany(targetEntity=ConstructionSchemas::class, mappedBy="construction", orphanRemoval=true)
     */
    private Collection $constructionSchemas;

    public function __construct()
    {
        $this->constructionSchemas = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getConstructionType(): string
    {
        return $this->constructionType;
    }

    public function setConstructionType(string $ConstructionType): self
    {
        $this->constructionType = $ConstructionType;

        return $this;
    }

    /**
     * @return Collection|ConstructionSchemas[]
     */
    public function getConstructionSchemas(): Collection
    {
        return $this->constructionSchemas;
    }

    public function addConstructionSchema(ConstructionSchemas $constructionSchema): self
    {
        if (!$this->constructionSchemas->contains($constructionSchema)) {
            $this->constructionSchemas[] = $constructionSchema;
            $constructionSchema->setConstruction($this);
        }

        return $this;
    }

    public function removeConstructionSchema(ConstructionSchemas $constructionSchema): self
    {
        // set the owning side to null (unless already changed)
        if ($this->constructionSchemas->removeElement($constructionSchema) && $constructionSchema->getConstruction() === $this) {
            $constructionSchema->setConstruction(null);
        }

        return $this;
    }

    public function getOrderedSchemas(): array
    {
        $orderedSchemas = [1 => [], 2 => [], 3 => [], 4 => [], 5 => []];
        foreach ($this->getConstructionSchemas() as $schema) {
            $orderedSchemas[$schema->getLevel()][$schema->getResource()->getName()] = $schema;
        }

        return $orderedSchemas;
    }
}
