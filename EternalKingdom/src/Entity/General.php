<?php

namespace App\Entity;

use App\Entity\Battle\Battle;
use App\Entity\World\MovementOrder;
use App\Entity\World\WorldMapNode;
use App\Repository\GeneralRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GeneralRepository::class)
 */
class General implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Lord::class, inversedBy="generals")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Lord $owner;

    /**
     * @ORM\Column(type="integer")
     */
    private int $reputation = 1;

    /**
     * @ORM\OneToOne(targetEntity=ArmyContainer::class, cascade={"persist", "remove"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ArmyContainer $armyContainer;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $fortifying = false;

    /**
     * Does the general is moving right now?
     *
     * @ORM\OneToOne(targetEntity=MovementOrder::class, mappedBy="instigator")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?MovementOrder $movementOrder = null;

    /**
     * Does the general is participating in a Battle right now?
     *
     * @ORM\ManyToOne(targetEntity=Battle::class, inversedBy="engagedGenerals")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Battle $battle = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $name;
    /**
     * Current location on the world map.
     *
     * @ORM\ManyToOne(targetEntity=WorldMapNode::class, inversedBy="generals")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMapNode $location;

    public function __construct(
        string $name,
        WorldMapNode $location,
    ) {
        $this->armyContainer = new ArmyContainer();
        $this->name = $name;
        $this->location = $location;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'ownerId' => $this->owner->getId(),
            'name' => $this->name,
            'locationName' => $this->location->getData()->getName(),
            'reputation' => $this->reputation,
            'isFortifying' => $this->fortifying,
            'movementOrder' => $this->movementOrder,
            'isAttacking' => null != $this->battle,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?Lord
    {
        return $this->owner;
    }

    public function setOwner(?Lord $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReputation(): ?int
    {
        return $this->reputation;
    }

    public function setReputation(int $reputation): self
    {
        $this->reputation = $reputation;

        return $this;
    }

    public function getArmyContainer(): ?ArmyContainer
    {
        return $this->armyContainer;
    }

    public function setArmyContainer(ArmyContainer $armyContainer): self
    {
        $this->armyContainer = $armyContainer;

        return $this;
    }

    public function getLocation(): WorldMapNode
    {
        return $this->location;
    }

    public function setLocation(WorldMapNode $destination): self
    {
        $this->location = $destination;

        return $this;
    }

    public function isFortifying(): ?bool
    {
        return $this->fortifying;
    }

    public function setFortifying(bool $fortifying): self
    {
        $this->fortifying = $fortifying;

        return $this;
    }

    public function hasAnArmy(): bool
    {
        return $this->armyContainer->hasAnyUnits();
    }

    public function getArmyCost(): int
    {
        return max(0, $this->getArmyContainer()->getTotalUnitCount() - $this->getReputation());
    }

    public function getMovementOrder(): ?MovementOrder
    {
        return $this->movementOrder;
    }

    public function setMovementOrder(?MovementOrder $currentMovementOrder): void
    {
        $this->movementOrder = $currentMovementOrder;
    }

    public function getBattle(): ?Battle
    {
        return $this->battle;
    }

    public function setBattle(?Battle $battle): void
    {
        $this->battle = $battle;
    }
}
