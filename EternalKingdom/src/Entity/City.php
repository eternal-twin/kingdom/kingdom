<?php

namespace App\Entity;

use App\Entity\World\WorldMapNode;
use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass=CityRepository::class)
 */
class City implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToOne(targetEntity=Lord::class, inversedBy="city", cascade={"persist", "remove"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Lord $lord;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $grid = '';

    /**
     * @ORM\Column(type="smallint")
     */
    private int $remainingTurns = 100;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $wastedTurns = 0;

    /**
     * @ORM\OneToMany(targetEntity=Storage::class, mappedBy="city", cascade={"persist", "remove"})
     */
    private Collection $storages;

    /**
     * @ORM\OneToMany(targetEntity=BuiltBuildings::class, mappedBy="city", cascade={"persist", "remove"})
     *
     * @OrderBy({"id" = "ASC"})
     */
    private Collection $builtBuildings;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $recruiting = true;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class)
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Constructions $constructionProject = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $projectLevel = null;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMapNode::class, cascade={"persist"})
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?WorldMapNode $node = null;

    /**
     * That's the default army where the soldier spawns when aligning swords in the grid.
     *
     * @ORM\OneToOne(targetEntity=ArmyContainer::class, cascade={"persist", "remove"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ArmyContainer $defense;

    // City population info

    /**
     * @ORM\Column(type="smallint")
     */
    private int $dungeonLevel = 1;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $housesCount = 0;

    public function __construct()
    {
        $this->storages = new ArrayCollection();
        $this->builtBuildings = new ArrayCollection();
        $this->defense = new ArmyContainer();
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'locationName' => $this->node->getData()->getName(),
            'towerLevel' => $this->dungeonLevel,
            'nbHouses' => $this->housesCount,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->node->getName();
    }

    public function getLord(): Lord
    {
        return $this->lord;
    }

    public function setLord(Lord $lord): self
    {
        $this->lord = $lord;

        return $this;
    }

    public function getGrid(): ?string
    {
        return $this->grid;
    }

    public function setGrid(string $grid): self
    {
        $this->grid = $grid;

        return $this;
    }

    public function getRemainingTurns(): ?int
    {
        return $this->remainingTurns;
    }

    public function setRemainingTurns(int $remainingTurns): self
    {
        $this->remainingTurns = $remainingTurns;

        return $this;
    }

    public function getWastedTurns(): ?int
    {
        return $this->wastedTurns;
    }

    public function setWastedTurns(int $wastedTurns): self
    {
        $this->wastedTurns = $wastedTurns;

        return $this;
    }

    /**
     * @return Collection|Storage[]
     */
    public function getStorages(): Collection
    {
        return $this->storages;
    }

    public function findStorage(string $resourceName): ?Storage
    {
        foreach ($this->getStorages() as $storage) {
            if ($storage->getResource()->getName() === $resourceName) {
                return $storage;
            }
        }

        return null;
    }

    public function addStorage(Storage $storage): self
    {
        if (!$this->storages->contains($storage)) {
            $this->storages[] = $storage;
            $storage->setCity($this);
        }

        return $this;
    }

    public function removeStorage(Storage $storage): self
    {
        // set the owning side to null (unless already changed)
        if ($this->storages->removeElement($storage) && $storage->getCity() === $this) {
            $storage->setCity(null);
        }

        return $this;
    }

    public function getStorageCount(string $resourceName): int // TODO : Maybe optimize ?
    {
        $storage = $this->findStorage($resourceName);
        if ($storage instanceof Storage) {
            return $storage->getAmount();
        }

        return 0;
    }

    public function getPopulationCount(): int
    {
        $count = 0;
        foreach ($this->storages as $storage) {
            if ('population' !== $storage->getResource()->getResourceType()) {
                continue;
            }
            $count += $storage->getAmount();
        }

        return $count;
    }

    public function getUnitCount(string $resourceName): int
    {
        return $this->getDefense()->getUnitCount($resourceName);
    }

    /**
     * @return Collection|BuiltBuildings[]
     */
    public function getBuiltBuildings(): Collection
    {
        return $this->builtBuildings;
    }

    public function addBuiltBuilding(BuiltBuildings $builtBuilding): self
    {
        if (!$this->builtBuildings->contains($builtBuilding)) {
            $this->builtBuildings[] = $builtBuilding;
            $builtBuilding->setCity($this);
        }

        return $this;
    }

    public function removeBuiltBuilding(BuiltBuildings $builtBuilding): self
    {
        // set the owning side to null (unless already changed)
        if ($this->builtBuildings->removeElement($builtBuilding) && $builtBuilding->getCity() === $this) {
            $builtBuilding->setCity(null);
        }

        return $this;
    }

    public function getBuildingLevel(string $buildingName): int
    {
        foreach ($this->builtBuildings as $builtBuilding) {
            if ($builtBuilding->getBuilding()->getName() !== $buildingName) {
                continue;
            }

            return $builtBuilding->getLevel();
        }

        return 0;
    }

    public function isRecruiting(): ?bool
    {
        return $this->recruiting;
    }

    public function setRecruiting(bool $recruiting): self
    {
        $this->recruiting = $recruiting;

        return $this;
    }

    public function getConstructionProject(): ?Constructions
    {
        return $this->constructionProject;
    }

    public function setConstructionProject(?Constructions $constructionProject): self
    {
        $this->constructionProject = $constructionProject;

        return $this;
    }

    public function getProjectLevel(): ?int
    {
        return $this->projectLevel;
    }

    public function setProjectLevel(?int $projectLevel): self
    {
        $this->projectLevel = $projectLevel;

        return $this;
    }

    public function getNode(): WorldMapNode
    {
        if (is_null($this->node)) {
            throw new \LogicException('A city need a node');
        }

        return $this->node;
    }

    public function setNode(WorldMapNode $node): self
    {
        $this->node = $node;

        return $this;
    }

    public function getFreeUnitCount(): int
    {
        $freeUnits = [0, 5, 10, 20, 50, 100];

        return $freeUnits[$this->getBuildingLevel('barracks')];
    }

    public function getDungeonLevel(): int
    {
        return $this->dungeonLevel;
    }

    public function getHousesCount(): int
    {
        return $this->housesCount;
    }

    public function increaseDungeonLevel(): self
    {
        ++$this->dungeonLevel;
        $this->housesCount = 0;

        return $this;
    }

    public function decreaseDungeonLevel(): self
    {
        --$this->dungeonLevel;
        $this->housesCount = 0;

        return $this;
    }

    public function addHouses(int $nbHouses): self
    {
        $this->housesCount += $nbHouses;
        if ($this->housesCount < 0) {
            $this->housesCount = 0;
        }

        return $this;
    }

    public function getDefense(): ArmyContainer
    {
        return $this->defense;
    }

    public function setDefense(ArmyContainer $defense): void
    {
        $this->defense = $defense;
    }
}
