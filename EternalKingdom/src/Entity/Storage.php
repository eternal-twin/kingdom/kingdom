<?php

namespace App\Entity;

use App\Repository\StorageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Storage holds player possession (amount of each resource).
 *
 * @ORM\Entity(repositoryClass=StorageRepository::class)
 */
class Storage
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    // TODO Ensure we can't have multiple storages of the same resource for one city in order to avoid bug!
    // PrimaryKey(CityID, ResourceId)

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="storages")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private City $city;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $resource;
    /**
     * @ORM\Column(type="integer")
     */
    private int $amount;

    public function __construct(
        Resources $resource,
        int $amount
    ) {
        $this->resource = $resource;
        $this->amount = $amount;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getResource(): ?Resources
    {
        return $this->resource;
    }

    public function setResource(?Resources $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
