<?php

use App\Entity\City;
use App\Model\SwapCandidate;
use App\Model\Tile;
use App\Service\GridManager;

beforeEach(function () {
    /** @var ReflectionClass<Lord> */
    $gridManagerReflection = new ReflectionClass(GridManager::class);
    $this->gridManager = $gridManagerReflection->newInstanceWithoutConstructor();

    $this->testGridData = [
        6, 6, 0, 1, 2, 3, 4, 5,
        0, 1, 2, 3, 4, 5, 5, 6,
        6, 6, 2, 1, 5, 3, 4, 5,
        0, 2, 1, 2, 2, 6, 5, 5,
        6, 6, 0, 4, 6, 4, 6, 6,
        0, 1, 2, 3, 4, 6, 1, 4,
        6, 6, 0, 1, 2, 3, 4, 5,
        0, 1, 2, 3, 4, 5, 6, 6,
    ];

    $this->city = new City();
});

test('Grid initialization should generate a grid of 64 tiles', function () {
    $grid = $this->gridManager->initGrid($this->city);
    expect(count($grid))->toBe(64);
});

test('Generated Grid for a city stored as a string should have a size of 64 characters', function () {
    $grid = $this->gridManager->initGrid($this->city);
    $this->city->setGrid($this->gridManager::convertGridToString($grid));

    expect(strlen($this->city->getGrid()))->toBe(64);
});

test('Swapping two valid tiles is a valid move and generates a chain reaction', function () {
    /** @var ReflectionClass<Lord> */
    $gridManagerReflection = new ReflectionClass(GridManager::class);
    $gridManagerReflection->getProperty('gridData')->setValue($this->gridManager, $this->testGridData);

    $grid = $this->gridManager->initGrid($this->city, /*useStaticGrid*/true);
    $this->city->setGrid($this->gridManager::convertGridToString($grid));

    $validSwap = new SwapCandidate(new Tile(1, 7), new Tile(2, 7));
    $canSwap = $this->gridManager->swap($this->city, $validSwap);
    expect($canSwap)->toBe(true);

    $chainReactionSteps = $this->gridManager->getChainReactionSteps();
    expect(count($chainReactionSteps))->toBeGreaterThan(0);
});

test('Swapping two invalid tiles is an invalid move', function () {
    /** @var ReflectionClass<Lord> */
    $gridManagerReflection = new ReflectionClass(GridManager::class);
    $gridManagerReflection->getProperty('gridData')->setValue($this->gridManager, $this->testGridData);

    $grid = $this->gridManager->initGrid($this->city, /*useStaticGrid*/true);
    $this->city->setGrid($this->gridManager::convertGridToString($grid));

    $invalidSwap = new SwapCandidate(new Tile(0, 0), new Tile(1, 0));
    $canSwap = $this->gridManager->swap($this->city, $invalidSwap);
    expect($canSwap)->toBe(false);

    $chainReactionSteps = $this->gridManager->getChainReactionSteps();
    expect(count($chainReactionSteps))->toBe(0);
});
