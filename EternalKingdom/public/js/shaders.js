const background_vs = `#version 300 es
in vec2 a_position;
in vec2 a_texCoords;

out vec2 v_texCoords;

void main() 
{
    gl_Position = vec4(a_position, 0., 1.);
    v_texCoords = a_texCoords;
}
`;

const background_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_texture;
in vec2 v_texCoords;
out vec4 color;

void main() 
{
    color = texture(u_texture, v_texCoords);
}
`;

const images_vs = `#version 300 es
in vec3 a_position;
in vec2 a_texCoords;
out vec2 v_texCoords;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};
uniform vec2 u_translation;

void main() 
{
    float z = u_ratio.z * a_position.z - 1.;
    gl_Position = vec4(u_ratio.xy * (a_position.xy - u_translation.xy) - vec2(1.) , z, 1.);
    v_texCoords = a_texCoords;
}
`;

const images_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_texture;
in vec2 v_texCoords;
out vec4 color;

void main() 
{
    color = texture(u_texture, v_texCoords);
    if(color.a == 0.)
        discard;
}
`;

const inChunkWaves_vs = `#version 300 es
#define M_PI 3.1415926535897932384626433832795

in vec3 a_position;
in vec2 a_texCoords;
out vec2 v_texCoords;
out float v_offset;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};

layout (std140) uniform time {
    float u_time;
};

uniform vec2 u_translation;

void main() 
{
    v_offset = cos(0.4 * M_PI * u_time + float(gl_VertexID));
    vec2 position = a_position.xy + v_offset * vec2(4., -2.);
    float z = u_ratio.z * a_position.z - 1.;
    gl_Position = vec4(u_ratio.xy * (position - u_translation.xy) - vec2(1.) , z, 1.);
    v_texCoords = a_texCoords;
}
`;

const waves_vs = `#version 300 es
#define M_PI 3.1415926535897932384626433832795
in vec3 a_position;
in vec2 a_texCoords;
out vec2 v_texCoords;
out float v_offset;

layout (std140) uniform viewParameters {
    vec3 u_ratio;
    vec2 u_viewPosition;
    float u_zoom;
};

layout (std140) uniform time {
    float u_time;
};

uniform vec2 u_translation;

void main() 
{
    v_offset = cos(0.4 * M_PI * u_time + float(gl_VertexID));
    vec2 position = a_position.xy + v_offset * vec2(4., -2.);
    float z = u_ratio.z * a_position.z - 1.;
    gl_Position = vec4((position - u_viewPosition) * u_ratio.xy / u_zoom, z, 1.);
    v_texCoords = a_texCoords;
}
`;

const waves_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_texture;
in vec2 v_texCoords;
in float v_offset;
out vec4 color;

void main() 
{
    color = texture(u_texture, v_texCoords);
    float alpha = color.a * min(1., 0.2 + v_offset);
    color = vec4(color.rgb, alpha);
}
`;

const distToLinks_vs =`#version 300 es
in vec2 a_position;
in float a_dist;

layout (std140) uniform mapGenSize {
    vec2 u_mapGenSize;
};

out float distance;

void main() 
{
    gl_Position = vec4(2. * a_position / u_mapGenSize - vec2(1.), 1., 1.);
    distance = a_dist;
}
`;

const distToLinks_fs =`#version 300 es
precision mediump float;

in float distance;
out vec4 color;

void main() 
{
    color = vec4(abs(distance),0. , 0., 1.);
}
`;

const randomGreenBlue_vs = `#version 300 es

in vec2 a_position;

void main() {
    gl_Position = vec4(a_position, -1.0, 1.);
}
`;

const randomGreenBlue_fs = `#version 300 es
precision mediump float;
precision highp int;

layout (std140) uniform mapGenSize {
    vec2 u_mapGenSize;
};

out vec4 color;

uint pcgHash(uint seed)
{
    uint state = seed * 747796405u + 2891336453u;
    uint word  = ((state >> ((state >> 28u) + 4u)) ^ state)*277803737u;
    return (word >> 22u) ^ word;
}

void main() 
{
    uint seed = uint(gl_FragCoord.x) + uint(gl_FragCoord.y * u_mapGenSize.x);
    uint green = pcgHash(seed);
    uint blue = pcgHash(green);
    vec2 greenBlue = vec2(float(green), float(blue)) / float(uint(0xffffffff));
    color = vec4(0., greenBlue, 1.);
}
`;

const zones_vs = `#version 300 es
in vec2 a_position;

layout (std140) uniform mapGenSize {
    vec2 u_mapGenSize;
};

out vec2 v_texCoords;
out float mapGenWidth;


void main() 
{
    v_texCoords = a_position / u_mapGenSize;
    mapGenWidth = u_mapGenSize.x;
    gl_Position = vec4(2. * v_texCoords - vec2(1.), 1., 1.);
}
`;

const inLake_fs = `#version 300 es
precision mediump float;

in vec2 v_texCoords;
uniform sampler2D u_distAndRandomTex;
uniform float u_minDist;

out vec4 color;

void main() 
{
    float inLake = 0.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(2, 2), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(3, 2), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(4, 2), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(5, 2), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(5, 3), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(5, 4), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(5, 5), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(4, 5), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(3, 5), 0).r >= u_minDist) inLake += 1.;
    if(texelFetch(u_distAndRandomTex, ivec2(gl_FragCoord * 4.) - ivec2(2, 5), 0).r >= u_minDist) inLake += 1.;

    color = vec4(inLake / 255., 0., 0., 0.);
}
`;

const communZones_fs = `#version 300 es
precision mediump float;
precision highp int;

in vec2 v_texCoords;
in float mapGenWidth;

uniform sampler2D u_distAndRandomTex;
uniform int u_landform;

out vec4 color;

uint pcgHash(uint seed)
{
    uint state = seed * 747796405u + 2891336453u;
    uint word = ((state >> ((state >> 28u) + 4u)) ^ state) * 277803737u;
    return ((word >> 22u) ^ word);
}

float pseudoRandom()
{
    uint hash = pcgHash(uint(gl_FragCoord.x) + uint(gl_FragCoord.y * mapGenWidth));
    return float(hash % 255u) / 255.;
}
`;

const lakesZones_fs  = `
uniform sampler2D u_inLakeTex;
uniform float u_threshold;

void main() 
{
    float inLakeId = 0.;
    
    ivec2 grid = ivec2(gl_FragCoord);
    if ((grid.x % 4 == 0) && (grid.y % 4 == 0))
    {
        if(texelFetch(u_inLakeTex, ivec2(gl_FragCoord / 4.), 0).r >= u_threshold)
            inLakeId += 1.;
        if(texelFetch(u_inLakeTex, ivec2(gl_FragCoord / 4.) + ivec2(1, 0), 0).r >= u_threshold)
            inLakeId += 2.;
        if(texelFetch(u_inLakeTex, ivec2(gl_FragCoord / 4.) + ivec2(0, 1), 0).r >= u_threshold)
            inLakeId += 8.;
        if(texelFetch(u_inLakeTex, ivec2(gl_FragCoord / 4.) + ivec2(1), 0).r >= u_threshold)
            inLakeId += 4.;
    }

    float landformId = ( float(u_landform) * 10. + inLakeId ) / 255.;

    uint landscapeSpriteId = 0u;
    uint waveSpriteId = 0u;
    if(inLakeId > 0.)
    {
        landscapeSpriteId = pcgHash(uint(gl_FragCoord.x) + uint(gl_FragCoord.y * mapGenWidth));
        waveSpriteId = pcgHash(landscapeSpriteId);
    }
    color = vec4(landformId, float((waveSpriteId % 255u) + 1u) / 255., float((landscapeSpriteId % 255u) + 1u) / 255., 1.);
}
`;

const mountainsDesertZones_fs = `
uniform float u_minDist;
uniform vec2 u_maxRandValue;

void main() 
{
    float landformId = float(u_landform) * 10. / 255.;
    float groundSpriteId = 0.;
    float landscapeSpriteId = 0.;

    vec4 distAndRandom = texture(u_distAndRandomTex, v_texCoords);
    if( distAndRandom.g <= u_maxRandValue.x)
    {
        groundSpriteId = 1. / 255.;
    }
    if( (distAndRandom.r >= u_minDist) && (distAndRandom.b <= u_maxRandValue.y))
    {
        landscapeSpriteId = 1. / 255. + pseudoRandom();
    }
    color = vec4(landformId, groundSpriteId, landscapeSpriteId, 1.);
}
`;

const dirtZones_fs = `
uniform float u_minDist;
uniform float u_maxRandValue;

void main() 
{
    float landformId = float(u_landform) * 10. / 255.;
    float landscapeSpriteId = 0.;

    vec4 distAndRandom = texture(u_distAndRandomTex, v_texCoords);
    if( (distAndRandom.r >= u_minDist) && (distAndRandom.b <= u_maxRandValue))
    {
        landscapeSpriteId = 1. / 255. + pseudoRandom();
    }
    color = vec4(landformId, 0., landscapeSpriteId, 1.);
}
`;

const forestHillsZones_fs = `
uniform vec3 u_randRangeValues;

void main() 
{
    float landformId = float(u_landform) * 10. / 255.;
    float groundSpriteId = 0.;
    float landscapeSpriteId = 0.;

    vec4 distAndRandom = texture(u_distAndRandomTex, v_texCoords);
    if( distAndRandom.g <= u_randRangeValues.x)
    {
        groundSpriteId = 1. / 255.;
    }
    float randLanscape = mod(distAndRandom.b, distAndRandom.r);
    if( ( u_randRangeValues.y <= randLanscape) && (randLanscape <= u_randRangeValues.z ))
    {
        landscapeSpriteId = 1. / 255. + pseudoRandom();
    }
    color = vec4(landformId, groundSpriteId, landscapeSpriteId, 1.);
}
`;

const links_vs = `#version 300 es
in vec4 a_vertex;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};

uniform vec2 u_translation;

out vec2 dist;

void main() 
{
    gl_Position = vec4(u_ratio.xy * (a_vertex.xy - u_translation) - vec2(1.) , 0., 1.);
    dist = a_vertex.zw;
}
`;

const links_fs = `#version 300 es
precision mediump float;

in vec2 dist;

out vec4 color;

void main() 
{
    float alpha = 1. - smoothstep(0.4, 0.6, abs(dist.x));
    alpha = alpha * step(0.5, mod(dist.y, 1.)) * 0.9;
    color = vec4(0.29, 0.18, 0.011, alpha);
}
`;

const resourcePanels_vs = `#version 300 es
in vec2 a_position;
in vec2 a_texCoords;
in vec2 a_instanceTranslation;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};
uniform vec2 u_translation;

out vec2 v_texCoords;

void main() 
{
    gl_Position = vec4(u_ratio.xy * (a_position + a_instanceTranslation - u_translation) - vec2(1.) , 0., 1.);
    v_texCoords = a_texCoords;
}
`;

const resourcePanels_fs = `#version 300 es
precision mediump float;

in vec2 v_texCoords;
uniform sampler2D u_texture;

out vec4 color;

void main() 
{
    color = texture(u_texture, v_texCoords);
}
`;

const resources_vs = `#version 300 es
in vec2 a_position;
in vec2 a_instanceTranslation;
in vec2 a_texCoordOffset;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};
uniform vec2 u_translation;
uniform vec2 u_texCoordRatio;

out vec2 v_texCoords;

void main() 
{
    gl_Position = vec4(u_ratio.xy * (a_position + a_instanceTranslation - u_translation) - vec2(1.) , 0., 1.);
    v_texCoords = a_texCoordOffset + a_position * u_texCoordRatio;
}
`;

const resources_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_texture;
in vec2 v_texCoords;
out vec4 color;

void main() 
{
    color = texture(u_texture, v_texCoords);
}
`;

const chunksToScreen_vs = `#version 300 es
in vec2 a_position;
in vec2 a_texCoords;

layout (std140) uniform viewParameters {
    vec3 u_ratio;
    vec2 u_viewPosition;
    float u_zoom;
};

uniform vec2 u_chunk_translation;

out vec2 v_texCoords;

void main() 
{
    v_texCoords = a_texCoords;
    gl_Position = vec4((a_position + u_chunk_translation - u_viewPosition) * u_ratio.xy / u_zoom , -1.0, 1.);
}
`;

const chunksToScreen_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_texture;
uniform sampler2D u_depth;
in vec2 v_texCoords;
out vec4 color;

void main() 
{
    color = vec4(texture(u_texture, v_texCoords).rgb, 1.);
    gl_FragDepth = texture(u_depth, v_texCoords).r;
}
`;

const chunksToSelection_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_texture;
in vec2 v_texCoords;
out vec4 color;

void main() 
{
    color = texture(u_texture, v_texCoords);
}
`;


const kingdoms_vs = `#version 300 es
in vec2 a_position;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};
uniform vec2 u_translation;


void main() 
{
    gl_Position = vec4(u_ratio.xy * (a_position - u_translation) - vec2(1.) , 0., 1.);
}
`;

const kingdoms_fs = `#version 300 es
precision mediump float;

uniform float u_kingdoms;

out vec4 color;

void main() 
{
    color.b = u_kingdoms;
}
`;

const frontiers_fs = `#version 300 es
precision mediump float;

out vec4 color;

void main() 
{
    color.a = 1.;
}
`;

const clickable_vs = `#version 300 es
in vec2 a_position;
in vec2 a_cityTranslation;
in vec2 a_color;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};

uniform vec2 u_translation;

out vec2 v_color;

void main() 
{
    float z = u_ratio.z * a_cityTranslation.y - 1.;
    gl_Position = vec4(u_ratio.xy * (a_position + a_cityTranslation - u_translation) - vec2(1.) , z, 1.);
    v_color = a_color;
}
`;

const clickable_fs = `#version 300 es
precision mediump float;

in vec2 v_color;
out vec4 color;

void main() 
{
    color = vec4(v_color, 0., 0.);
}
`;

const easterEggSelection_vs = `#version 300 es
in vec2 a_position;

layout (std140) uniform chunksRatio {
    vec3 u_ratio;
};

uniform vec2 u_translation;

void main() 
{
    float z = u_ratio.z * a_position.y - 1.;
    gl_Position = vec4(u_ratio.xy * (a_position - u_translation) - vec2(1.) , z, 1.);
}
`;

const easterEggSelection_fs = `#version 300 es
precision mediump float;

uniform vec2 u_color;
out vec4 color;

void main() 
{
    color = vec4(u_color, 0., 0.);
}
`;

const generals_vs = `#version 300 es
in vec3 a_position;
in vec2 a_texCoords;
in vec4 a_generalTranslation;
in float a_hue;

layout (std140) uniform viewParameters {
    vec3 u_ratio;
    vec2 u_viewPosition;
    float u_zoom;
};

layout (std140) uniform time {
    float u_time;
};

out vec3 texCoords;
out float hue;

void main()
{
    float z = u_ratio.z * (a_generalTranslation.y + a_position.z) - 1.;
    gl_Position = vec4((a_position.xy + a_generalTranslation.xy - u_viewPosition) * u_ratio.xy / u_zoom, z, 1.);
    float animation = step(fract(u_time + 0.99), 0.5) * (a_generalTranslation.a - 1.) / 9.;
    texCoords = vec3(a_texCoords, a_generalTranslation.z + animation);
    hue = a_hue;
}
`;

const generals_fs = `#version 300 es
precision mediump float;

in vec3 texCoords;
in float hue;

uniform sampler2D u_texture;

out vec4 color;

vec3 hueShift (vec3 inColor, float shift)
{
    vec3 P = vec3(0.55735) * dot(vec3(0.55735), inColor);
    vec3 U = inColor - P;
    vec3 V = cross(vec3(0.55735), U);    
    vec3 outColor = U * cos(shift * 6.2832) + V * sin(shift * 6.2832) + P;
    return outColor;
}

void main() 
{
    color = texture(u_texture, vec2(texCoords.x + texCoords.z, texCoords.y));
    color.xyz = hueShift(color.xyz, hue);
    if(color.a == 0.) discard;
}
`;

const generalsSelection_vs = `#version 300 es
in vec3 a_position;
in vec2 a_generalTranslation;
in vec2 a_color;

layout (std140) uniform viewParameters {
    vec3 u_ratio;
    vec2 u_viewPosition;
    float u_zoom;
};

out vec2 v_color;

void main() 
{
    float z = u_ratio.z * a_generalTranslation.y - 1.;
    gl_Position = vec4((a_position.xy + a_generalTranslation.xy - u_viewPosition) * u_ratio.xy / u_zoom, z, 1.);
    v_color = a_color;
}
`;

const generalsSelection_fs = `#version 300 es
precision mediump float;

in vec2 v_color;
out vec4 color;

void main() 
{
    color = vec4(v_color, 0., 0.);
}
`;

const kingdomView_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_selectionTex;
uniform int u_playerId;
uniform vec4 u_kingdomColor;

in vec2 v_texCoords;
out vec4 color;

void main() 
{
    vec4 kingdoms = texture(u_selectionTex, v_texCoords);
    int iPlayerId = int(kingdoms.b * 255.) - 1;
    if (iPlayerId != u_playerId) discard;

    color = u_kingdomColor;
}
`;

const strategic_fs = `#version 300 es
precision mediump float;

uniform sampler2D u_selectionTex;
uniform sampler2D u_kingdomColorTex;

in vec2 v_texCoords;
out vec4 color;

void main() 
{
    vec3 frontierColor = vec3(0.1, 0.05, 0.05);

    vec4 kingdoms = texture(u_selectionTex, v_texCoords);
    if(kingdoms.ba == vec2(0.)) discard;

    vec3 kingdomColor;
    if(kingdoms.b == 0.)
    {
        kingdomColor = vec3(0.);
    }
    else
    {
        int iPlayerId = int(kingdoms.b * 255.) - 1;
        kingdomColor = texelFetch(u_kingdomColorTex, ivec2(iPlayerId, 0), 0).rgb;
    }

    if(kingdoms.a == 1.)
    {
        color = vec4(mix(kingdomColor, frontierColor, 0.5), 0.8);
    }
    else
    {
        color = vec4(kingdomColor, 0.5);
    }
}
`;

const generalPath_vs = `#version 300 es
in vec2 a_position;

layout (std140) uniform viewParameters {
    vec3 u_ratio;
    vec2 u_viewPosition;
    float u_zoom;
};

void main() 
{
    gl_Position = vec4((a_position - u_viewPosition) * u_ratio.xy / u_zoom , .0, 1.);

}
`;

const generalPath_fs = `#version 300 es
precision mediump float;

out vec4 color;

void main() 
{
    color = vec4(1., 1., 1., 0.7);
}
`;


const uboParam =
{
    'mapGenSize':
    {
        'variables': ['u_mapGenSize'],
        'programs': [
            'distToLinks', 'inLakes','randomGreenBlue',
            'lakesZones','mountainsDesertZones','dirtZones','forestHillsZones'
        ]
    },
    'chunksRatio':
    {
        'variables': ['u_ratio'],
        'programs': [
            'links', 'images', 'inChunkWaves', 'resourcePanels', 'resources',
            'kingdoms', 'frontiers', 'clickable', 'easterEggSelection'
        ]
    },
    'viewParameters':
    {
        'variables': ['u_ratio', 'u_viewPosition', 'u_zoom'],
        'programs': [
            'chunksToScreen', 'chunksToSelection', 'generals', 'generalsSelection',
            'waves', 'kingdomView', 'strategic', 'generalPath'
        ]
    },
    'time':
    {
        'variables': ['u_time'],
        'programs': [
            'inChunkWaves', 'waves', 'generals'
        ]
    }
};

const shadersParam = 
{
    'background': 
    {
        'vs': background_vs,
        'fs': background_fs,
        'attributes': {'a_position': 2, 'a_texCoords': 2},
        'uniforms': ['u_texture']
    },
    'images': 
    {
        'vs': images_vs,
        'fs': images_fs,
        'attributes': {'a_position': 3, 'a_texCoords': 2}, 
        'uniforms': ['u_translation', 'u_texture']
    },
    'inChunkWaves':
    {
        'vs': inChunkWaves_vs,
        'fs': waves_fs,
        'attributes': {'a_position': 3, 'a_texCoords': 2}, 
        'uniforms': ['u_translation', 'u_texture']
    },
    'waves': 
    {
        'vs': waves_vs,
        'fs': waves_fs,
        'attributes': {'a_position': 3, 'a_texCoords': 2}, 
        'uniforms': ['u_translation', 'u_texture']
    },
    'distToLinks':
    {
        'vs': distToLinks_vs,
        'fs': distToLinks_fs,
        'attributes': {'a_position': 2, 'a_dist': 1}
    },
    'randomGreenBlue':
    {
        'vs': randomGreenBlue_vs,
        'fs': randomGreenBlue_fs,
        'attributes': {'a_position': 2}
    },
    'inLakes':
    {
        'vs': zones_vs,
        'fs': [inLake_fs],
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_distAndRandomTex', 'u_minDist']
    },
    'lakesZones':
    {
        'vs': zones_vs,
        'fs': [communZones_fs + lakesZones_fs],
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_threshold', 'u_landform', 'u_inLakeTex']
    },
    'mountainsDesertZones':
    {
        'vs': zones_vs,
        'fs': [communZones_fs + mountainsDesertZones_fs],
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_distAndRandomTex', 'u_minDist', 'u_maxRandValue', 'u_landform']
    },
    'dirtZones':
    {
        'vs': zones_vs,
        'fs': [communZones_fs + dirtZones_fs],
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_distAndRandomTex', 'u_minDist', 'u_maxRandValue', 'u_landform']
    },
    'forestHillsZones':
    {
        'vs': zones_vs,
        'fs': [communZones_fs + forestHillsZones_fs],
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_distAndRandomTex', 'u_randRangeValues', 'u_landform']
    },
    'links':
    {
        'vs': links_vs,
        'fs': links_fs,
        'attributes': {'a_vertex': 4}, 
        'uniforms': ['u_translation']
    },
    'resourcePanels':
    {
        'vs': resourcePanels_vs,
        'fs': resourcePanels_fs,
        'attributes': {'a_position': 2, 'a_texCoords': 2}, 
        'uniforms': ['u_translation', 'u_texture'],
        'perInstanceAttributes': {'a_instanceTranslation': 2}
    },
    'resources':
    {
        'vs': resources_vs,
        'fs': resources_fs,
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_translation', 'u_texCoordRatio', 'u_texture'],
        'perInstanceAttributes': {'a_instanceTranslation': 2, 'a_texCoordOffset': 2}
    },
    'chunksToScreen':
    {
        'vs': chunksToScreen_vs,
        'fs': chunksToScreen_fs,
        'attributes': {'a_position': 2, 'a_texCoords': 2}, 
        'uniforms': ['u_chunk_translation', 'u_texture', 'u_depth']
    },
    'chunksToSelection':
    {
        'vs': chunksToScreen_vs,
        'fs': chunksToSelection_fs,
        'attributes': {'a_position': 2, 'a_texCoords': 2}, 
        'uniforms': ['u_chunk_translation', 'u_texture']        
    },
    'kingdoms':
    {
        'vs': kingdoms_vs,
        'fs': kingdoms_fs,
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_kingdoms', 'u_translation']
    },
    'frontiers':
    {
        'vs': kingdoms_vs,
        'fs': frontiers_fs,
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_translation']
    },
    'clickable':
    {
        'vs': clickable_vs,
        'fs': clickable_fs,
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_translation'],
        'perInstanceAttributes': {'a_cityTranslation': 2, 'a_color': 2}
    },
    'easterEggSelection':
    {
        'vs': easterEggSelection_vs,
        'fs': easterEggSelection_fs,
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_translation', 'u_color'],
    },
    'generals':
    {
        'vs': generals_vs,
        'fs': generals_fs,
        'attributes': {'a_position': 3, 'a_texCoords': 2}, 
        'uniforms': ['u_translation', 'u_texture'],
        'perInstanceAttributes': {'a_generalTranslation': 4, 'a_hue': 1}
    },
    'generalsSelection':
    {
        'vs': generalsSelection_vs,
        'fs': generalsSelection_fs,
        'attributes': {'a_position': 2}, 
        'uniforms': ['u_translation'],
        'perInstanceAttributes': {'a_generalTranslation': 2, 'a_color': 2}
    },
    'kingdomView':
    {
        'vs': chunksToScreen_vs,
        'fs': kingdomView_fs,
        'attributes': {'a_position': 2, 'a_texCoords': 2}, 
        'uniforms': ['u_chunk_translation', 'u_selectionTex', 'u_playerId', 'u_kingdomColor']
    },
    'strategic':
    {
        'vs': chunksToScreen_vs,
        'fs': strategic_fs,
        'attributes': {'a_position': 2, 'a_texCoords': 2}, 
        'uniforms': ['u_chunk_translation', 'u_selectionTex', 'u_kingdomColorTex']
    },
    'generalPath':
    {
        'vs': generalPath_vs,
        'fs': generalPath_fs,
        'attributes': {'a_position': 2}, 
        'uniforms': [],
    },
};
