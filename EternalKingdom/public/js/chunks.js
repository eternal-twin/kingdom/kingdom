// ETERNAL KINGDOM - Offscreen rendering of map chunks


class ChunkView
{
    static width;
    static height;
    static backgroundMesh;

    column;
    line;
    // offscreen framebuffer used to pre-render the map (non-animated elements only)
    renderFramebuffer;
    // offscreen selection framebuffer used to know what the mouse/finger is selecting
    selectionFramebuffer;
    playersDataChunk;

    //wave can be animated: we need to keep the mesh to render it at each frame
    wavesMesh;

    constructor(column, line)
    {
        this.column = column;
        this.line = line;
        this.renderFramebuffer = new Framebuffer(ChunkView.width, ChunkView.height, true);
        this.selectionFramebuffer = new Framebuffer(ChunkView.width, ChunkView.height, true);
        gl.bindTexture(gl.TEXTURE_2D, this.selectionFramebuffer.texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        this.wavesMesh = null;
    }    


    render(zonePixels, zoneWidth, zoneHeight)
    {
        let groundVertices = [];
        let groundIndices = [];
        let nbGroundVertices = 0;
        let landscapeVertices = [];
        let landscapeIndices = [];
        let nbLandscapeVertices = 0;
        let lakesVertices = [];
        let lakesIndices = [];
        let nbLakesVertices = 0;
        let wavesVertices = [];
        let wavesIndices= [];
        let nbWavesVertices = 0;
    
        const landform = ['mountains', 'snowy_mountains', 'volcanos', 'sand', 'dirt', 'forest', 'hills', 'lake'];
        const nbSamplesX = ChunkView.width / MAP_SCALE_FACTOR;
        const nbSamplesY = ChunkView.height / MAP_SCALE_FACTOR;
    
        let firstPx = Math.max(nbSamplesX * this.column - Math.ceil(groundMinMax[1] / MAP_SCALE_FACTOR), 0);
        let lastPx = Math.min(nbSamplesX * (this.column + 1) - Math.ceil(groundMinMax[0] / MAP_SCALE_FACTOR), zoneWidth - 1);
        let firstPy = Math.max(nbSamplesY * this.line - Math.ceil(groundMinMax[3] / MAP_SCALE_FACTOR), 0);
        let lastPy = Math.min(nbSamplesY * (this.line + 1) - Math.ceil(groundMinMax[2] / MAP_SCALE_FACTOR), zoneHeight - 1);
    
        let landscapeFirstPx = Math.max(nbSamplesX * this.column - Math.ceil(landscapeMinMax[1] / MAP_SCALE_FACTOR), 0);
        let landscapeLastPx = Math.min(nbSamplesX * (this.column + 1) - Math.ceil(landscapeMinMax[0] / MAP_SCALE_FACTOR), zoneWidth - 1);
        let landscapeFirstPy = Math.max(nbSamplesY * this.line - Math.ceil(landscapeMinMax[3] / MAP_SCALE_FACTOR), 0);
        let landscapeLastPy = Math.min(nbSamplesY * (this.line + 1) - Math.ceil(landscapeMinMax[2] / MAP_SCALE_FACTOR), zoneHeight - 1);
    
        let nbWaveSprites = spritesData["waves"]["sprites"].length;
        let tileSize = spritesData["lake"]["tile_size"];
        let waveHalfHeight = Math.round(0.5 * spritesData["waves"]["sprites"][0][3]);

        for(let py = lastPy; py >= firstPy ; py--)
        {
            let y = MAP_SCALE_FACTOR * py;
    
            for(let px = firstPx; px <= lastPx; px++)
            {
                let x = MAP_SCALE_FACTOR * px;
    
                let redPixelId = 4 * (px + py * zoneWidth);
                let landformId = zonePixels[redPixelId];
                let groundSpriteId = zonePixels[redPixelId + 1];
                let landscapeSpriteId = zonePixels[redPixelId + 2];
                
                if (landformId > 70) 
                {
                    if((px >= nbSamplesX * this.column)&&(py >= nbSamplesY * this.line)&&(px < nbSamplesX * (this.column + 1))&&(py < nbSamplesY * (this.line + 1)))
                    {
                        let cellId = landformId - 70;
                        let cellName = "cell_" + cellId.toString();
                        let spriteId = (landscapeSpriteId - 1) % spritesData["lake"]["sprites"][cellName].length;
                        nbLakesVertices += addSprite(lakesVertices, lakesIndices, nbLakesVertices, x, y, lakesSprites[cellName][spriteId]);
                    
                        let waveId = groundSpriteId;
                        function addWave(xWave, yWave)
                        {
                            spriteId = waveId % nbWaveSprites;
                            nbWavesVertices += addSprite(wavesVertices, wavesIndices, nbWavesVertices, xWave, yWave, wavesSprites[spriteId]);
                            waveId = (waveId * 137 + 187) % 256;
                        }

                        if (cellId == 1)
                            addWave(x + 0.1 * tileSize, y + 0.05 * tileSize - waveHalfHeight);
                        else if (cellId == 2)
                            addWave(x + 0.9 * tileSize, y + 0.05 * tileSize - waveHalfHeight);
                        else if (cellId == 4)
                            addWave(x + 0.9 * tileSize, y + 0.95 * tileSize - waveHalfHeight);
                        else if (cellId == 8)
                            addWave(x + 0.1 * tileSize, y + 0.95 * tileSize - waveHalfHeight);
                        else
                        {
                            if ([3, 5, 7, 9, 11, 13, 15].includes(cellId))
                                addWave(x + 0.2 * tileSize, y + 0.2 * tileSize - waveHalfHeight);
                            if ([3, 6, 7, 10, 11, 14, 15].includes(cellId))
                                addWave(x + 0.8 * tileSize, y + 0.2 * tileSize - waveHalfHeight);
                            if ([5, 6, 7, 12, 13, 14, 15].includes(cellId))
                                addWave(x + 0.8 * tileSize, y + 0.8 * tileSize - waveHalfHeight);
                            if ([9, 10, 11, 12, 13, 14, 15].includes(cellId))
                                addWave(x + 0.2 * tileSize, y + 0.8 * tileSize - waveHalfHeight);
                            if ([3, 7, 11, 15].includes(cellId))
                                addWave(x + 0.5 * tileSize, y + 0.3 * tileSize - waveHalfHeight);
                            if ([6, 10, 14, 15].includes(cellId))
                                addWave(x + 0.9 * tileSize, y + 0.5 * tileSize - waveHalfHeight);
                            if ([12, 13, 14, 15].includes(cellId))
                                addWave(x + 0.5 * tileSize, y + 0.9 * tileSize - waveHalfHeight);
                            if ([9, 11, 13, 15].includes(cellId))
                                addWave(x + 0.1 * tileSize, y + 0.5 * tileSize - waveHalfHeight);
                        }
                    }
                } else if (landformId < 70) {
                    landformId = landformId / 10;

                    if (groundSpriteId)
                    {
                        let groundName = spritesData["landforms_ground"][landform[landformId]];
                        nbGroundVertices += addSprite(groundVertices, groundIndices, nbGroundVertices, x, y, groundSprites[groundName]);
                    }
                    if ((landscapeSpriteId)&&(px >= landscapeFirstPx)&&(px <= landscapeLastPx)&&(py >= landscapeFirstPy)&&(py <= landscapeLastPy))
                    {
                        let landformName = landform[landformId];
                        let spriteId = (landscapeSpriteId - 1) % spritesData["landforms_sprites"][landformName].length;
                        let spriteName = spritesData["landforms_sprites"][landformName][spriteId];
                        nbLandscapeVertices += addSprite(landscapeVertices, landscapeIndices, nbLandscapeVertices, x, y, landscapeSprites[spriteName]);
                    }
                }
            }
        }
    
        let groundVertexBuffer = new VertexBuffer(groundVertices);
        let groundIndexBuffer = new IndexBuffer(groundIndices);
        let groundMesh = new IndicedMesh2d(groundVertexBuffer, groundIndexBuffer, shaders['images']);
        let landscapeVertexBuffer = new VertexBuffer(landscapeVertices);
        let landscapeIndexBuffer = new IndexBuffer(landscapeIndices);
        let landscapeMesh = new IndicedMesh2d(landscapeVertexBuffer, landscapeIndexBuffer, shaders['images']);
        let lakesVertexBuffer = new VertexBuffer(lakesVertices);
        let lakesIndexBuffer = new IndexBuffer(lakesIndices);
        let lakesMesh = new IndicedMesh2d(lakesVertexBuffer, lakesIndexBuffer, shaders['images']);
    
        if (nbWavesVertices > 0)
        {
            let wavesVertexBuffer = new VertexBuffer(wavesVertices);
            let wavesIndexBuffer = new IndexBuffer(wavesIndices);
            this.wavesMesh = new IndicedMesh2d(wavesVertexBuffer, wavesIndexBuffer, shaders['waves']);
        }

        this.#drawMeshes(groundMesh, lakesMesh, landscapeMesh);

        groundMesh.deleteGlObject();
        groundVertexBuffer.deleteGlObject();
        groundIndexBuffer.deleteGlObject();
        landscapeMesh.deleteGlObject();
        landscapeVertexBuffer.deleteGlObject();
        landscapeIndexBuffer.deleteGlObject();
        lakesMesh.deleteGlObject();
        lakesVertexBuffer.deleteGlObject();
        lakesIndexBuffer.deleteGlObject();

        //TODO: add graphic option?
        gl.bindTexture(gl.TEXTURE_2D, this.renderFramebuffer.texture);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    }

    #drawMeshes(groundMesh, lakesMesh, landscapeMesh)
    {
        this.renderFramebuffer.bindAndSetViewport();
        gl.clearColor(0.0, 0.0, 0.0, 0.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
        shaders['background'].use();
        gl.uniform1i(shaders['background'].uniformLoc['u_texture'], 0);
        textures['background'].bind();
        ChunkView.backgroundMesh.draw();
    
        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        
        shaders['images'].use();
        gl.uniform1i(shaders['images'].uniformLoc['u_texture'], 0);
        gl.uniform2fv(
            shaders['images'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );
        textures['ground'].bind();
        groundMesh.draw();
        textures['lake'].bind();
        lakesMesh.draw();
    
        shaders['links'].use();
        gl.uniform2fv(
            shaders['links'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );
        linksMesh.draw();

        gl.enable(gl.DEPTH_TEST);
        gl.depthFunc(gl.LEQUAL);
        shaders['images'].use();
        textures['landscape'].bind();
        landscapeMesh.draw();
        this.playersDataChunk.drawCities();
        easterEggsMesh.draw();
        gl.disable(gl.DEPTH_TEST);

        if (!graphicsSettings.animate && this.wavesMesh != null)
        {
            shaders['inChunkWaves'].use();
            textures['landscape'].bind();
            gl.uniform1i(shaders['inChunkWaves'].uniformLoc['u_texture'], 0);
            gl.uniform2fv(
                shaders['inChunkWaves'].uniformLoc['u_translation'], 
                [this.column * ChunkView.width, this.line * ChunkView.height]
            );
            this.wavesMesh.draw();
        }

        shaders['resourcePanels'].use();
        gl.uniform2fv(
            shaders['resourcePanels'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );
        resourcePanelsMesh.draw();

        shaders['resources'].use();
        gl.uniform2fv(
            shaders['resources'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );
        resourcesMeshes.draw();

        gl.disable(gl.BLEND);
    }

    renderSelection()
    {
        this.selectionFramebuffer.bindAndSetViewport();
        //this.selectionFramebuffer.bindWithCopiedDepthAndSetViewport(this.renderFramebuffer);
        gl.clearColor(0.0, 0.0, 0.0, 0.0);
        //gl.clear(gl.COLOR_BUFFER_BIT);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.colorMask(true, true, false, false);
        //gl.enable(gl.DEPTH_TEST);  
        gl.depthFunc(gl.LEQUAL);
        shaders['clickable'].use();
        gl.uniform2fv(
            shaders['clickable'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );

        for(const cities of citiesRenderableData.citiesSelectionMeshes)
        {
            cities.draw();
        }

        shaders['easterEggSelection'].use();
        gl.uniform2fv(
            shaders['easterEggSelection'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );
        gl.uniform2fv(
            shaders['easterEggSelection'].uniformLoc['u_color'], 
            [0., 1./255]
        );
        easterEggMeshSelection.draw();

        gl.disable(gl.DEPTH_TEST);  
        gl.colorMask(false, false, true, false);
        shaders['kingdoms'].use();
        gl.uniform2fv(
            shaders['kingdoms'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );

        for (const [playerId, kingdom] of Object.entries(this.playersDataChunk.kingdomsColorMeshes))
        {
            gl.uniform1f(shaders['kingdoms'].uniformLoc['u_kingdoms'], (parseInt(playerId) + 1) / 255.);
            kingdom.draw();
        }
    
        gl.colorMask(false, false, false, true);
        shaders['frontiers'].use();
        gl.uniform2fv(
            shaders['frontiers'].uniformLoc['u_translation'], 
            [this.column * ChunkView.width, this.line * ChunkView.height]
        );
        this.playersDataChunk.kingdomsFrontiersMesh.draw();
    
        gl.colorMask(true, true, true, true);
    }
}


class PlayersDataChunk
{
    citiesMesh;
    kingdomsColorMeshes;
    kingdomsFrontiersMesh;
    hasCitiesData;
    hasStrategicData;

    constructor()
    {
        this.hasCitiesData = false;
        this.hasStrategicData = false;
    }

    setCityMesh(citiesMesh)
    {
        this.citiesMesh = citiesMesh;
        this.hasCitiesData = true;
    }

    setKingdomsMeshes(kingdomsColorMeshes, kingdomsFrontiersMesh)
    {
        this.kingdomsColorMeshes = kingdomsColorMeshes;
        this.kingdomsFrontiersMesh = kingdomsFrontiersMesh;
        this.hasStrategicData = true;
    }

    drawCities()
    {
        this.citiesMesh.draw();
    }
}



