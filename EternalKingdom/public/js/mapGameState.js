// ETERNAL KINGDOM - Players related stuff to render in the map


class CitySprite
{
    x;
    y;
    spriteData;

    constructor(x, y, spriteData)
    {
        this.x = x;
        this.y = y;
        this.spriteData = spriteData;
    }
}

class CitiesRenderableData
{
    spriteList;
    citiesSelectionMeshes;

    constructor()
    {
        this.spriteList = [];
        this.citiesSelectionMeshes = [];
    }

    loadCities(firstCityColorId)
    {
        let citiesSelection = [];
        let spriteArray = citiesSprites["city"];
        let cityVertices = [
            spriteArray[0], spriteArray[4], 
            spriteArray[2], spriteArray[4], 
            spriteArray[2], spriteArray[6], 
            spriteArray[0], spriteArray[6]
        ];
        citiesSelection.push({'vertices': cityVertices, 'perInstanceData': []});
        for( let towerLvl = 0; towerLvl < 4; towerLvl++)
        {
            spriteArray = citiesSprites["towers"][towerLvl];
            cityVertices = [
                spriteArray[0], spriteArray[4], 
                spriteArray[2], spriteArray[4], 
                spriteArray[2], spriteArray[8], 
                spriteArray[0], spriteArray[8]
            ];
            citiesSelection.push({'vertices': cityVertices, 'perInstanceData': []});
        }

        for(let cityId = 0; cityId < mapData["cities"].length ; cityId ++)
        {
            let cityGraphData = mapData["cities"][cityId];
            let x = cityGraphData["x"] * MAP_SCALE_FACTOR;
            let y = cityGraphData["y"] * MAP_SCALE_FACTOR;
            let colorId = firstCityColorId + cityId;
            let red = Math.floor(colorId / 256);
            let green = colorId % 256;
            
            if (cityGraphData["cap"])
            {
                let towerLvl = 0;
                let nbHouses = 0;
                let capitalSeed = cityId * Math.PI;
                let pseudoRandom = seededRandom({seed: capitalSeed.toString()});

                if(cityGraphData["name"] in playerCapitals)
                {
                    towerLvl = playerCapitals[cityGraphData["name"]].towerLevel - 1;
                    nbHouses = playerCapitals[cityGraphData["name"]].nbHouses;
                }
                else
                {
                    nbHouses = pseudoRandom(4);
                }
                
                this.spriteList.push(new CitySprite(x, y, citiesSprites["towers"][towerLvl]));

                if(nbHouses)
                {
                    let houses = Array(nbHouses);
                    let houseLvl = Math.min(towerLvl, 2);
                    
                    for(let i = 0 ; i < nbHouses ; i++)
                    {
                        let positionId = pseudoRandom(housesPositions.length);
                        let houseId = houseLvl ? pseudoRandom(3) : pseudoRandom(1);
                        houses[i] = [positionId, houseId];
                    }
                    
                    for(const house of houses) 
                    {
                        this.spriteList.push(new CitySprite(x + housesPositions[house[0]][0], y + housesPositions[house[0]][1], citiesSprites["houses"][houseLvl][house[1]]));
                    }
                }
                citiesSelection[towerLvl + 1]['perInstanceData'].push(x, y, red / 255., green / 255.);
            }
            else
            {
                this.spriteList.push(new CitySprite(x, y, citiesSprites["city"]));
                citiesSelection[0]['perInstanceData'].push(x, y, red / 255., green / 255.);
            }
        }

        let indexBuffer = new IndexBuffer([0, 1, 2, 0, 2, 3]);
        for (let cities of citiesSelection)
        {
            this.citiesSelectionMeshes.push(new InstancedIndicedMesh2d(
                new VertexBuffer(cities['vertices']), 
                new VertexBuffer(cities['perInstanceData']),
                indexBuffer, shaders['clickable']));
        }
    }
}


function prepareKingdomsColorTex()
{
    let nbPlayers = Object.keys(lordsIds).length;
    let imgKingdomsColorBuffer = new Uint8Array(nbPlayers * 4);

    for (let i = 0; i < lordsIds.length; i++)
    {
        const owner = lordsDict[lordsIds[i]];
        let colorId = hashCode(owner.ownerName) % kingdomColors.length;
        if (colorId < 0) colorId += kingdomColors.length;
        let color = kingdomColors[colorId];
        imgKingdomsColorBuffer[4 * i] = color[0];
        imgKingdomsColorBuffer[4 * i + 1] = color[1];
        imgKingdomsColorBuffer[4 * i + 2] = color[2];
        imgKingdomsColorBuffer[4 * i + 3] = 255;
    }
    kingdomsColorTex = Texture.loadFromArray(nbPlayers, 1, imgKingdomsColorBuffer, false);
}


class KingdomsAndFrontiers
{
    voronoiVertices;
    kingdomsColorIndices;
    edgeToPlayer;
    frontierVertices;
    frontierIndices;

    constructor(nbChunksX, nbChunksY)
    {
        this.#fillKingdomsArrays(nbChunksX, nbChunksY);
        this.#fillFrontiersArrays(nbChunksX, nbChunksY);   
    }

    #fillKingdomsArrays(nbChunksX, nbChunksY)
    {
        this.voronoiVertices = mapData["voronoi_vertices"].map(x => x * MAP_SCALE_FACTOR);

        let voronoiVerticesChunks = []
        for(let i = 0 ; 2 * i < this.voronoiVertices.length ; i ++)
        {
            voronoiVerticesChunks.push(Math.min(nbChunksX - 1, Math.max(0, Math.floor(this.voronoiVertices[2 * i]/ ChunkView.width))));
            voronoiVerticesChunks.push(Math.min(nbChunksY - 1, Math.max(0, Math.floor(this.voronoiVertices[2 * i + 1]/ ChunkView.height))));
        }

        this.kingdomsColorIndices = new Array(nbChunksX);
        for (let i = 0; i < nbChunksX; i++)
        {
            this.kingdomsColorIndices[i] = new Array(nbChunksY);
            for (let j = 0; j < nbChunksY; j++) 
            {
                this.kingdomsColorIndices[i][j] = {};
            }
        }

        this.edgeToPlayer = {};

        for (let i = 0; i < mapData["cities"].length; i++)
        {
            let node = nodesDict[mapData["cities"][i]['name']];
            let playerId = -1;
            if (node != null && node.isOccupied())
            {
                playerId = lordsDict[node.getMostRelevantOwningLordId()].localId;
            }

            if (playerId !== -1) {
                let regionVertices = mapData["voronoi_indices"][i];

                for (let j = 0; j < regionVertices.length; j++)
                {
                    let edgeKey;
                    if(j < regionVertices.length - 1) 
                    {
                        edgeKey = regionVertices[j].toString() + '_' + regionVertices[j + 1].toString();
                    }
                    else
                    {
                        edgeKey = regionVertices[j].toString() + '_' + regionVertices[0].toString();
                    }
                    this.edgeToPlayer[edgeKey] = playerId;

                    if((j === 0) || (j === regionVertices.length - 1))
                        continue;

                    let chunksX = [
                        voronoiVerticesChunks[2 * regionVertices[0]],
                        voronoiVerticesChunks[2 * regionVertices[j]], 
                        voronoiVerticesChunks[2 * regionVertices[j + 1]]
                    ];
                    let chunksY = [
                        voronoiVerticesChunks[2 * regionVertices[0] + 1],
                        voronoiVerticesChunks[2 * regionVertices[j] + 1], 
                        voronoiVerticesChunks[2 * regionVertices[j + 1] + 1]
                    ];

                    for (let k = Math.min(...chunksX); k <= Math.max(...chunksX); k++) 
                    {
                        for (let l = Math.min(...chunksY); l <= Math.max(...chunksY); l++) 
                        {
                            if (!(playerId in this.kingdomsColorIndices[k][l])) 
                            {
                                this.kingdomsColorIndices[k][l][playerId] = [];
                            }
                            this.kingdomsColorIndices[k][l][playerId].push(regionVertices[0], regionVertices[j], regionVertices[j+1]);
                        }
                    }
                }
            }
        }
    }

    #buildFrontiers()
    {
        let frontierEdges = {};
        for (const [edgeKey, playerId] of Object.entries(this.edgeToPlayer))
        {
            if(!(playerId in frontierEdges)) frontierEdges[playerId] = [];
            const arrayEdges = edgeKey.split('_');
            const otherEdgeKey = arrayEdges[1] + '_' + arrayEdges[0];
            if(!(otherEdgeKey in this.edgeToPlayer) || (playerId != this.edgeToPlayer[otherEdgeKey]))
            {
                frontierEdges[playerId].push([parseInt(arrayEdges[0]), parseInt(arrayEdges[1])]);
            }
        }
    
        let frontiers = [];
        for (let [playerId, edges] of Object.entries(frontierEdges))
        {
            let incompleteFrontiersArray = edges;
            let maxIterations = incompleteFrontiersArray.length;
            let iteration = 0;
    
            while((incompleteFrontiersArray.length) && (iteration < maxIterations))
            {
                let frontier = incompleteFrontiersArray.pop();
                if(frontier[0] == frontier[frontier.length - 1] )
                {
                    frontier.pop();
                    frontiers.push(frontier);
                    continue;
                }
    
                let connectedEdgeFound = false;
                for(let i = 0; i < incompleteFrontiersArray.length; i++)
                {
                    let otherFrontier = incompleteFrontiersArray[i];
                    if(otherFrontier[0] == frontier[frontier.length - 1])
                    {
                        frontier.pop();
                        incompleteFrontiersArray[i] = [].concat(frontier, otherFrontier);
                        connectedEdgeFound = true;
                        break;
                    }
                    else if(frontier[0] == otherFrontier[otherFrontier.length - 1])
                    {
                        frontier.shift();
                        incompleteFrontiersArray[i] = [].concat(otherFrontier, frontier);
                        connectedEdgeFound = true;
                        break;
                    }
                }
    
                iteration ++;
                if((!connectedEdgeFound) || (iteration > maxIterations))
                {
                    //should not go here!
                    console.log('Error in frontiers determination: a kingdom has an incomplete frontier.')
                    break;
                }
            }
        }
        return frontiers;
    }

    #fillFrontiersArrays(nbChunksX, nbChunksY)
    {
        let frontiers = this.#buildFrontiers(this.edgeToPlayer);

        this.frontierVertices = [];
        this.frontierIndices = new Array(nbChunksX);
        for (let i = 0; i < nbChunksX; i++)
        {
            this.frontierIndices[i] = new Array(nbChunksY);
            for (let j = 0; j < nbChunksY; j++) 
            {
                this.frontierIndices[i][j] = [];
            }
        }


        let currentIndex = 0;
        const halfFrontierWidth = 3.;
        
        for(let frontier of frontiers)
        {
            let frontierVerticesChunksX = [];
            let frontierVerticesChunksY = [];

            let prevPointX = this.voronoiVertices[2 * frontier[frontier.length - 2]];
            let prevPointY = this.voronoiVertices[2 * frontier[frontier.length - 2] + 1];
            let currentPointX = this.voronoiVertices[2 * frontier[frontier.length - 1]];
            let currentPointY = this.voronoiVertices[2 * frontier[frontier.length - 1] + 1];

            let prevVectorX = prevPointX - currentPointX;
            let prevVectorY = prevPointY - currentPointY;
            let norm = Math.sqrt(prevVectorX * prevVectorX + prevVectorY * prevVectorY);
            prevVectorX /= norm;
            prevVectorY /= norm;

            for(let nextPointId of frontier)
            {
                let nextPointX = this.voronoiVertices[2 * nextPointId];
                let nextPointY = this.voronoiVertices[2 * nextPointId + 1];
                let nextVectorX = nextPointX - currentPointX;
                let nextVectorY = nextPointY - currentPointY;
                norm = Math.sqrt(nextVectorX * nextVectorX + nextVectorY * nextVectorY);
                nextVectorX /= norm;
                nextVectorY /= norm;

                let crossProduct = prevVectorX * nextVectorY - prevVectorY * nextVectorX;
                let deltaX, deltaY;
                if(Math.abs(crossProduct) < 0.01)
                {
                    deltaX = halfFrontierWidth * nextVectorY;
                    deltaY = -halfFrontierWidth * nextVectorX;
                }
                else {
                    deltaX = prevVectorX + nextVectorX;
                    deltaY = prevVectorY + nextVectorY;
                    let cosAngle = prevVectorX * nextVectorX + prevVectorY * nextVectorY;
                    let angle = Math.acos(Math.max(-1., Math.min(1., cosAngle)));
                    norm = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                    let norm_sin_half_angle = norm * Math.sin( angle/ 2.);
                    deltaX = halfFrontierWidth * deltaX / norm_sin_half_angle;
                    deltaY = halfFrontierWidth * deltaY / norm_sin_half_angle;

                    if(crossProduct < 0.)
                    {
                        deltaX = -deltaX;
                        deltaY = -deltaY;
                    }
                }
                this.frontierVertices.push(currentPointX + deltaX, currentPointY + deltaY);
                this.frontierVertices.push(currentPointX - deltaX, currentPointY - deltaY);

                frontierVerticesChunksX.push(Math.min(nbChunksX - 1, Math.max(0, Math.floor((currentPointX + deltaX) / ChunkView.width))));
                frontierVerticesChunksY.push(Math.min(nbChunksY - 1, Math.max(0, Math.floor((currentPointY + deltaY) / ChunkView.height))));
                frontierVerticesChunksX.push(Math.min(nbChunksX - 1, Math.max(0, Math.floor((currentPointX - deltaX) / ChunkView.width))));
                frontierVerticesChunksY.push(Math.min(nbChunksY - 1, Math.max(0, Math.floor((currentPointY - deltaY) / ChunkView.height))));
                
                prevVectorX = -nextVectorX;
                prevVectorY = -nextVectorY;
                currentPointX = nextPointX;
                currentPointY = nextPointY;
            }

            let chunksX, chunksY;
            for(let i=0 ; i < frontier.length - 1 ; i++)
            {
                chunksX = [frontierVerticesChunksX[2*i], frontierVerticesChunksX[2*i+1], frontierVerticesChunksX[2*i+2], frontierVerticesChunksX[2*i+3]];
                chunksY = [frontierVerticesChunksY[2*i], frontierVerticesChunksY[2*i+1], frontierVerticesChunksY[2*i+2], frontierVerticesChunksY[2*i+3]];

                for (let k = Math.min(...chunksX); k <= Math.max(...chunksX); k++) 
                {
                    for (let l = Math.min(...chunksY); l <= Math.max(...chunksY); l++) 
                    {
                        this.frontierIndices[k][l].push(currentIndex + 2 * i, currentIndex + 2 * i + 1, currentIndex + 2 * i + 2);
                        this.frontierIndices[k][l].push(currentIndex + 2 * i + 2, currentIndex + 2 * i + 1, currentIndex + 2 * i + 3);
                    }
                }
            }

            chunksX = [frontierVerticesChunksX[2*frontier.length - 2], frontierVerticesChunksX[2*frontier.length - 1], frontierVerticesChunksX[0], frontierVerticesChunksX[1]];
            chunksY = [frontierVerticesChunksY[2*frontier.length - 2], frontierVerticesChunksY[2*frontier.length - 1], frontierVerticesChunksY[0], frontierVerticesChunksY[1]];
            
            
            for (let k = Math.min(...chunksX); k <= Math.max(...chunksX); k++) 
            {
                for (let l = Math.min(...chunksY); l <= Math.max(...chunksY); l++) 
                {
                    this.frontierIndices[k][l].push(currentIndex + 2 * (frontier.length - 1), currentIndex + 2 * (frontier.length - 1) + 1, currentIndex);
                    this.frontierIndices[k][l].push(currentIndex, currentIndex + 2 * (frontier.length - 1) + 1, currentIndex + 1);
                }
            }
            currentIndex += 2 * frontier.length;
        }
    }
}


function prepareResourcesMeshes()
{
    let spriteSheetWidth = textures[spritesData["resources"]["source"]].width;
    let spriteSheetHeight = textures[spritesData["resources"]["source"]].height;
    let resourcesSize = spritesData["resources"]["resources_size"]
    
    let panelSpriteData = spritesData["resources"]["resource_panel"];
    let resourcePanelVertexBuffer = new VertexBuffer([
        0., 0., 
        panelSpriteData[0] / spriteSheetWidth, 1. - (panelSpriteData[1] + panelSpriteData[3]) / spriteSheetHeight,
        panelSpriteData[2], 0., 
        (panelSpriteData[0] + panelSpriteData[2]) / spriteSheetWidth, 1. - (panelSpriteData[1] + panelSpriteData[3]) / spriteSheetHeight,
        0., panelSpriteData[3],
        panelSpriteData[0] / spriteSheetWidth, 1. - panelSpriteData[1] / spriteSheetHeight,
        panelSpriteData[2], panelSpriteData[3], 
        (panelSpriteData[0] + panelSpriteData[2]) / spriteSheetWidth, 1. - panelSpriteData[1] / spriteSheetHeight,
    ]);
    let panelXoffset = spritesData["resources"]['resource_panel_offset'][0];
    let panelYoffset = spritesData["resources"]['resource_panel_offset'][1];
    let resourceXoffset = spritesData["resources"]['resources_offset'][0];
    let resourceYoffset = spritesData["resources"]['resources_offset'][1];

    let resourceVertexBuffer = new VertexBuffer([
        0., 0.,
        resourcesSize, 0.,
        0., resourcesSize,
        resourcesSize, resourcesSize
    ]);

    let indexBuffer = new IndexBuffer([0, 1, 3, 0, 3, 2]);

    let texCoords = {}
    for (const [resourceName, spriteData] of Object.entries(spritesData["resources"]["resources_sprites"]))
    {
        texCoords[resourceName] = [
            spriteData[0] / spriteSheetWidth, 
            1. - (spriteData[1] + resourcesSize) / spriteSheetHeight
        ];
    }

    let panelInstancesData = [];
    let resourcesInstancesData = [];

    for (const spotName in resourceSpots)
    {
        const spot = resourceSpots[spotName];
        let nodeData = mapData['cities'][nodeNameToLocalId[spotName]];
        let node = emptyNode;
        node.x = nodeData.x;
        node.y = nodeData.y;

        panelInstancesData.push(node.x * MAP_SCALE_FACTOR + panelXoffset, node.y * MAP_SCALE_FACTOR + panelYoffset);
        resourcesInstancesData.push(node.x * MAP_SCALE_FACTOR + resourceXoffset, node.y * MAP_SCALE_FACTOR + resourceYoffset);
        resourcesInstancesData.push(texCoords[resourceNames[spot.resource]][0], texCoords[resourceNames[spot.resource]][1]);
    }

    resourcePanelsMesh = new InstancedIndicedMesh2d(
        resourcePanelVertexBuffer, new VertexBuffer(panelInstancesData), indexBuffer, shaders['resourcePanels']
    );
    resourcesMeshes = new InstancedIndicedMesh2d(
        resourceVertexBuffer, new VertexBuffer(resourcesInstancesData), indexBuffer, shaders['resources']
    );
    shaders['resourcePanels'].use();
    gl.uniform1i(shaders['resourcePanels'].uniformLoc['u_texture'], 0);
    shaders['resources'].use();
    gl.uniform1i(shaders['resources'].uniformLoc['u_texture'], 0);
    gl.uniform2fv(
        shaders['resources'].uniformLoc['u_texCoordRatio'],
        [1. / spriteSheetWidth, 1. / spriteSheetHeight]);
}

function loadMapGameState()
{
    for (let i = 0; i < allGenerals.length; i++) {
        allGenerals[i].updateLocation();
    }
    prepareKingdomsColorTex();
    citiesRenderableData = new CitiesRenderableData();
    citiesRenderableData.loadCities(nbEasterEggs + 1);
    mapRenderer.loadCitiesData();
    mapRenderer.prepareKingdomsMeshes();
    refreshGeneralMeshes();
    prepareResourcesMeshes();
    prepareEasterEggs();
}

// Probably overkill to call this for one general update but it fixed a bug for now...
function refreshGeneralMeshes()
{
    generalsMeshes = new GeneralsMeshes(2 + mapData["cities"].length);
}