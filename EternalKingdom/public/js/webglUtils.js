// ETERNAL KINGDOM - Classes to encapsulate WebGL boiler plate stuff


class Texture
{
    texture;
    width;
    height;

    constructor(width, height, repeatable)
    {
        this.width  = width;
        this.height = height;
        this.texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        if(repeatable)
        {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        }
        else
        {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        } 
        
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    }

    static loadFromImage(image, repeatable)
    {
        let tex = new Texture(image.width, image.height, repeatable);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
        return tex;
    }

    static loadFromArray(width, height, pixels, repeatable)
    {
        let tex = new Texture(width, height, repeatable);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
        return tex;
    }

    bind()
    {
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
    }

    deleteGlObject()
    {
        gl.deleteTexture(this.texture);
    }
}

class UniformBufferObject
{
    uboBuffer;
    uboVariableInfo;

    constructor(programList, uniformBlockName, uboVariableNames, bufferBaseIndex, uniformBlockBinding)
    {
        const blockIndex = gl.getUniformBlockIndex(programList[0].program, uniformBlockName);
        const blockSize = gl.getActiveUniformBlockParameter(
            programList[0].program, blockIndex, gl.UNIFORM_BLOCK_DATA_SIZE
        );

        this.uboBuffer = gl.createBuffer();
        gl.bindBuffer(gl.UNIFORM_BUFFER, this.uboBuffer);
        gl.bufferData(gl.UNIFORM_BUFFER, blockSize, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.UNIFORM_BUFFER, null);

        gl.bindBufferBase(gl.UNIFORM_BUFFER, bufferBaseIndex, this.uboBuffer);
        const uboVariableIndices = gl.getUniformIndices(programList[0].program, uboVariableNames);
        const uboVariableOffsets = gl.getActiveUniforms(programList[0].program, uboVariableIndices, gl.UNIFORM_OFFSET);
        this.uboVariableInfo = {};

        uboVariableNames.forEach((name, index) => {
            this.uboVariableInfo[name] = {
              index: uboVariableIndices[index],
              offset: uboVariableOffsets[index],
            };
        });

        for( const program of programList)
        {
            let index = gl.getUniformBlockIndex(program.program, uniformBlockName);
            gl.uniformBlockBinding(program.program, index, uniformBlockBinding);
        }
    }

    sendSingleVariableData(variableName, data)
    {
        gl.bindBuffer(gl.UNIFORM_BUFFER, this.uboBuffer);
        gl.bufferSubData(
            gl.UNIFORM_BUFFER,
            this.uboVariableInfo[variableName].offset,
            new Float32Array(data)
        );
        gl.bindBuffer(gl.UNIFORM_BUFFER, null);
    }

    sendData(data)
    {
        gl.bindBuffer(gl.UNIFORM_BUFFER, this.uboBuffer);
        gl.bufferSubData(
            gl.UNIFORM_BUFFER,
            0,
            new Float32Array(data)
        );
        gl.bindBuffer(gl.UNIFORM_BUFFER, null);
    }
}


class ShaderProgram
{
    name;
    program;
    attributes;
    perInstanceAttributes;
    uniformLoc;
    nbFloatPerVertex;
    nbFloatPerInstance;

    constructor(name, vsSource, fsSource, attributes={}, uniformNames=[], perInstanceAttributes={})
    {
        this.name = name;
        const vs = createShader(gl.VERTEX_SHADER, vsSource);
        if(vs == 0)
        {
            console.log("Failed to compile vexter shader " + name);
        }
        const fs = createShader(gl.FRAGMENT_SHADER, fsSource);
        if(fs == 0)
        {
            console.log("Failed to compile fragment shader " + name);
        }
        this.program = createProgram(vs, fs);
        if(this.program == 0)
        {
            console.log("Failed to compile shader program " + name);
        }
        gl.deleteShader(vs);
        gl.deleteShader(fs);
        this.attributes = {};
        this.nbFloatPerVertex = 0;
        for (const [name, size] of Object.entries(attributes)) {
            this.attributes[name] = {};
            this.attributes[name]['size'] = size;
            this.attributes[name]['location'] = gl.getAttribLocation(this.program, name);
            this.nbFloatPerVertex += size;
        }
        this.perInstanceAttributes = {};
        this.nbFloatPerInstance = 0;
        for (const [name, size] of Object.entries(perInstanceAttributes)) {
            this.perInstanceAttributes[name] = {};
            this.perInstanceAttributes[name]['size'] = size;
            this.perInstanceAttributes[name]['location'] = gl.getAttribLocation(this.program, name);
            this.nbFloatPerInstance += size;
        }
        this.uniformLoc = {};
        for (let uniform of uniformNames) {
            this.uniformLoc[uniform] = gl.getUniformLocation(this.program, uniform);
        }
    }

    use()
    {
        gl.useProgram(this.program);
    }

    bindAttributes()
    {
        let offset = 0;
        let stride = this.nbFloatPerVertex * Float32Array.BYTES_PER_ELEMENT;
        for (const [name, attribData] of Object.entries(this.attributes)) 
        {
            let location = attribData['location'];
            let size = attribData['size'];
            gl.enableVertexAttribArray(location);
            gl.vertexAttribPointer(
                location, size, gl.FLOAT, false, stride, offset
            );
            gl.vertexAttribDivisor(location, 0);
            offset += size * Float32Array.BYTES_PER_ELEMENT;
        }
    }

    bindInstanceAttributes()
    {
        let offset = 0;
        let stride = this.nbFloatPerInstance * Float32Array.BYTES_PER_ELEMENT;
        for (const [name, attribData] of Object.entries(this.perInstanceAttributes)) 
        {
            let location = attribData['location'];
            let size = attribData['size'];
            gl.enableVertexAttribArray(location);
            gl.vertexAttribPointer(
                location, size, gl.FLOAT, false, stride, offset
            );
            gl.vertexAttribDivisor(location, 1);
            offset += size * Float32Array.BYTES_PER_ELEMENT;
        }
    }

    unbindAttributes()
    {
        for (const [name, attribData] of Object.entries(this.attributes)) 
        {
            gl.disableVertexAttribArray(attribData['location']);
        }
        for (const [name, attribData] of Object.entries(this.perInstanceAttributes)) 
        {
            gl.disableVertexAttribArray(attribData['location']);
        }
    }

    deleteGlObject()
    {
        gl.deleteProgram(this.program);
    }
}

function createShader(type, source)
{
    let shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    let success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (success) 
    {
        return shader;
    }
    
    console.log(gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
    return 0;
}

function createProgram(vertexShader, fragmentShader)
{
    let program = gl.createProgram();

    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    let success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (success) 
    {
        return program;
    }   
    console.log(gl.getProgramInfoLog(program));
    gl.deleteProgram(program);
    return 0;
}


class Framebuffer 
{
    fb;
    texture;
    depthTexture;
    width;
    height;

    constructor(width, height, hasDepth)
    {
        this.width = width;
        this.height = height;

        this.texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

        this.fb = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.texture, 0);

        if(hasDepth)
        {
            this.depthTexture = gl.createTexture();
            gl.bindTexture(gl.TEXTURE_2D, this.depthTexture);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT24, width, height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_INT, null);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, this.depthTexture, 0);
        } 
    }

    bindAndSetViewport()
    {
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
        gl.viewport(0, 0, this.width, this.height);
    }

    bindWithCopiedDepthAndSetViewport(sourceFramebuffer)
    {
        gl.bindFramebuffer(gl.READ_FRAMEBUFFER, sourceFramebuffer.fb);
        gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, this.fb);
        gl.blitFramebuffer(
            0, 0, sourceFramebuffer.width, sourceFramebuffer.height, 
            0, 0, this.width, this.height, 
            gl.DEPTH_BUFFER_BIT, gl.NEAREST
        );
        gl.viewport(0, 0, this.width, this.height);
    }

    bindAndReadPixels()
    {
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
        const pixels = new Uint8Array(this.width * this.height * 4);
        gl.readPixels(0, 0, this.width, this.height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
        return pixels;
    }

    bindAndReadSinglePixel(x, y)
    {
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
        const pixel = new Uint8Array(4);
        gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixel);
        return pixel;
    }

    deleteGlObjects()
    {
        gl.deleteTexture(this.texture);
        if(hasDepth) gl.deleteTexture(this.depthTexture);
        gl.deleteFramebuffer(this.fb);
    }   
}


class VertexBuffer
{
    vbo;
    nbFloat;

    constructor(vertices, usage=gl.STATIC_DRAW)
    {
        this.nbFloat = vertices.length;
        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), usage);
    }

    deleteGlObject()
    {
        gl.deleteBuffer(this.vbo);
    }
}

class IndexBuffer
{
    vbo;
    nbIndices;

    constructor(indices)
    {
        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
        this.nbIndices = indices.length;
    }

    deleteGlObject()
    {
        gl.deleteBuffer(this.vbo);
    }
}


class Mesh2d
{
    vertexBuffer;
    vertexArray;
    nbVertices;
    shader;

    constructor(vertexBuffer, shader)
    {
        this.shader = shader;
        this.vertexBuffer = vertexBuffer;
        this.nbVertices = this.vertexBuffer.nbFloat / this.shader.nbFloatPerVertex;

        this.vertexArray = gl.createVertexArray();
        gl.bindVertexArray(this.vertexArray);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer.vbo);
        this.shader.bindAttributes();
        gl.bindVertexArray(null);
    }

    draw()
    {  
        gl.bindVertexArray(this.vertexArray);
        gl.drawArrays(gl.TRIANGLES, 0, this.nbVertices);
    }

    deleteGlObject()
    {
        gl.deleteVertexArray(this.vertexArray);
    }
}

class IndicedMesh2d
{
    vertexArray;
    vertexBuffer;
    indexBuffer;
    shader;

    constructor(vertexBuffer, indexBuffer, shader)
    {
        this.shader = shader;
        this.vertexBuffer = vertexBuffer;
        this.indexBuffer = indexBuffer
        
        this.vertexArray = gl.createVertexArray();
        gl.bindVertexArray(this.vertexArray);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer.vbo);
        this.shader.bindAttributes();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer.vbo);
        gl.bindVertexArray(null);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
    }

    draw()
    {
        gl.bindVertexArray(this.vertexArray);
        gl.drawElements(gl.TRIANGLES, this.indexBuffer.nbIndices, gl.UNSIGNED_SHORT, 0);
    }

    deleteGlObject()
    {
        gl.deleteVertexArray(this.vertexArray);
    }
}

class InstancedMesh2d
{
    vertexArray;
    vertexBuffer;
    instancesVertexBuffer;
    nbVertices;
    nbInstances;
    shader;

    constructor(vertexBuffer, instancesVertexBuffer, shader)
    {
        this.shader = shader;
        this.vertexBuffer = vertexBuffer;
        this.nbVertices = this.vertexBuffer.nbFloat / this.shader.nbFloatPerVertex;
        this.instancesVertexBuffer = instancesVertexBuffer;
        this.nbInstances = this.instancesVertexBuffer.nbFloat / this.shader.nbFloatPerInstance;

        this.vertexArray = gl.createVertexArray();
        gl.bindVertexArray(this.vertexArray);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer.vbo);
        this.shader.bindAttributes();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.instancesVertexBuffer.vbo);
        this.shader.bindInstanceAttributes();
        gl.bindVertexArray(null);
    }

    draw()
    {
        gl.bindVertexArray(this.vertexArray);
        gl.drawArraysInstanced(gl.TRIANGLES, 0, this.nbVertices, this.nbInstances);
    }

    deleteGlObject()
    {
        gl.deleteVertexArray(this.vertexArray);
    }
}

class InstancedIndicedMesh2d
{
    vertexArray;
    vertexBuffer;
    instancesVertexBuffer;
    indexBuffer;
    nbInstances;
    shader;

    constructor(vertexBuffer, instancesVertexBuffer, indexBuffer, shader)
    {
        this.shader = shader;
        this.vertexBuffer = vertexBuffer;
        this.instancesVertexBuffer = instancesVertexBuffer;
        this.indexBuffer = indexBuffer;
        this.nbInstances = this.instancesVertexBuffer.nbFloat / this.shader.nbFloatPerInstance;

        this.vertexArray = gl.createVertexArray();
        gl.bindVertexArray(this.vertexArray);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer.vbo);
        this.shader.bindAttributes();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.instancesVertexBuffer.vbo);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer.vbo);
        
        this.shader.bindInstanceAttributes();
        gl.bindVertexArray(null);
    }

    draw()
    {
        gl.bindVertexArray(this.vertexArray);
        gl.drawElementsInstanced(gl.TRIANGLES, this.indexBuffer.nbIndices, gl.UNSIGNED_SHORT, 0, this.nbInstances);
    }

    deleteGlObject()
    {
        gl.deleteVertexArray(this.vertexArray);
    }
}

