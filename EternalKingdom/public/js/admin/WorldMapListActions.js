function resetNodeName(nodeId, nodeName)
{
    let newName = confirm("Comment voulez-vous réinitialliser le nom de ce noeud?");

    if (nodeName !== "")
    {
        let payload = {
            nodeId : nodeId,
            newName: nodeName
        }
        ServerPost('/admin/worldNodes/rename', payload, handleRenameResponse);
    }
}

function handleRenameResponse(data)
{
    if (data['success'])
    {
        location.reload();
    }
}