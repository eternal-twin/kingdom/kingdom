let graphicsPanel;
let myGeneralsHue;
let vassalsGeneralsHue;
let lordGeneralsHue;
let otherGeneralsHue;
    
let myGeneralsHueOutput;
let vassalsGeneralsHueOutput;
let lordGeneralsHueOutput;
let otherGeneralsHueOutput;

let graphicsSettings = {};

function readSettingsFromPage()
{
    graphicsSettings.animate = document.getElementById ('animate').checked;
    graphicsSettings.fpsLimit = document.getElementById ('fpsLimit').value;
    
    if (myLord !== null)
    {
        graphicsSettings.alwaysViewKingdom = document.getElementById ('viewKingdom').checked;
        graphicsSettings.myGeneralsHue = myGeneralsHue.value;
        graphicsSettings.vassalsGeneralsHue = vassalsGeneralsHue.value;
        graphicsSettings.lordGeneralsHue = lordGeneralsHue.value;
        graphicsSettings.otherGeneralsHue = otherGeneralsHue.value;
    }
    else
    {
        graphicsSettings.otherGeneralsHue = 0;
    }
}

function readSettingsFromLocalStorage()
{
    if (debug)
    {
        console.log(localStorage);
    }

    if (localStorage.animate)
    {
        graphicsSettings.animate = (localStorage.animate === 'true');
        document.getElementById ('animate').checked = graphicsSettings.animate;
    }
    else
    {
        graphicsSettings.animate = document.getElementById ('animate').checked;
    }

    if (localStorage.fpsLimit)
    {
        graphicsSettings.fpsLimit = localStorage.fpsLimit;
        document.getElementById ('fpsLimit').value = localStorage.fpsLimit;
    }
    else
    {
        graphicsSettings.fpsLimit = document.getElementById ('fpsLimit').value;
    }

    if (myLord !== null)
    {
        if (localStorage.alwaysViewKingdom)
        {
            graphicsSettings.alwaysViewKingdom = (localStorage.alwaysViewKingdom === 'true');
            document.getElementById ('viewKingdom').checked = graphicsSettings.alwaysViewKingdom;
        }
        else
        {
            graphicsSettings.alwaysViewKingdom = document.getElementById ('viewKingdom').checked;
        }
    
        if (localStorage.myGeneralsHue)
        {
            graphicsSettings.myGeneralsHue = localStorage.myGeneralsHue;
            myGeneralsHue.value = localStorage.myGeneralsHue;
        }
        else
        {
            graphicsSettings.myGeneralsHue = myGeneralsHue.value;
        }

        if (localStorage.vassalsGeneralsHue)
        {
            graphicsSettings.vassalsGeneralsHue = localStorage.vassalsGeneralsHue;
            vassalsGeneralsHue.value = localStorage.vassalsGeneralsHue;
        }
        else
        {
            graphicsSettings.lordGeneralsHue = vassalsGeneralsHue.value;
        }

        if (localStorage.lordGeneralsHue)
        {
            graphicsSettings.lordGeneralsHue = localStorage.lordGeneralsHue;
            lordGeneralsHue.value = localStorage.lordGeneralsHue;
        }
        else
        {
            graphicsSettings.lordGeneralsHue = lordGeneralsHue.value;
        }

        if (localStorage.otherGeneralsHue)
        {
            graphicsSettings.otherGeneralsHue = localStorage.otherGeneralsHue;
            otherGeneralsHue.value = localStorage.otherGeneralsHue;
        } 
        else
        {
            graphicsSettings.otherGeneralsHue = otherGeneralsHue.value;
        }

        fillAllHueOutputs();
    }
    else
    {
        graphicsSettings.otherGeneralsHue = 0;
    }
}

function writeSettingsInLocalStorage()
{
    Object.keys(graphicsSettings).forEach(function (k) {
        localStorage.setItem(k, graphicsSettings[k]);
    });
}


function initGraphicsSettings()
{
    graphicsPanel = document.getElementById ('map_settings');
    showDOM(graphicsPanel, false);

    if (myLord !== null)
    {
        myGeneralsHue = document.getElementById ('myGeneralsHue');
        vassalsGeneralsHue = document.getElementById ('vassalsGeneralsHue');
        lordGeneralsHue = document.getElementById ('lordGeneralsHue');
        otherGeneralsHue = document.getElementById ('otherGeneralsHue');
        
        myGeneralsHueOutput = document.getElementById ('myGeneralsHueOutput');
        vassalsGeneralsHueOutput = document.getElementById ('vassalsGeneralsHueOutput');
        lordGeneralsHueOutput = document.getElementById ('lordGeneralsHueOutput');
        otherGeneralsHueOutput = document.getElementById ('otherGeneralsHueOutput');
        myGeneralsHue.addEventListener('input', function() {
            fillHueOutput(this, myGeneralsHueOutput)
        });
        vassalsGeneralsHue.addEventListener('input', function() {
            fillHueOutput(this, vassalsGeneralsHueOutput)
        });
        lordGeneralsHue.addEventListener('input', function() {
            fillHueOutput(this, lordGeneralsHueOutput)
        });
        otherGeneralsHue.addEventListener('input', function() {
            fillHueOutput(this, otherGeneralsHueOutput)
        });
        fillAllHueOutputs();
    }
    readSettingsFromLocalStorage();
}

function fillHueOutput(inputRange, outputLabel)
{
    //outputLabel.value = inputRange.value;
    let hsl = "hsl(" + inputRange.value + "deg, 100%,50%)";
    outputLabel.style.backgroundColor = hsl;
}

function fillAllHueOutputs()
{
    fillHueOutput(myGeneralsHue, myGeneralsHueOutput);
    fillHueOutput(vassalsGeneralsHue, vassalsGeneralsHueOutput);
    fillHueOutput(lordGeneralsHue, lordGeneralsHueOutput);
    fillHueOutput(otherGeneralsHue, otherGeneralsHueOutput);
}

function onClickSettings()
{
    showDOM(graphicsPanel, true);
}

function closeSettings()
{
    showDOM(graphicsPanel, false);
    readSettingsFromLocalStorage();
}

function applySettings()
{
    let animateOld = graphicsSettings.animate;
    readSettingsFromPage();
    writeSettingsInLocalStorage();
    generalsMeshes.updateInstances();
    showDOM(graphicsPanel, false);

    if(!animateOld && graphicsSettings.animate)
    {
        mapRenderer.update(0);
    }
    if(!graphicsSettings.animate)
    {
        if(animateOld)
        {
            mapRenderer.stopAnimation();
        }
        mapRenderer.renderMap();
    }
}

function resetSettings()
{
    document.getElementById ('animate').checked = true
    document.getElementById ('fpsLimit').value = 60;

    if (myLord !== null)
    {
        document.getElementById ('viewKingdom').checked = true
        myGeneralsHue.value = 240;
        vassalsGeneralsHue.value = 120;
        lordGeneralsHue.value = 45;
        otherGeneralsHue.value = 0;
        fillAllHueOutputs();
    }
}