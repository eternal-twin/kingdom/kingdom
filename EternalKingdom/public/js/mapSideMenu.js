// ETERNAL KINGDOM - MAP SIDEMENU

// TODO Put all enum in GameDefinition.js ?
const UnitType = {
    SOLDIER       : 0,
    PIKEMAN       : 1,
    ARCHER        : 2,
    HORSEMAN      : 3,
    KNIGHT        : 4,
    MOUNTED_ARCHER: 5,
    PALADIN       : 6,
    CATAPULT      : 7,
    BALLISTA      : 8,
    UnitCount     : 9,
}

const ActionType = {
    SEND_MESSAGE  : 0,
    VIEW_BATTLE   : 1,
    MOVE_START    : 2,
    MOVE_CONFIRM  : 3,
    MOVE_CANCEL   : 4,
    MOVE_TURN_BACK: 5,
    TRANSFER_UNITS: 6,
    FORTIFY_START : 7,
    FORTIFY_STOP  : 8,
    ATTACK        : 9,
    HARVEST       : 10,
    COUNT         : 11,
}

//#region Variables

// Sidebar cached references
let sideMenu = null;
let sideMenuTitle = null;
let sideMenuInfo = null;

let sideMenuUnitsHeader1 = null;
let sideMenuUnitsList1 = null;
let sideMenuUnitsHeader2 = null;
let sideMenuUnitsList2 = null;

// Actions cached references
let actionTitle = null;
let actions = null;

let isSideMenuOpened = false;

// Count of currently displayed actions (0 = hide the section)
let actionsDisplayed = 0;

//#endregion

//#region SideMenu WorldMap

function initSideMenu()
{
    sideMenu = document.getElementById("map_sidebar");
    sideMenuTitle = sideMenu.querySelector("#SideMenuTitle");
    sideMenuInfo = sideMenu.querySelector(".infos")
    actionTitle = sideMenu.querySelector(".h3b");
    actions = sideMenu.querySelector(".actions");

    sideMenuUnitsHeader1 = sideMenu.querySelector("#army1");
    sideMenuUnitsList1 = sideMenu.querySelector('#armyUnits1');
    sideMenuUnitsHeader2 = sideMenu.querySelector("#army2");
    sideMenuUnitsList2 = sideMenu.querySelector('#armyUnits2');
}

function openWorldSideMenu()
{
    if (isSideMenuOpened) {
        return;
    }

    if (selectedEntity.type !== EntityType.INVALID) {
        sideMenu.style.width = "285px";
        sideMenu.style.left = "560px";
        isSideMenuOpened = true;
    }
}

function closeWorldSideMenu()
{
    if (!isSideMenuOpened) {
        return;
    }

    sideMenu.style.width = "0";
    sideMenu.style.left = "845px";

    isSideMenuOpened = false;
}

function updateSideMenuContent()
{
    updateContent(sideMenuInfo);
    updateSideMenuTitle();
    updateSideMenuUnits();
    updateSideMenuActions();
}

function updateSideMenuTitle()
{
    let titleLink = null;
    switch(selectedEntity.type) {
        case EntityType.GENERAL:
            titleLink = getGeneralShortcutURL(world.id, selectedGeneral.dbID);
            break;
        case EntityType.NODE:
            titleLink = getNodeShortcutURL(world.id, selectedNode.dbID);
            break;
        default: break;
    }

    sideMenuTitle.innerHTML = selectedEntity.name;
    sideMenuTitle.href = titleLink;
}

/**
 * Toggle visibility of all actions thanks to the current context!
 */
function updateSideMenuActions()
{
    actionsDisplayed = 0;
    for (let i = 0; i < ActionType.COUNT; i++) {
        showDOM(actions.children[i]);
    }

    if(isSelectingDestination || selectedEntity.type === EntityType.GENERAL)
    {
        updateGeneralActions();
    }
    else if(selectedEntity.type === EntityType.NODE)
    {
        updateNodeActions();
    }
    showDOM(actionTitle, actionsDisplayed > 0);
}

function updateGeneralActions()
{
    const isOwnedGeneral = selectedGeneral.owner.isEquals(myLord);
    if(isOwnedGeneral)
    {
        if (isSelectingDestination)
        {
            showAction(ActionType.MOVE_CONFIRM);
            showAction(ActionType.MOVE_CANCEL);
        }
        else
        {
            const isMoving = selectedGeneral.isMoving();

            if (!isMoving) {
                const nearbyNode = nodesDict[selectedGeneral.locationName];
                const isFortifying = selectedGeneral.isFortifying;
                const isAttacking = selectedGeneral.isAttacking;

                if (nearbyNode !== undefined) {
                    let canAttack = true;
                    if (nearbyNode.isOccupied())
                    {
                        const nearbyNodeOwner = nearbyNode.sovereign ? nearbyNode.sovereign : nearbyNode.owner ? nearbyNode.owner : null;
                        const isOwningNearbyNode = nearbyNodeOwner ? nearbyNodeOwner.isEquals(myLord) : false;
                        if (isOwningNearbyNode && !nearbyNode.isInWar())
                        {
                            showAction(ActionType.TRANSFER_UNITS);
                            updateActionLink(ActionType.TRANSFER_UNITS, getGeneralActionURL(selectedGeneral.dbID, "units"));

                            if (!isFortifying) {
                                showAction(ActionType.FORTIFY_START);
                                updateActionLink(ActionType.FORTIFY_START, getGeneralActionURL(selectedGeneral.dbID, "fortify"));
                            } else {
                                showAction(ActionType.FORTIFY_STOP);
                                updateActionLink(ActionType.FORTIFY_STOP, getGeneralActionURL(selectedGeneral.dbID, "fortify"));
                            }
                        }

                        const isNearbyNodeResourceSpot = resourceSpots[selectedGeneral.locationName] !== undefined;
                        const hasHarvestRight = isOwningNearbyNode || (lordsDict[nearbyNodeOwner.dbID].relation ?
                            lordsDict[nearbyNodeOwner.dbID].relation.canHarvest : false);
                        let canHarvest = isNearbyNodeResourceSpot && hasHarvestRight;
                        // TODO /* && nearbyResourceSpot.harvestableAmount > 0 */
                        if (canHarvest) {
                            showAction(ActionType.HARVEST);
                        }

                        canAttack = !nearbyNode.isInWar() && !isOwningNearbyNode;
                    }

                    if (canAttack)
                    {
                        showAction(ActionType.ATTACK);
                    } else if (nearbyNode.isInWar()) {
                        updateActionLink(ActionType.VIEW_BATTLE, getBattleActionURL(nearbyNode.battleDbId));
                        showAction(ActionType.VIEW_BATTLE);
                    }
                }

                if (!isFortifying && !isMoving && !isAttacking) {
                    showAction(ActionType.MOVE_START);
                }
            }
            else
            {
                showAction(ActionType.MOVE_TURN_BACK);
            }
        }
    }
}

function updateNodeActions()
{
    if (selectedNode !== null){
        if (selectedNode.hasOwner() && !selectedNode.owner.isEquals(myLord)) {
            showAction(ActionType.SEND_MESSAGE);
        }
        if (selectedNode.isInWar()) {
            updateActionLink(ActionType.VIEW_BATTLE, getBattleActionURL(selectedNode.battleDbId));
            showAction(ActionType.VIEW_BATTLE);
        }
    }
}

function showAction(actionType)
{
    showDOM(actions.children[actionType], true);
    actionsDisplayed++;
}

function updateActionLink(actionType, url)
{
    actions.children[actionType].children[0].setAttribute("data-link", url);
}

function updateSideMenuUnits()
{
    let units = [];
    if (hoveredNode != null && hoveredNode.dbID in defenseUnitsCache) {
        units = defenseUnitsCache[hoveredNode.dbID];
    } else if (hoveredGeneral != null && hoveredGeneral.dbID in generalUnitsCache) {
        units = generalUnitsCache[hoveredGeneral.dbID];
    }

    updateDisplayedUnits(sideMenuUnitsHeader1, sideMenuUnitsList1, units);

    units = [];
    if (hoveredNode != null && hoveredNode.dbID in nodeUnitsCache) {
        units = nodeUnitsCache[hoveredNode.dbID];
    }

    updateDisplayedUnits(sideMenuUnitsHeader2, sideMenuUnitsList2, units);
}

/**
 * Update the title above units depending the clicked entity
 * @param unitsHeader
 */
function updateUnitsTitle(unitsHeader)
{
    // TODO BUG : Cache translation in the page and update this appropriately!
    if (selectedEntity.type === EntityType.NODE) {
        if (mapData["cities"][selectedEntity.id]['cap']) {
            unitsHeader.innerHTML = "Defense";
        }
    } else if (selectedEntity.type === EntityType.GENERAL) {
        unitsHeader.innerHTML = "Units";
    }
}

/**
 * Update visibility of each Unit cell depending their amount
 * @param unitsList
 * @param units
 */
function updateUnitCells(unitsList, units)
{
    let unitViews = unitsList.querySelectorAll(".number");
    for (let i = 0; i < unitViews.length; i++) {
        let unitAmount = units[i];
        if (unitAmount <= 0) {
            showDOM(unitViews[i].parentNode.parentNode, false);
        } else {
            showDOM(unitViews[i].parentNode.parentNode, true);
            unitViews[i].innerHTML = unitAmount;
        }
    }
}

function hideAllUnitCells(unitsList)
{
    updateUnitCells(unitsList, Array(UnitType.UnitCount).fill(0));
}

//#endregion

