// ETERNALKINGDOM - WORLDMAP

// Please, documentate properly all the new functions!

//#region Type declaration

// Enumeration to know what is selected
const EntityType = {
    NODE   : 0,
    GENERAL: 1,
    SECRET:  2,
    INVALID: 3,
}

const InvalidDBId = -1;

class ServerEntity
{
    dbID = InvalidDBId;

    constructor(dbID) {
        this.dbID = dbID;
    }

    isValid()
    {
        return this.dbID !== InvalidDBId;
    }
}

class Lord extends ServerEntity
{
    userId;
    //localId is used client side for hovering/selecting in the map
    localId;
    ownerName;
    ownerTitle;
    relation;
    
    constructor(lordId, userId, localId, ownerName, ownerTitle)
    {
        super(lordId);

        this.userId = userId;
        this.localId = localId;
        this.ownerName = ownerName;
        this.ownerTitle = ownerTitle;
        this.relation = null;
    }

    isEquals(other)
    {
        return other != null && this.dbID === other.dbID;
    }

    toString()
    {
        return this.ownerTitle + " " + this.ownerName;
    }
}

class Node extends ServerEntity
{
    x;
    y;
    name;
    owner = null;
    sovereign = null;
    battleDbId = InvalidDBId;

    generals = [];

    trade = 0;

    constructor(dbID, x, y, name)
    {
        super(dbID);

        this.x = x;
        this.y = y;
        this.name = name;
    }

    getMostRelevantOwningLordId()
    {
        if (this.hasSovereign()) {
            return this.sovereign.dbID;
        } else if (this.hasOwner()) {
            return this.owner.dbID;
        }

        return InvalidDBId;
    }

    isOccupied()
    {
        return this.hasOwner() || this.hasSovereign();
    }

    hasOwner()
    {
        return this.owner !== null && this.owner.isValid();
    }

    hasSovereign()
    {
        return this.sovereign !== null && this.isValid();
    }

    isInWar()
    {
        return this.battleDbId !== InvalidDBId;
    }
}

class CityInfo
{
    towerLevel = -1;
    nbHouses = -1;

    constructor(towerLevel, nbHouses)
    {
        this.towerLevel = towerLevel;
        this.nbHouses = nbHouses;
    }

    isValid()
    {
        return this.towerLevel >= 0 && this.nbHouses >= 0;
    }
}

class ResourceSpotInfo extends ServerEntity
{
    resource;
    amount;

    constructor(dbID, resourceName, amount)
    {
        super(dbID);

        this.resource = resourceToId[resourceName];
        this.amount = amount;
    }
}

class Relation
{
    diplomacyType;
    canCross;
    canHarvest;

    constructor(diplomacyType, canCross, canHarvest)
    {
        this.diplomacyType = diplomacyType;
        this.canCross = canCross;
        this.canHarvest = canHarvest;
    }
}

//#endregion

//#region MapData
let world = null;
let lordsDict = {};
// used to render color per lord in Strategic mode.
let lordsIds = [];
// TODO This should be loaded from mapGameStates first then completed with claimed node to avoid to query all nodes data
// AND unclaimed territories
let nodesDict = {};
let playerCapitals = {};
let resourceSpots = {};
let allGenerals = [];

let myLord = null;
let myCity = null;
let myGeneralsLocalID = [];

let emptyNode = new Node(InvalidDBId, 0, 0, "Empty");
//#endregion

//#region Const

//ratio between generated map size and map size in pixels
const MAP_SCALE_FACTOR = 8;

const debug = true;

//paths to the images used to render the map
const imagesSrc = {
    'ground'    : '/img/map/ground_layer.png',
    'landscape' : '/img/map/landscape_sprites.png',
    'lake'      : '/img/map/lake.png',
    'background': '/img/map/map_background.jpg',
    'generals'  : '/img/map/generals.png'
}

const resourceNames = ['linen', 'iron', 'horse'];

// Useful to convert a resource name as display ID
const resourceToId = {
    'lin' : 0,
    'iron' : 1,
    'horse' : 2,
};

// list of colors for kingdoms in strategic view
// https://en.wikipedia.org/wiki/Help:Distinguishable_colors
const kingdomColors = [
    [240,163,255],[0,117,220],[153,63,0],[76,0,92],[25,25,25],[0,92,49],[43,206,72],
    [255,204,153],[128,128,128],[148,255,181],[143,124,0],[157,204,0],[194,0,136],
    [0,51,128],[255,164,5],[116,10,255],[255,168,187],[66,102,0],[255,0,16],[94,241,242],[0,153,143],
    [224,255,102],[153,0,0],[255,255,128],[255,225,0],[255,80,5] 
];

// possible positions for houses around a capital
const housesPositions =
    [[-20, -19], [-14, -19], [-8, -19], [-2, -19], [4, -19], [10, -19], [16, -19], [-23, -16], [-17, -16], [-11, -16], [-5, -16], [1, -16], [7, -16], [13, -16], [19, -16], [25, -16], [-32, -13], [-26, -13], [-20, -13], [-14, -13], [-8, -13], [-2, -13], [4, -13], [10, -13], [16, -13], [22, -13], [28, -13], [-35, -10], [-29, -10], [-23, -10], [-17, -10], [-11, -10], [-5, -10], [1, -10], [7, -10], [13, -10], [19, -10], [25, -10], [31, -10], [-38, -7], [-32, -7], [-26, -7], [-20, -7], [-14, -7], [10, -7], [16, -7], [22, -7], [28, -7], [34, -7], [-35, -4], [-29, -4], [-23, -4], [-17, -4], [19, -4], [25, -4], [31, -4], [37, -4], [-38, -1], [-32, -1], [-26, -1], [-20, -1], [22, -1], [28, -1], [34, -1], [40, -1], [-35, 2], [-29, 2], [-23, 2], [19, 2], [25, 2], [31, 2], [37, 2], [-38, 5], [-32, 5], [-26, 5], [-20, 5], [16, 5], [22, 5], [28, 5], [34, 5], [-35, 8], [-29, 8], [-23, 8], [-17, 8], [-11, 8], [-5, 8], [1, 8], [7, 8], [13, 8], [19, 8], [25, 8], [31, 8], [37, 8], [-32, 11], [-26, 11], [-20, 11], [-14, 11], [-8, 11], [-2, 11], [4, 11], [10, 11], [16, 11], [22, 11], [28, 11], [34, 11], [-29, 14], [-23, 14], [-17, 14], [-11, 14], [-5, 14], [1, 14], [7, 14], [13, 14], [19, 14], [25, 14], [31, 14], [-26, 17], [-20, 17], [-14, 17], [-8, 17], [-2, 17], [4, 17], [10, 17], [16, 17], [22, 17], [-5, 20], [1, 20], [7, 20]];

const nbEasterEggs = 1;

    //#endregion

//#region Variables

// htlm element that contains the map
let view;
let viewHalfWidth, viewHalfHeight;
//the WebGL context
let gl;
// the tooltip with informations about a city or a general on mouve hover
let tooltip;
// we don't want to draw the tooltip when the mouse is out of the map frame, or when dragging
let hideTooltip = false;

// we need to keep trace of previous mouse (or touch) position when dragging
let lastClientX = 0;
let lastClientY = 0;

// we need to prevent click event after dragging, so we need to difference 'click' vs 'mousedown, dragging and mouseup'
let hasDragged = false;

//we need to prevent the whole page scrolling when scrolling the map on tactile devices
let isPageScrollable = true;

let focusedNode = null;
let focusedGeneralDbID = InvalidDBId;
let focusedKingdomLordDbID = InvalidDBId;

//what the mouse is hovering, or is clicking?
let hoveredEntity = {
    id  : -1, //the id of the entity; -1 means no entity is selected
    type: EntityType.INVALID,
    name: 'None',
}
let selectedEntity = {
    id  : -1, //the id of the entity; -1 means no entity is selected
    type: EntityType.INVALID,
    name: 'None',
}
// Not null if we hovered/clicked any Node
let hoveredNode = null;
let selectedNode = null;
// Not null if we hovered/clicked any General
let hoveredGeneral = null;
let selectedGeneral = null;
//the id of the player; -1 means no kingdom is hovered
let hoveredKingdomPlayerId = -1;

//shows the current kingdoms with colors
let isStrategicView = false;

//map from image names to Image objects
let loadedImages = {}

// Shared sprite information across all maps
let spritesData;

// the graph with cities and the paths between them, and definition of zones and voronoi cells
let mapData;

let generalEasterEgg;

//Names in json will not be the same as nodes in DB when emperor will modify then, it may cause issues.
let nodeNameToLocalId = {};


//#endregion

//#region Initialization

// call init when DOM is loaded
if (document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)) {
    init();
} else {
    document.addEventListener("DOMContentLoaded", init);
}

function init()
{
    tooltip = document.getElementById('map_tip');
    kingdomtip = document.getElementById('map_kingdom_tip');
    showDOM(tooltip, false);
    showDOM(kingdomtip, false);

    view = document.getElementById('map_view');
    gl = view.getContext("webgl2", { premultipliedAlpha: false }, { antialias: false });

    initSideMenu();

    let parentRect = view.parentNode.getBoundingClientRect();
    view.width = parentRect.width;
    view.height = parentRect.height;
    // set the display size.
    view.style.width = view.width + "px"; 
    view.style.height = view.height + "px";

    viewHalfWidth = view.width >> 1;
    viewHalfHeight = view.height >> 1;

    loadJson().then(() => 
    {
        readServerData();
        initGraphicsSettings();
        initMap(parentRect.width, parentRect.height);
    }).then(() => 
    {
        initEventListeners();

        for (let i = 0; i < flashMessages.length; i++) {
            showGeneralNotification(false, flashMessages[i]);
        }
    });
}

/*
* Read the json data and store everything in dictionaries for quick access!
*/
function readServerData()
{
    world = mapServerData.world;
    // TODO Should we clear mapServerData to free memory since we store everything in dictionnaries?

    for (let i = 0; i < mapServerData.lords.length; i++)
    {
        let lordData = mapServerData.lords[i];
        lordsDict[lordData.id] = new Lord(lordData.id, lordData.userId, i, lordData.name, lordData.title);
        lordsIds.push(lordData.id);
    }

    for (let i = 0; i < mapServerData.nodes.length; i++)
    {
        let nodeData = mapServerData.nodes[i];
        let nodeKey = nodeData.data.name;
        let nodeName = nodeData.customName != null ? nodeData.customName : nodeData.data.name;
        nodesDict[nodeKey] = new Node(nodeData.id, nodeData.data.x, nodeData.data.y, nodeName);
        if (nodeData.ownerId !== InvalidDBId) {
           nodesDict[nodeKey].owner = lordsDict[nodeData.ownerId];
        }
        if (nodeData.sovereignId !== InvalidDBId) {
            nodesDict[nodeKey].sovereign = lordsDict[nodeData.sovereignId];
        }
    }

    if (typeof myLordId !== 'undefined' && typeof myCityID !== 'undefined') {
        myLord = lordsDict[myLordId];
        myCity = nodesDict[myCityID];

        if (debug)
        {
            console.log("Lord: " + myLord + " / city pos: " + myCity.x + " / " + myCity.y);
        }
    }

    // TODO To optimize the DB queries, we should use the node DB Id and have a dictionary (DB ID, Node name)
    for (let i = 0; i < mapServerData.cities.length; i++)
    {
        let cityData = mapServerData.cities[i];
        playerCapitals[cityData.name] = new CityInfo(cityData.dungeonLevel, cityData.housesCount);
        console.assert(playerCapitals[cityData.name].isValid(), "Corrupted data found in json - fail to load cityInfo");
    }

    for (let i = 0; i < mapServerData.resourceSpots.length; i++)
    {
        let spotData = mapServerData.resourceSpots[i];
        resourceSpots[spotData.locationName] = new ResourceSpotInfo(spotData.id, spotData.resource, spotData.amount);
    }

    for (let i = 0; i < mapServerData.battles.length; i++)
    {
        let battleData = mapServerData.battles[i];
        nodesDict[battleData.locationName].battleDbId = battleData.id;
    }

    for (let i = 0; i < mapServerData.generals.length; i++)
    {
        let generalData = mapServerData.generals[i];
        let owner = lordsDict[generalData.ownerId];
        let general = new General(generalData.id, generalData.name, owner, generalData.locationName, generalData.reputation,
            generalData.isFortifying, generalData.isAttacking
        );
        general.setNodeLocation(general.locationName);

        if (generalData.movementOrder != null)
        {
            general.setMovementOrder(generalData.movementOrder);
        }

        allGenerals.push(general);

        if (myLord != null && owner.isEquals(myLord))
        {
            myGeneralsLocalID.push(allGenerals.length - 1);
        }
    }

    if (lordsDict.length > 0) {
        if (mapServerData.relations != undefined) {
            for (let i = 0; i < mapServerData.relations.length; i++) {
                const relation = mapServerData.relations[i];
                lordsDict[relation.lordId].relation = new Relation(relation.diplomacyType, relation.canCross, relation.canHarvest);
            }
        }
    }
}

async function initMap(viewWidth, viewHeight)
{
    for (let i = 0; i < mapData["cities"].length; i++)
    {
        let city = mapData["cities"][i];
        nodeNameToLocalId[city.name] = i;
    }

    await initMapRendering(mapData['map_width'], mapData['map_height']);
    initMapScroll();
    clearHoveredEntity();
    clearSelectedEntity();
    if(graphicsSettings.animate)
    {
        mapRenderer.update(0);
    }
    else
    {
        mapRenderer.renderMap();
    }
}

async function loadJson()
{
    if (debug)
    {
        console.log("Load the world graph: " + worldName);
    }

    let mapDataUrl = '/maps/' + worldName + '.json';
    let spritesDataUrl = '/maps/spritesData.json';

    const [spritesDataResponse, mapDataResponse] =
        await Promise.all([fetch(spritesDataUrl), fetch(mapDataUrl)]);

    spritesData = await spritesDataResponse.json();
    mapData = await mapDataResponse.json();
}


function initMapScroll()
{
    if(focusedNode)
    {
        centerMapOnNode(focusedNode);
    }
    else if(focusedGeneralDbID !== InvalidDBId)
    {
        const generalIndex = allGenerals.findIndex(e => e.dbID === focusedGeneralDbID);
        centerMapOnGeneral(generalIndex);
    }
    else if(focusedKingdomLordDbID !== InvalidDBId)
    {
        let owner = lordsDict[focusedKingdomLordDbID];
        let ownerNodeName = null;
        for (const [ nodeName, node ] of Object.entries(nodesDict)) {
            if (node.owner != null && node.owner.isEquals(owner)) {
                ownerNodeName = nodeName;
                break;
            }
        }
        if(ownerNodeName != null) {
            centerMapOnNode(ownerNodeName);
        }
    }
    else
    {
        onClickCapital();
    }
}

function initEventListeners()
{
    view.addEventListener('mousemove', onMouseMove);
    view.addEventListener('mousedown', onMouseDragStart);
    view.addEventListener('touchstart', onTouchDragStart);
    view.addEventListener('mouseleave', onMouseLeave);
    view.addEventListener('mouseover', onMouseOver);
    view.addEventListener('click', onClickMap);

    let preventScrolling = function (e) {
        if (!isPageScrollable) {
            e.preventDefault();
        }
    }
    document.addEventListener('touchmove', preventScrolling, {passive: false});

    initButtonWithCallback('zoom_out','click', onClickZoomOut);
    initButtonWithCallback('zoom_in','click', onClickZoomIn);
    initButtonWithCallback('go_to_capital', 'click', onClickCapital);
    initButtonWithCallback('refresh_map', 'click', onRefreshMap);
    initButtonWithCallback('toggle_strat_mode','click', onToggleStrategicMode);
    initButtonWithCallback('map_settings_icon','click', onClickSettings);
}

function initButtonWithCallback(buttonId, event, callback)
{
    let button = document.getElementById(buttonId);
    if(button != null)
    {
        button.addEventListener(event, callback);
    }
}

//#endregion

//#region Update methods

//#region Selection system


//function setEntity


function clearHoveredEntity()
{
    hoveredEntity.type = EntityType.INVALID;
    hoveredEntity.id = -1;
    hoveredNode = null;
    hoveredGeneral = null;
}

function clearSelectedEntity()
{
    selectedEntity.type = EntityType.INVALID;
    selectedEntity.id = -1;
    selectedNode = null;
    selectedGeneral = null;
}

function hoverEasterEgg(easterEggId)
{
    hoveredEntity.type = EntityType.SECRET;
    hoveredEntity.id = easterEggId;
    hoveredGeneral = generalEasterEgg;
    hoveredNode = null;
    hoveredEntity.name = "Mountain's King";
}

function hoverNode(nodeId)
{
    hoveredEntity.type = EntityType.NODE;
    hoveredEntity.id = nodeId;
    hoveredGeneral = null;
}

function hoverGeneral(generalId)
{
    hoveredEntity.type = EntityType.GENERAL;
    hoveredEntity.id = generalId;
    hoveredNode = null;
}

/**
 * When finger or mouse is hovering the map, read selection offscreen canvas to check if a selectable entity
 * has been hovered.
 *
 * @param {int} x x position of mouse or finger in selection (or view) canvas
 * @param {int} y y position of mouse or finger in selection (or view) canvas
 */
function updateHoveredEntity(x, y)
{
    let selectedColor = mapRenderer.getSelectedColor(x, y);
    let hasChanged = false;
    
    if((selectedColor[0] === 0) && (selectedColor[1] === 0))
    {
        hasChanged = (hoveredEntity.type !== EntityType.INVALID)
        clearHoveredEntity();
    }
    else
    {
        const redGreenId = selectedColor[0] * 256 + selectedColor[1];
        if(redGreenId < nbEasterEggs + 1)
        {
            hasChanged = (hoveredEntity.type !== EntityType.SECRET) || (hoveredEntity.id !== redGreenId - 1);
            hoverEasterEgg(redGreenId - 1);
        }
        else if(redGreenId < nbEasterEggs + mapData["cities"].length + 1)
        {
            const newId = redGreenId - nbEasterEggs - 1;
            hasChanged = (hoveredEntity.type !== EntityType.NODE) || (hoveredEntity.id !== newId);
            hoverNode(redGreenId - nbEasterEggs - 1);
            if(hasChanged)
            {
                fetchHoveredNodeInfo();
            }
        }
        else
        {
            const newId = redGreenId - mapData["cities"].length - nbEasterEggs - 1;
            hasChanged = (hoveredEntity.type !== EntityType.GENERAL) || (hoveredEntity.id !== newId);
            hoverGeneral(redGreenId - mapData["cities"].length - nbEasterEggs - 1);
            if(hasChanged)
            {
                fetchHoveredGeneralInfo();
            }
        } 
    }

    if((selectedColor[2] !== 0)&&(selectedColor[3] !== 255))
    {
        hoveredKingdomPlayerId = lordsIds[selectedColor[2] - 1];
    }
    else
    {
        hoveredKingdomPlayerId = -1;
    }
    
    if(hasChanged && isSelectingDestination)
    {
        if(!graphicsSettings.animate)
        {
            mapRenderer.renderMap();
        }
    }
}

function selectHoveredEntity()
{
    selectedEntity = hoveredEntity;
    selectedGeneral = hoveredGeneral;
    selectedNode = hoveredNode;
}

function fetchHoveredNodeInfo()
{
    hoveredEntity.name = mapData['cities'][hoveredEntity.id]['name'];

    if (nodesDict[hoveredEntity.name] !== undefined)
    {
        hoveredNode = nodesDict[hoveredEntity.name];
    } else {
        let emptyNodeData = mapData['cities'][hoveredEntity.id];
        emptyNode.x = emptyNodeData.x;
        emptyNode.y = emptyNodeData.y;
        emptyNode.name = hoveredEntity.name;

        hoveredNode = emptyNode;
    }

    serverRequestNodeInfo();
}

function fetchHoveredGeneralInfo()
{
    hoveredGeneral = allGenerals[hoveredEntity.id];
    if(hoveredGeneral !== null) {
        hoveredEntity.name = "General " + hoveredGeneral.name;
    }
}

//#endregion

//#region UI

/* Refresh tooltip visibility and update content if relevant */
function updateTooltip()
{
    if (hideTooltip) {
        return;
    }

    if (hoveredEntity.type !== EntityType.INVALID) {
        updateTooltipContent();

        document.body.style.cursor = "pointer";
        tooltip.style.left = event.pageX + 8 + 'px';
        tooltip.style.top = event.pageY + 10 + 'px';
        showDOM(tooltip, true);
        showDOM(kingdomtip, false);
    } else {
        document.body.style.cursor = "default";
        showDOM(tooltip, false);

        if((hoveredKingdomPlayerId !== -1)&&(isStrategicView))
        {
            kingdomtip.style.left = event.pageX + 8 + 'px';
            kingdomtip.style.top = event.pageY + 10 + 'px';
            kingdomtip.innerHTML = lordsDict[hoveredKingdomPlayerId].ownerName;
            showDOM(kingdomtip, true);
        }
        else
        {
            showDOM(kingdomtip, false);
        }
        
    }
}

/* Update the content of the tooltip with data found in the hovered node. */
function updateTooltipContent()
{
    let title = tooltip.querySelector("h1");
    title.innerHTML = hoveredEntity.name;

    let infos = tooltip.querySelector(".infos");
    updateContent(infos);

    updateUnitsOnTooltip();
}

function updateUnitsOnTooltip()
{
    let units = [];
    if (hoveredNode != null && hoveredNode.dbID in defenseUnitsCache) {
        units = defenseUnitsCache[hoveredNode.dbID];
    } else if (hoveredGeneral != null && hoveredGeneral.dbID in generalUnitsCache) {
        units = generalUnitsCache[hoveredGeneral.dbID];
    }

    let unitsHeader = tooltip.querySelector("#army1");
    let unitsList = tooltip.querySelector("#armyUnits1");
    updateDisplayedUnits(unitsHeader, unitsList, units);

    units = [];
    if (hoveredNode != null && hoveredNode.dbID in nodeUnitsCache) {
        units = nodeUnitsCache[hoveredNode.dbID];
    }

    unitsHeader = tooltip.querySelector("#army2");
    unitsList = tooltip.querySelector("#armyUnits2");
    updateDisplayedUnits(unitsHeader, unitsList, units);
}

/* Refresh the specified UI with the selected entity */
function updateContent(infos)
{
    if(infos == null) {
        return;
    }

    for (let i = 0; i < infos.children.length; i++) {
        showDOM(infos.children[i], false);
    }

    switch (hoveredEntity.type)
    {
        case EntityType.NODE: displayNodeInfo(infos); break;
        case EntityType.GENERAL: displayGeneralInfo(infos); break;
        case EntityType.SECRET: displayEasterEggInfo(infos); break;
        default:
            break;
    }
}

function displayNodeInfo(infos)
{
    if(hoveredNode == null)
        return;

    if (mapData['cities'][hoveredEntity.id]['cap']) {
        if (hoveredNode.hasOwner()) {
            let capitalCity = infos.querySelector("#Capital");

            let cityLink = infos.querySelector("#CityLink");
            if(cityLink != null) {
                cityLink.href = getNodeShortcutURL(world.id, hoveredNode.dbID);
            }
            let lordLink = capitalCity.querySelector("#LordLink");
            if(lordLink !== null) {
                lordLink.innerHTML = hoveredNode.owner.toString();
                lordLink.href = getUserShortcutURL(hoveredNode.owner);
            }

            showDOM(capitalCity, true);
        } else {
            let barbarianCity = infos.querySelector("#BarbarianCity");
            showDOM(barbarianCity, true);
        }
    } else {
        if (!hoveredNode.hasSovereign()) {
            let wild = infos.querySelector("#Wild");
            showDOM(wild, true);
        }
    }

    if (hoveredNode.hasSovereign()) {
        let territory = infos.querySelector("#Territory");

        let lordLink = territory.querySelector("#LordLink");
        if (lordLink !== null) {
            lordLink.innerHTML = hoveredNode.sovereign.toString();
            lordLink.href = getUserShortcutURL(hoveredNode.sovereign);
        }
        showDOM(territory, true);

        let trade = infos.querySelector("#Trade");
        if (trade != null)
        {
            if (hoveredNode.sovereign.isEquals(myLord))
            {
                let trade = infos.querySelector("#Trade");
                if (trade != null)
                {
                    showDOM(trade, true);
                    let tradeValue = trade.querySelector("strong");
                    tradeValue.innerHTML = hoveredNode.trade.toString();

                    if (hoveredNode.trade < 0)
                    {
                        tradeValue.classList.add("warn");
                    }
                    else
                    {
                        tradeValue.classList.remove("warn");
                    }
                }
            }
            else
            {
                showDOM(trade, false);
            }
        }
    }

    const spot = resourceSpots[hoveredEntity.name];
    if (spot != null) {

        // Tooltip
        let resourceView = null;
        switch (spot.resource) {
            case 0:
                resourceView = infos.querySelector("#resourceLin");
                break;
            case 1:
                resourceView = infos.querySelector("#resourceIron");
                break;
            case 2:
                resourceView = infos.querySelector("#resourceHorse");
                break;
        }

        if (resourceView != null) {
            showDOM(resourceView, true);

            let number = resourceView.querySelector(".number");
            number.innerHTML = spot.amount;
        }
    }
}

function displayGeneralInfo(infos)
{
    if(hoveredGeneral == null)
    {
        console.error("No general but try to display a general");
        return;
    }

    let infoBlock = infos.querySelector("#greputation");
    if(infoBlock != null)
    {
        showDOM(infoBlock, true);
        let reputation = infoBlock.querySelector("strong");
        if (reputation != null)
            reputation.innerHTML = hoveredGeneral.reputation;
    }

    displayGeneralOwnership(infos.querySelector("#gowner"));

    if (hoveredGeneral.isMoving())
    {
        infoBlock = infos.querySelector("#gmoving");
        if(infoBlock != null)
        {
            showDOM(infoBlock, true);
            let nodeLink = infoBlock.querySelector("a");
            if(nodeLink) {
                let destination = hoveredGeneral.movementOrder.destinationName;
                nodeLink.innerHTML = destination;
                nodeLink.href = getNodeShortcutURL(world.id, nodesDict[destination].dbID);
            }

            let movementEndDate = hoveredGeneral.movementOrder.movementEndDate;

            updateTimer(movementEndDate, "timerTooltip")
            updateTimer(movementEndDate, "timerSidePanel")
        }
    }
    else
    {
        if(hoveredGeneral.isFortifying === false)
            infoBlock = infos.querySelector("#gidle");
        else
            infoBlock = infos.querySelector("#gfortifying");

        if(infoBlock != null)
        {
            showDOM(infoBlock, true);
            let nodeLink = infoBlock.querySelector("a");
            if(nodeLink != null)
            {
                nodeLink.innerHTML = hoveredGeneral.locationName;
                nodeLink.href = getNodeShortcutURL(world.id, nodesDict[hoveredGeneral.locationName].dbID);
            }
        }
    }
}

/**
 * Display the owner of the current General
 * @param ownerBlock
 */
function displayGeneralOwnership(ownerBlock)
{
    if (ownerBlock == null)
        return;

    showDOM(ownerBlock, true);
    let ownerText = ownerBlock.querySelector("strong");
    if (ownerText == null)
    {
        ownerText = ownerBlock.querySelector("a");
        if(hoveredGeneral.owner.isValid())
            ownerText.href = getUserShortcutURL(hoveredGeneral.owner);
    }
    ownerText.innerHTML = hoveredGeneral.owner.toString();
}

function updateDisplayedUnits(unitsHeader, unitsList, units)
{
    // TODO Refactor a bit this to be able to display garrison and defense!

    if (unitsHeader !== null && unitsList !== null) {
        let hasAnyUnits = false;
        if (units.length > 0) {
            const isZero = (e) => e === 0;
            hasAnyUnits = units.every(isZero) === false;
        }

        if (hasAnyUnits) {
            showDOM(unitsHeader, true);
            showDOM(unitsList, true);

            updateUnitsTitle(unitsHeader);
            updateUnitCells(unitsList, units);
        } else {
            showDOM(unitsHeader, false);
            showDOM(unitsList, false);

            hideAllUnitCells(unitsList);
        }
    }
}

//#endregion

//#endregion

//#region Interaction

function centerMapOnNode(nodeName)
{
    const node = nodesDict[nodeName];
    if(node)
    {
        mapRenderer.viewPosition.setPositionAndZoom(
            node.x * MAP_SCALE_FACTOR,
            node.y * MAP_SCALE_FACTOR, 1
        );
    }
}

/*
 * generalId : general index in the array allGenerals!
 */
function centerMapOnGeneral(generalId)
{
    const general = allGenerals[generalId];
    if(general)
    {
        mapRenderer.viewPosition.setPositionAndZoom(
            general.x,
            general.y, 1
        );
        if(!graphicsSettings.animate)
        {
            mapRenderer.renderMap();
        }
    }
}

function onMouseDragStart(event)
{
    lastClientX = event.clientX;
    lastClientY = event.clientY;
    onDragStart(event);
}

function onTouchDragStart(event)
{
    isPageScrollable = false;
    lastClientX = event.touches[0].clientX;
    lastClientY = event.touches[0].clientY;
    onDragStart(event);
}

function onDragStart(/*event*/)
{
    hasDragged = false;
    showDOM(tooltip, false);
    showDOM(kingdomtip, false);
    document.addEventListener("mouseup", onDragEnd);
    document.addEventListener("touchend", onTouchEnd);
    document.addEventListener("mousemove", onMouseDragging);
    document.addEventListener("touchmove", onTouchDragging);
    view.removeEventListener('mousemove', onMouseMove);
}

function onMouseDragging(event)
{
    let deltaX = event.clientX - lastClientX;
    let deltaY = event.clientY - lastClientY;

    onDragging(deltaX, deltaY);

    lastClientX = event.clientX;
    lastClientY = event.clientY;
}

function onTouchDragging(event)
{
    let deltaX = event.touches[0].clientX - lastClientX;
    let deltaY = event.touches[0].clientY - lastClientY;

    onDragging(deltaX, deltaY);

    lastClientX = event.touches[0].clientX;
    lastClientY = event.touches[0].clientY;
}

function onDragging(deltaX, deltaY)
{
    if (deltaX === 0 && deltaY === 0) {
        return;
    }
    hasDragged = true;
    hideTooltip = true;
    mapRenderer.viewPosition.drag(-deltaX, deltaY);
    if(!graphicsSettings.animate)
    {
        mapRenderer.renderMap();
    }
}

function onTouchEnd(/*event*/)
{
    isPageScrollable = true;
    onDragEnd();
}

function onDragEnd()
{
    hideTooltip = false;

    document.removeEventListener("mousemove", onMouseDragging);
    document.removeEventListener("touchmove", onTouchDragging);
    document.removeEventListener("mouseup", onDragEnd);
    document.removeEventListener("touchend", onTouchEnd);
    view.addEventListener('mousemove', onMouseMove);
}

/**
 * When mouse is moving but not dragging (every button is up)
 */
function onMouseMove(event)
{
    let rect = view.getBoundingClientRect();
    updateHoveredEntity(event.clientX - rect.left, event.clientY - rect.top);

    updateTooltip();
}

/**
 * Hide tooltip when mouse is out of the map area
 */
function onMouseLeave(/*event*/)
{
    hideTooltip = true;
    showDOM(tooltip, false);
}

/**
 * Enable tooltip drawing when mouse is hovering the map area
 */
function onMouseOver(/*event*/)
{
    hideTooltip = false;
}

function onClickMap(event)
{
    if (!hasDragged) {
        let rect = view.getBoundingClientRect();
        updateHoveredEntity(event.clientX - rect.left, event.clientY - rect.top);
        selectHoveredEntity();

        if(isSelectingDestination)
        {
            if (selectedEntity.type === EntityType.NODE)
            {
                extendGeneralPath();
            }
        }
        else
        {
            if (hoveredEntity.id !== -1) {
                switch(selectedEntity.type)
                {
                    case EntityType.NODE:
                        serverRequestNodeInfo();
                        break;
                    case EntityType.GENERAL:
                        serverRequestGeneralInfo();
                        break;
                    default:
                        break;
                }

                openWorldSideMenu();
                updateSideMenuContent();
            } else {
                closeWorldSideMenu();
            }
        }
    }
}

function onClickZoomOut(/*event*/)
{
    mapRenderer.viewPosition.zoomOut();
    if(!graphicsSettings.animate)
    {
        mapRenderer.renderMap();
    }
}

function onClickZoomIn(/*event*/)
{
    mapRenderer.viewPosition.zoomIn();
    if(!graphicsSettings.animate)
    {
        mapRenderer.renderMap();
    }
}

function onClickCapital()
{
    if(myCity != null)
    {
        mapRenderer.viewPosition.setPositionAndZoom(
            myCity.x * MAP_SCALE_FACTOR,
            myCity.y * MAP_SCALE_FACTOR, 1
        );
    }
    else
    {
        mapRenderer.viewPosition.setPositionAndZoom(
            MAP_SCALE_FACTOR * mapData['map_width'] / 2,
            MAP_SCALE_FACTOR * mapData['map_height'] / 2,
            1
        );
    }
    if(!graphicsSettings.animate)
    {
        mapRenderer.renderMap();
    }
}

function onClickMyGeneral(generalId)
{
    let focusGeneralId = -1;
    for(let i = 0; i < myGeneralsLocalID.length; i++)
    {
        let currentGeneral = allGenerals[myGeneralsLocalID[i]];
        if(currentGeneral.dbID === generalId)
        {
            focusGeneralId = myGeneralsLocalID[i];
            break;
        }
    }

    if (focusGeneralId !== -1)
    {
        centerMapOnGeneral(focusGeneralId);
    }
}

function onRefreshMap()
{
    //TODO: server request to get what have changed! (Lord Spawn/Death, Ownership, Battle, Movement, General Spawn/Death, ...)
    //TODO: clear WebGL object before recreating them, or update WebGL object
    loadMapGameState();
    if(!graphicsSettings.animate)
    {
        mapRenderer.renderMap();
    }
}

function onToggleStrategicMode()
{
    isStrategicView = !isStrategicView;
    if(!graphicsSettings.animate)
    {
        mapRenderer.renderMap();
    }
}

//#endregion

//#region Requests

// TODO Use ArmyContainer ID to factorize the UnitCache BUT the client needs to store the armyContainer ID during first batch query

let pendingNodeRequests = [];
let nodeUnitsCache = {};
let defenseUnitsCache = {};
let pendingGeneralRequests = [];
let generalUnitsCache = {};

/* Asks the server to fetch the missing node information (such as units, resource spot info, ...). */
function serverRequestNodeInfo()
{
    if (hoveredNode === null || hoveredNode.dbID === -1 || !hoveredNode.isOccupied() ||  pendingNodeRequests.includes(hoveredNode.dbID) || hoveredNode.dbID in nodeUnitsCache) {
        return;
    }

    let params = {
        world: world.id,
        node : hoveredNode.dbID,
    }

    pendingNodeRequests.push(hoveredNode.dbID);
    ServerGet("/fetchNodeInfo", params, handleNodeInfoResponse);
}

function handleNodeInfoResponse(json)
{
    if (json !== null) {
        const nodeId = parseInt(json['node']);
        pendingNodeRequests.splice(nodeId, 1);

        nodeUnitsCache[nodeId] = readUnitsFromResponse(json, 'units');
        defenseUnitsCache[nodeId] = readUnitsFromResponse(json, 'defenseUnits');

        if (hoveredNode != null && hoveredNode.id === nodeId) {
            updateUnitsOnTooltip();
            updateSideMenuUnits();
        }
    }
}

/* Asks the server to fetch the missing General info (such as units, movement, ...)*/
function serverRequestGeneralInfo()
{
    if (hoveredGeneral === null || hoveredGeneral.dbID === -1 || pendingGeneralRequests.includes(hoveredGeneral.dbID) || hoveredGeneral.dbID in generalUnitsCache) {
        return;
    }

    let params = {
        world: world.id,
        general : hoveredGeneral.dbID,
    }

    pendingGeneralRequests.push(hoveredGeneral.dbID);
    ServerGet("/fetchGeneralInfo", params, handleGeneralInfoResponse);
}

function handleGeneralInfoResponse(json)
{
    if (json !== null) {
        const generalId = parseInt(json['general']);
        pendingGeneralRequests.splice(generalId, 1);

        generalUnitsCache[generalId] = readUnitsFromResponse(json, 'units');

        if (hoveredGeneral != null && hoveredGeneral.dbID === generalId) {
            updateUnitsOnTooltip();
            updateSideMenuUnits();
        }
    }
}

function readUnitsFromResponse(json, propertyName)
{
    const units = json[propertyName];

    let unitAmounts = Array(UnitType.UnitCount).fill(0);
    for (let unit in units) {
        let amount = units[unit];
        switch (unit) {
            case "soldier":
                unitAmounts[UnitType.SOLDIER] = amount;
                break;
            case "pikeman":
                unitAmounts[UnitType.PIKEMAN] = amount;
                break;
            case "archer":
                unitAmounts[UnitType.ARCHER] = amount;
                break;
            case "horseman":
                unitAmounts[UnitType.HORSEMAN] = amount;
                break;
            case "knight":
                unitAmounts[UnitType.KNIGHT] = amount;
                break;
            case "mountedArcher":
                unitAmounts[UnitType.MOUNTED_ARCHER] = amount;
                break;
            case "paladin":
                unitAmounts[UnitType.PALADIN] = amount;
                break;
            case "catapult":
                unitAmounts[UnitType.CATAPULT] = amount;
                break;
            case "ballista":
                unitAmounts[UnitType.BALLISTA] = amount;
                break;
            default:
                break;
        }
    }

    return unitAmounts;
}

//#endregion

//#region Easter egg

function displayEasterEggInfo(infos)
{
    let text = infos.querySelector("#Text");
    if(text != null)
    {
        showDOM(text, true);
        text.innerHTML = "</br>Hello! I'm the Mountain's King.</br>For now, let me alone! I just want to sleep.</br><a href='https://myhordes.eu'>ZZZzzzz...</a>";
    }
    displayGeneralOwnership(infos.querySelector("#gowner"));
}

//#endregion